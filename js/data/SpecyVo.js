var SpecyVo = function () {

    this.id = "";
    this.translateKey = "";
    this.pricing = {};


    /**
     * Fill this with data
     */
    this.fillData = function ($data)
    {
        console.log("Creating Specy Vo with id: "+$data.id);

        this.id = $data.id;
        this.translateKey = $data.translateKey;
        this.pricing = $data.pricing;

    }

}

/*
 "pricing" : {
 "wood":"8",
 "0-120" : "0.4",
 "120-130": "0.55",
 "140-150": "0.55",
 "150-180": "0.55",
 "180-9999": "0.7"
 }
 */

