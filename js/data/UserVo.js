var UserVo = function () {

    /* ---------------------------------- Local properties ---------------------------------- */
    this.id = "";
    this.firstname = "";
    this.lastname = "";
    this.email = "";
    this.woodlandId = "";

    this.isLoggedIn = false;
    this.lastUpdate = 0;
    this.lastLocalChangesTimestamp = 0;


    /* ---------------------------------- PUBLIC methods ---------------------------------- */

    /**
     * Init
     * @param configFileURL
     */
    this.initialize = function ()
    {
        //probably smth like load local data?
    };

    /* ---------------------------------- PRIVATE methods ---------------------------------- */
    /**
     * Update / Create user Info
     */
    this.updateData = function ($data)
    {
        this.id = (Debug.USE_DEFAULT_ID)?Debug.DEFAULT_ID:$data.landowner.id;
        this.firstname = $data.landowner.firstName;
        this.lastname = $data.landowner.lastName;
        this.email = $data.landowner.email;
        this.woodlandId = $data.woodland.id;

        //this.lastUpdate = $data.lastUpdate;
        //this.lastLocalChangesTimestamp = 0;
    }

    /**
     * Generate data object for template html
     */
    this.generateTemplateData = function ()
    {
        var data = {};
        data.displayName = this.firstname + " " + this.lastname;
        return data;
    }


}