/**
 * Created by jonathan on 18/02/14.
 */
var Debug = (function () {

    /* --------------------------------- SINGLETON  -------------------------------- */
    var _static  = {

        // TESTING
        USE_LOCAL_SERVER_URL: false,
        //REMOVE_FORM_CHECK: false,
        //REMOVE_GPS_CHECK: false,
        LOGIN_PRE_FILL: false,

        //DATA
        CLEAR_LOCAL_DATA:false,
        USE_FAKE_DATA_WHEN_NEEDED:false,
        USE_DEFAULT_ID:false,
        DEFAULT_ID:48,
        FORCE_SYNC:false,
        DISABLE_SYNC:false,

        //DEBUG
        DO_NOT_WARN_ON_ERROR: false,
        DO_NOT_KILL_APP_ON_ERROR: false,
        DO_NOT_SEND_MAIL:false,
        OPEN_CONSOLE_ON_ERROR:false,
        UPLOAD_PHOTO_WHEN_TAKEN:false,
        USE_FAKE_NOTE:false,
        FORCE_NO_INTERNET:false,
        LOG_SERVICES_RESULTS:false,
        FORCE_SERVER_CALL_TO_FAIL:false
    };

    return  _static;
})();