/**
 * Created by jonathan on 18/02/14.
 */
var Infos = (function () {

    /* --------------------------------- SINGLETON  -------------------------------- */
    var _static  = {

        // an emulation of static variables and methods
        version: "1.0.2",
        config: new ConfigVo(),
        user: new UserVo(),
        session: new SessionVo(),
        speciesJSON: {},
        faqsJSON: {},
        pagesJSON: {},
        lang:"fr-be",
        //langHasChanged:false,
        localStorage:null,

        internet:function()
        {

            if(!navigator.network)
            {
                console.log("We are in browser... so this is supposed to be a testing environment");
                return (Debug.FORCE_NO_INTERNET == true)?false:true;
            }

            return ((navigator.network.connection.type === "none" || navigator.network.connection.type === null ||
                navigator.network.connection.type === "unknown" ) ? false : true );
        },



        generateFaqTemplateData : function()
        {
            var obj = {};
            obj.faqslist = [];
            var faqsToUse = this.faqsJSON[this.lang].faqs;
            if(faqsToUse !=undefined && faqsToUse.length>0)
            {
                for(var i=0; i<faqsToUse.length; i++) {
                    var faq = {};
                    faq.question = faqsToUse[i].question;
                    faq.answer = faqsToUse[i].answer;
                    obj.faqslist.push(faq);
                }
                return obj;
            }
            else
            return null;


        },

        generatePrivacyTemplateData : function()
        {
            var obj = {};
            obj.content = this.pagesJSON.privacy[this.lang];
            return obj;
        },

        /**
         * Save User account locally
         * @param $userId
         * @param $userLogin
         * @param $userPass
         */
        saveUser : function ($userId, $userLogin, $userPass)
        {
            this.localStorage.saveUser($userId, $userLogin, $userPass);
        },

        /**
         * Save Freshly downloaded user data locally
         * @param $userEmail
         * @param $dataToSave
         */
        saveUserFreshData : function ($userEmail,$dataToSave)
        {
            console.log('Infos > save User FRESH Data');
            //Clean data before saving (removing the result property)
            if($dataToSave.result != undefined)
                $dataToSave.result = {};

            //Set the timestamp to 0, because those are fresh data, no need to sync
            this.localStorage.syncPost($userEmail,$dataToSave, 0);
        },

        /**
         * Save current user data locally
         *
         */
        saveUserData : function ()
        {
            console.log('Infos > save User Data');
            var timestamp = new Date().getTime();
            //Set the timestamp to the current time, so we know that changes have been made and we need a sync.
            this.localStorage.syncPost(this.user.email,SyncManager.getInstance().generateJSonToSave(), timestamp);
        }


    };

    return  _static;
})();