var ConfigVo = function () {

    /* ---------------------------------- Local properties ---------------------------------- */
    var rawConfig; //XML
    var configNode; //XML

    /* ---------------------------------- PUBLIC methods ---------------------------------- */

    /**
     * Init
     * @param configFileURL
     */
    this.initialize = function (configFileURL)
    {
        console.log('ConfigVo.initialize');
        loadConfigFile(configFileURL);
    };

    /**
     * getValue
     * @param valueName
     */
    this.getValue = function (valueName)
    {
        return $(configNode).find(valueName).attr('data');
    };

    /* ---------------------------------- PRIVATE methods ---------------------------------- */
    /**
     * Load configuration file
     * @param configFileURL
     */
    function loadConfigFile (configFileURL)
    {
        console.log('ConfigVo > loadConfigFile');

        rawConfig =  SimpleLoader.getInstance().loadXML(configFileURL);
        configNode = $(rawConfig).find('config');

    }


}