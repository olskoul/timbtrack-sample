var MarkingVo = function () {

    this.id = new Date().getTime();
    this.seq = null;
    this.dbid = null;
    this.kind = null;
    this.point = new PointGPS();
    this.quality = false;
    this.species = null;
    this.specialTree = false;
    this.tonnage = null;
    this.quickTonnage = false;
    this.batch = null;
    this.group = null;
    this.calculation = new CalculationVo();
    this.notes = [];
    this.sync = SyncManager.SYNC_CREATE;

    this.setSpecies = function (speciesId)
    {
        if(speciesId != undefined)
        {
            //reset sequence id edition of species
           if(this.species != speciesId)
           this.seq = null;

           //set value
           this.species = speciesId;
        }
    }

    this.getPrice = function ()
    {
        console.log("Getting Price of marking (id: " +this.id+')');
        var isWood = (this.kind == "wood");
        var priceToUse = SpeciesManager.getInstance().getPrice(this.species,this.getCircumference(),isWood);
        var tonnage = this.getTonnage(false);
        console.log('Marking '+this.id+' : '+tonnage+" * "+priceToUse+" /m3 ");

        return  tonnage * priceToUse;
    }

    this.getTonnage = function ($accurate)
    {
        console.log("Getting tonnage of marking (id: " +this.id+')');
       switch (this.kind)
        {
            case "foot":
                var val1 = ( this.calculation.circumference / 100 ) - ( ( this.calculation.height / 2 - 1.5 ) * ( this.calculation.scrolling / 100 ) );
                var val2 = Math.pow(val1,2);
                var val3 = val2 /  (Math.PI * 4 );
                this.tonnage = val3 * this.calculation.height;
                break;

            case "ground":
                var val1 = ( this.calculation.diameter / 100 );
                var val2 = Math.pow(val1,2);
                var val3 = val2 / ( Math.PI * 4 );
                this.tonnage = val3 * this.calculation.length;
                break;

            case "wood":

                //quick tonnage case
                if(this.quickTonnage == true)
                {
                    return this.calculation.quickTonnage;
                }

                //or by volumes calculation
                var total = 0;
                for(var i=0; i<this.calculation.woodVolumes.length;i++)
                {
                    var volume = this.calculation.woodVolumes[i];
                    total += volume.getVolume();
                }
                return Math.floor(total * 100) / 100; //2 decimal
                break;

        }

        console.log("Accurate Tonnage is: "+this.tonnage);
        if($accurate == true)
        return this.tonnage
        else
        return Math.floor(this.tonnage * 100) / 100;
    }

    /**
     * Get circumference
     */
    this.getCircumference = function ()
    {
        switch (this.kind)
        {
            case "foot":
                return this.calculation.circumference;
                break;

            case "ground":
                return this.calculation.diameter * Math.PI;
                break;

            case "wood":
                return null;
                break;
        }
    }

    /**
     * Generate data object formatted to fit html Template
     * @returns {{}}
     */
    this.generateWoodSummaryTemplateData = function ()
    {

        var obj = {};
        obj.volumetotal = 0;
        obj.volumeList = [];

        if(this.quickTonnage == true)
        {
            var markingSummaryObj = {};
            markingSummaryObj.index = 1;
            markingSummaryObj.volume = this.calculation.quickTonnage;
            obj.volumeList.push(markingSummaryObj);
            obj.volumetotal = this.calculation.quickTonnage;
        }
        else
        {
            for(var i=0; i < this.calculation.woodVolumes.length; i++)
            {
                var woodVolumesVo = this.calculation.woodVolumes[i];
                var markingSummaryObj = {};
                markingSummaryObj.index = i+1;
                var volume = woodVolumesVo.getVolume();
                if(volume > 0.001)
                {
                    volume = Math.floor(volume * 1000) / 1000;
                }
                markingSummaryObj.volume = volume;
                volume =  obj.volumetotal + markingSummaryObj.volume;//Math.floor((obj.volumetotal + markingSummaryObj.volume) * 1000) / 1000;
                obj.volumetotal = volume;
                obj.volumeList.push(markingSummaryObj);
            }

            obj.volumetotal = Math.floor(obj.volumetotal * 1000) / 1000;
        }


        return obj;

    }

    /**
     * Fill this with data
     */
    this.fillData = function ($data)
    {
        console.log("Creating Marking Vo with id: "+$data.id);
        this.id = $data.id;
        this.seq = $data.seq;
        this.dbid = $data.dbid;
        this.kind = $data.kind;
        this.quality = $data.quality;
        this.species = $data.species;
        this.specialTree = $data.specialTree;
        this.batch = $data.batch;
        this.group = $data.group;
        this.tonnage = $data.tonnage;
        this.calculation = new CalculationVo();
        this.calculation.fillData($data.calculation);
        this.notes = $data.notes;
        this.point = new PointGPS();
        this.point.fillData($data.point);
        this.quickTonnage = $data.quickTonnage;
        this.sync = $data.sync;
    }

}

var CalculationVo = function()
{
    this.woodVolumes = [];
    this.circumference = null;
    this.height = null;
    this.scrolling = null;
    this.length = null;
    this.diameter = null;
    this.quickTonnage = null;

    this.fillData = function($data)
    {
        this.circumference = $data.circumference;
        this.height = $data.height;
        this.length = $data.length;
        this.scrolling = $data.scrolling;
        this.diameter = $data.diameter;
        this.quickTonnage = $data.quickTonnage;
        if($data.woodVolumes)
        {
            for(var i=0; i<$data.woodVolumes.length ; i++)
            {
                var volumeVo = new WoodVolumesVo();
                volumeVo.fillData($data.woodVolumes[i]);
                this.woodVolumes.push(volumeVo);
            }
        }

    }

}

var WoodVolumesVo = function($width, $height, $depth)
{
    this.id = new Date().getTime();
    this.width = $width;
    this.height = $height;
    this.depth = $depth;


    this.getVolume = function ()
    {
        var volume = ( this.width/100 * this.height/100 * this.depth/100 );
        var stere = volume * 1.4285;
        return  stere;
    }

    this.fillData = function($data)
    {
        this.id = $data.id;
        this.width = $data.width;
        this.height = $data.height;
        this.depth = $data.depth;
    }
}



var PointGPS = function()
{
    //Wood only
    this.speed = false;
    this.heading = false;
    this.altitudeAccuracy = false;
    this.accuracy = false; // in meters
    this.altitude = false;
    this.longitude = false;
    this.latitude = false;

    this.fillFromPosition = function(coords, $position)
    {

        this.speed = ($position.getSpeed() == undefined)?false:$position.getSpeed();
        this.heading = ($position.getHeading() == undefined)?false:$position.getHeading();
        this.altitudeAccuracy = ($position.getAltitudeAccuracy() == undefined)?false:$position.getAltitudeAccuracy();
        this.accuracy = ($position.getAccuracy() == undefined)?false:$position.getAccuracy();
        this.altitude = ($position.getAltitude() == undefined)?false:$position.getAltitude();
        this.longitude = (coords == undefined)?false:coords[0];
        this.latitude = (coords == undefined)?false:coords[1];

    }

    this.fillData = function($data)
    {
        this.speed = $data.speed;
        this.heading = $data.heading;
        this.altitudeAccuracy = $data.altitudeAccuracy;
        this.accuracy = $data.accuracy;
        this.altitude = $data.altitude;
        this.longitude = $data.longitude;
        this.latitude = $data.latitude;
    }
}
