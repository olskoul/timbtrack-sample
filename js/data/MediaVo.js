var MediaVo = function () {

    this.id = new Date().getTime();
    this.dbid = null;
    this.type = "";
    this.hash = "";
    this.name = "";
    this.size = "";
    this.sync = SyncManager.CREATED;
    this.createdAt = "";
    this.noteId = null;

    /**
     * Fill this with data
     */
    this.fillData = function ($data)
    {
        console.log("Creating Media Vo with id: "+$data.id);

        this.id = $data.id;
        this.dbid = $data.dbid;
        this.type = $data.type;
        if($data.hash.substr(0,1) == "/")
        {
            $data.hash = $data.hash.substr(1,$data.hash.length-1);
        }
        this.hash = $data.hash;
        this.name = $data.name;
        this.size = $data.size;
        this.sync = $data.sync;
    }

}

