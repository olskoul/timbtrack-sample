var GroupVo = function () {

    this.id = "";
    this.dbid = null;
    this.customName = "";
    this.markings = []; //list of markingVo ids
    this.sync = SyncManager.SYNC_CREATE;

    this.generateTemplateData = function()
    {
        var obj = {};
        obj.groupId = this.id;
        obj.customName = this.customName;
        obj.markings = [];

        obj.totalBySpecies = this.getTotalBySpecies();

        for(var i=0; i<this.markings.length; i++)
        {
            var markingVo = MarkingManager.getInstance().getMarkingById(this.markings[i]);
            var markingSummaryObj = {};
            markingSummaryObj.id = markingVo.id;
            markingSummaryObj.index = i+1;
            markingSummaryObj.realIndex = i;
            markingSummaryObj.sequence = markingVo.seq;
            var speciesLabel = SpeciesManager.getInstance().getInitialsLabel(markingVo.species);

            markingSummaryObj.speciesLabel = speciesLabel;
            markingSummaryObj.markingid = markingVo.id;
            markingSummaryObj.kindShort = (markingVo.kind == "wood")?"":"("+ResourceManager.getString("marking_shortname_"+markingVo.kind)+")";
            markingSummaryObj.kind = ResourceManager.getString("marking_Details_"+markingVo.kind+"_title");
            markingSummaryObj.kindId = markingVo.kind;
            var tonnageType = (markingVo.kind == "wood")?ResourceManager.getString("common_stere_s"):"m³";
            markingSummaryObj.tonnage = markingVo.getTonnage()+" / "+obj.totalBySpecies[markingVo.species]+" "+tonnageType;
            obj.markings.push(markingSummaryObj);
        }



        return obj;

    }

    /**
     * Get total tonnage by species
     * returns associative array (Key = species ID)
     */
    this.getTotalBySpecies = function (){
        var arr = [];
        for(var i=0; i<this.markings.length; i++)
        {
            var currentMarkingVo = MarkingManager.getInstance().getMarkingById(this.markings[i]);
            if(arr[currentMarkingVo.species] == undefined)
                arr[currentMarkingVo.species] = currentMarkingVo.getTonnage();
            else
                arr[currentMarkingVo.species] +=  currentMarkingVo.getTonnage();
        }
        return arr;
    }

    /**
     * Get Next Sequence ID for marking
     */
    this.getNextSeq = function (markingVo){
        var seq=1;
        for(var i=0; i<this.markings.length; i++)
        {
            var currentMarkingVo = MarkingManager.getInstance().getMarkingById(this.markings[i]);
            if (currentMarkingVo.species == markingVo.species && currentMarkingVo.id != markingVo.id){
                seq++;
            }
        }
        return seq;
    }

    /**
     * Fill this with data
     */
    this.fillData = function ($data)
    {
        console.log("Creating group Vo with id: "+$data.id);
        this.id = $data.id;
        this.dbid = $data.dbid;
        this.customName = $data.customName;
        this.markings = $data.markings;
        this.sync = $data.sync;
    }
}