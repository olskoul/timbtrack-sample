var BatchVo = function ($id) {

    this.id = $id;
    this.dbid = null;
    this.name = "";
    this.markings = []; //list of markingVo ids
    this.type = ""; //MarkingManager.BATCH_TYPE_INVENTORY or MarkingManager.BATCH_TYPE_STOCK
    this.isDefault = false;
    this.priceForTemplate;
    this.sync = SyncManager.SYNC_CREATE;

    /**
     * Get price of all marking in batch
     */
    this.getPrice = function ()
    {
        var price = 0;
        for(var i=0; i<this.markings.length; i++)
        {
            var markingVo = MarkingManager.getInstance().getMarkingById(this.markings[i]);
            var markingPrice = markingVo.getPrice();
            price += markingPrice;
        }

        return price;
    }

    /**
     * Fill this with data
     */
    this.fillData = function ($data)
    {
        console.log("Creating batch Vo with id: "+$data.id);
        this.id = $data.id;
        this.dbid = $data.dbid;
        this.name = $data.name;
        this.type = $data.type;
        this.markings = $data.markings;
        this.isDefault = $data.isDefault;
        this.sync = $data.sync;
    }

}