var AreaVo = function () {

    this.id = "";
    this.name = "";
    this.bounds = [];
    this.batch = "";
    this.openLayerStyle = new openLayerStyleVo();

    /**
     * Fill this with data
     */
    this.fillData = function ($data)
    {
        console.log("Creating Area Vo with id: "+$data.id);

        this.id = $data.id;
        this.name = $data.name;
        this.bounds = [];
        this.batch = $data.batch;
        this.openLayerStyle = new OpenLayerStyleVo().fillData($data.openLayerStyle);

        for(var i=0; i<$data.bounds.length; i++)
        {
            this.bounds.push($data.bounds[i]);
        }
    }
}

var OpenLayerStyleVo = function () {

    this.color = "";
    this.pattern = "";
    this.opacity = "";
    this.lineStyle = "";

    this.fillData = function ($data)
    {
        console.log("Creating openLayerStyle Vo");

        this.color = $data.color;
        this.pattern = $data.pattern;
        this.opacity = $data.opacity;
        this.lineStyle = $data.lineStyle;
    }
}
