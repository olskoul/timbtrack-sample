var WoodlandVo = function () {

    this.id = "";
    this.name = "";
    this.geoJSON = "";
    this.bounds = [];
    this.areas = [];
    this.batch;
    /**
     * Fill this with data
     */
    this.fillData = function ($data)
    {
        console.log("Creating Woodland Vo with id: "+$data.id);

        this.id = $data.id;
        this.name = $data.name;
        this.geoJSON = $data.geoJSON;
        this.bounds = [];
        this.batch = $data.batch;

        for(var i=0; i<$data.bounds.length; i++)
        {
            this.bounds.push($data.bounds[i]);
        }
    }



}