var NoteVo = function () {

    this.id = new Date().getTime();
    this.dbid = null;
    this.type = null;
    this.name = ""; //??
    this.content = "";
    this.medias = [];
    this.point = new PointGPS();
    this.sync = SyncManager.SYNC_CREATE;

    /**
     * Fill this with data
     */
    this.fillData = function ($data)
    {
        console.log("Creating Note Vo with id: "+$data.id);

        this.id = $data.id;
        this.dbid = $data.dbid;
        this.name = $data.name;
        this.content = $data.content;
        this.point = new PointGPS();
        this.point.fillData($data.point);
        this.type = $data.type;
        this.sync = $data.sync;

        if($data.medias)
        {
            for(var i=0; i<$data.medias.length; i++)
            {
                var mediaVo = new MediaVo();
                mediaVo.fillData($data.medias[i]);
                this.medias.push(mediaVo);
            }
        }

    }

}

