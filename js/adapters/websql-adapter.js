var WebSqlAdapter = function () {

    /**
     * Initiate Data Adapter
     * Create tables (if don't exist)
     *
     * @returns {*}
     */
    this.initialize = function () {
        var deferred = $.Deferred();
        this.db = window.openDatabase("EmployeeDemoDB", "1.0", "Employee Demo DB", 200000);
        this.db.transaction(
            function (tx) {
                createTable(tx);
                addSampleData(tx);
             },
            function (error) {
                console.log('Transaction error: ' + error);
                deferred.reject('Transaction error: ' + error);
            },
            function () {
                console.log('Transaction success');
                deferred.resolve();
            }
        );
        return deferred.promise();
    }

    /**
     * Create table function
     * Dynamically creates tables the app needs
     *
     * Users table
     *
     * @param tx
     */
    var createTable = function (tx) {
        //tx.executeSql('DROP TABLE IF EXISTS employee');

        /**
         * Create Users table
         * user ID
         * username
         * password
         */
        var sql = "CREATE TABLE IF NOT EXISTS users ( " +
            "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
            "id INTEGER, " +
            "firstName VARCHAR(50), " +
            "lastName VARCHAR(50), " +
            "userName VARCHAR(50), " +
            "password VARCHAR(50)) ";

        tx.executeSql(sql, null,
            function () {
                console.log('Create table success');
            },
            function (tx, error) {
                alert('Create table error: ' + error.message);
            });
    }

    this.getAllUsers = function (id) {

        console.log('Gat all users');
        var deferred = $.Deferred();
        this.db.transaction(
            function (tx) {

                var sql = "SELECT * " +
                    "FROM users ";

                tx.executeSql(sql, [id], function (tx, results) {
                    deferred.resolve(results.rows.length === 1 ? results.rows.item(0) : null);
                });
            },
            function (error) {
                deferred.reject("Transaction Error: " + error.message);
            }
        );
        return deferred.promise();
    };

   /* this.findByName = function (searchKey) {
        var deferred = $.Deferred();
        this.db.transaction(
            function (tx) {

                var sql = "SELECT e.id, e.firstName, e.lastName, e.title, e.pic, count(r.id) reportCount " +
                    "FROM employee e LEFT JOIN employee r ON r.managerId = e.id " +
                    "WHERE e.firstName || ' ' || e.lastName LIKE ? " +
                    "GROUP BY e.id ORDER BY e.lastName, e.firstName";

                tx.executeSql(sql, ['%' + searchKey + '%'], function (tx, results) {
                    var len = results.rows.length,
                        employees = [],
                        i = 0;
                    for (; i < len; i = i + 1) {
                        employees[i] = results.rows.item(i);
                    }
                    deferred.resolve(employees);
                });
            },
            function (error) {
                deferred.reject("Transaction Error: " + error.message);
            }
        );
        return deferred.promise();
    }

    this.findById = function (id) {
        var deferred = $.Deferred();
        this.db.transaction(
            function (tx) {

                var sql = "SELECT e.id, e.firstName, e.lastName, e.title, e.city, e.officePhone, e.cellPhone, e.email, e.pic, e.managerId, m.firstName managerFirstName, m.lastName managerLastName, count(r.id) reportCount " +
                    "FROM employee e " +
                    "LEFT JOIN employee r ON r.managerId = e.id " +
                    "LEFT JOIN employee m ON e.managerId = m.id " +
                    "WHERE e.id=:id";

                tx.executeSql(sql, [id], function (tx, results) {
                    deferred.resolve(results.rows.length === 1 ? results.rows.item(0) : null);
                });
            },
            function (error) {
                deferred.reject("Transaction Error: " + error.message);
            }
        );
        return deferred.promise();
    };*/



    var addSampleData = function (tx) {

        var users = [
            {"userId": 1, "firstName": "Russell", "lastName": "Westbrook", "userName": "Russ", "password": "Russ00"}
           ];
        var l = users.length;
        var sql = "INSERT OR REPLACE INTO users " +
            "(id, id, firstName, lastName, userName, password) " +
            "VALUES (?, ?, ?, ?, ?, ?)";
        var e;
        for (var i = 0; i < l; i++) {
            e = users[i];
            tx.executeSql(sql, [0, e.id, e.firstName, e.lastName, e.userName, e.password],
                function () {
                    console.log('INSERT success');
                },
                function (tx, error) {
                    alert('INSERT error: ' + error.message);
                });
        }
    }

}