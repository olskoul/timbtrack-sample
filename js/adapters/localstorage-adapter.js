var LocalStorageAdapter = function () {

    this.LAST_LOCAL_UPDATE_KEY = 'lastLocalUpdate';
    this.LOCAL_ITEM_USERS = 'users';
    this.LOCAL_ITEM_LAST_USER = 'lastuser';
    this.LOCAL_ITEM_DATAS = 'datas';
    this.LOCAL_ITEM_RESOURCES = 'resources';
   // this.LOCAL_ITEM_DATAS = 'datas';

    this.initialize = function()
    {
        var deferred = $.Deferred();
        deferred.resolve();
        return deferred.promise();
    }

    /**
     * Save user credential
     * @param $userLogin
     * @param $userPass
     */
    this.saveUser = function($userLogin,$userPass)
    {
        console.log("Local storage > saveUser");
        //Get users already saved
        var users = JSON.parse(window.localStorage.getItem(this.LOCAL_ITEM_USERS));
        users = (users == null)?{}:users;

        //inject user
        users[$userLogin] = {login:$userLogin,pass:$userPass};

        //save all
        window.localStorage.setItem(this.LOCAL_ITEM_USERS, JSON.stringify(users));

    }

    /**
     * Save last user username
     * @param $userLogin
     */
    this.saveLastUser = function($userLogin)
    {
        console.log("Local storage > save last User");

        //save
       window.localStorage.setItem(this.LOCAL_ITEM_LAST_USER, $userLogin);
    }

    /**
     * Get last user credential
     */
    this.getLastUser = function()
    {
        console.log("Local storage > GET last User");

        //save
        var userName = window.localStorage.getItem(this.LOCAL_ITEM_LAST_USER);

        //return
        if(userName != undefined)
            return userName;
        else
            return "";
    }

    /**
     * Save resources (translations, faqs, privacy pages)
     * @param $data
     */
    this.saveResources = function($data)
    {
        console.log("Local storage > saveResources");
        //save data
        window.localStorage.setItem(this.LOCAL_ITEM_RESOURCES, JSON.stringify($data));

    }
    /**
     * Load resources (translations, faqs, privacy pages)
     * @param $data
     */
    this.loadResources = function()
    {
        console.log("Local storage > lod local resources");
        //load data
        var resources = JSON.parse(window.localStorage.getItem(this.LOCAL_ITEM_RESOURCES));
        return resources;

    }

    /**
     * Check if user has already logged in the app before
     * @param $userName
     * @param $userPass
     * @returns {boolean}
     */
    this.checkUser = function($userName,$userPass)
    {
        console.log("Local storage > checkUser");
        //Get users already saved
        var users = JSON.parse(window.localStorage.getItem(this.LOCAL_ITEM_USERS));

        if(users == null)
        return false;

        //inject user
        if(users[$userName] != undefined && users[$userName] != null)
        {
            if(users[$userName].pass == $userPass)
            return true;
            else
            return false;
        }
        else
        return false
    }

    /**
     * Load user data
     * @param $userEmail
     * @returns {data} or null
     */
    this.syncGET = function($userEmail)
    {
        console.log("Local storage > loadUserData");
        //Get datas already saved
        var datas = JSON.parse(window.localStorage.getItem(this.LOCAL_ITEM_DATAS));

        if(datas == null)
           return null;

        var userData = datas[$userEmail];
        //console.log("LOCAL DATA FOR ID "+$userEmail+" toString: ");
        //console.log(JSON.stringify(userData));

        if(userData != null && userData != undefined)
        return userData;
        else
        {
            console.log("Localstorage: userData is null or undefined");
            return null;
        }

    }

    /**
     * Save user data
     * @param $userEmail
     * @returns {data} or null
     */
    this.syncPost = function($userEmail, $userData, $timeStamp)
    {
        console.log("Local storage > saveUserData > "+$userEmail);

        //User data to string
        var dataString = JSON.stringify($userData);
        console.log("User Data to save (string): ");
        console.log(dataString);

        //Get datas repository
        var datas = JSON.parse(window.localStorage.getItem(this.LOCAL_ITEM_DATAS));
        datas = (datas == null)?{}:datas;

        //inject timestamp
        $userData[this.LAST_LOCAL_UPDATE_KEY] = $timeStamp;

        //inject data
        datas[$userEmail] = $userData;

        //save all
        window.localStorage.setItem(this.LOCAL_ITEM_DATAS,JSON.stringify(datas));
    }




    /**
     * DELETE ALL LOCAL DATA
     */
    this.clearLocalData = function()
    {
        console.log("LocalStorage > clearLocalData")
        window.localStorage.clear();
    }

}