// We use an "Immediate Function" to initialize the application to avoid leaving anything behind in the global scope
(function () {

    /* ---------------------------------- Local Variables ---------------------------------- */

    var localStorage = new LocalStorageAdapter();
    var pageSlider = new PageSlider($('#content-wrapper'));
    var currentPage = "";
    var cache = {};

    /* ---------------------------------- Initiate data adapter ---------------------------------- */

    localStorage.initialize().done(function ()
    {
        console.log('App > localStorage.initialize');
        console.log('App > token must be null at this point: token = '+ServicesManager.getInstance().getToken());
        console.log('App > Debug.FORCE_NO_INTERNET should be false: Debug.FORCE_NO_INTERNET = '+Debug.FORCE_NO_INTERNET);

        //fix Width
        var fixWidth = $(window).width();
        $('body').css('width',fixWidth);

        //ref localStorage
        Infos.localStorage = localStorage;

        //If flag is true, clear local Data
        if(Debug.CLEAR_LOCAL_DATA)
        Infos.localStorage.clearLocalData();

        //disable click from slider container
        pageSlider.activateContainer(false);

        //load config
        loadConfig();

        //load resources
        loadResources();

        //init Geolocation
        GeolocationManager.getInstance().init();

    });


    /**
     * Load config
     */
    function loadConfig()
    {
        console.log('App > load config');
        Infos.config.initialize("assets/xml/config.xml");
    }


    /**
     * Load local resources
     */
    function loadResources()
    {
        console.log("App > load Resources");
        //load resources
        Loading.showLoading("Loading languages...",true);
        ServicesManager.getInstance().getResources(function($data)
        {
            resourceLoadedSuccess($data);
        },
        function($msg)
        {
            console.log("App > Resources loadError: "+$msg);

            //try to load the local version
            ServicesManager.getInstance().getResourceLocally(function($data)
            {
                //Local version loaded with sucess
                console.log("App > Resources load locally success");
                Loading.hideLoading();
                resourceLoadedSuccess($data, true);

            },function($msg)
            {
                //Local version failed
                console.log("App > Resources load locally success -> We're fucked");
                Loading.hideLoading();
                var retryFunction = loadResources;
                PopupManager.getInstance().openAlertPopup("Loading languages failed", "Languages failed to load online and no local version was found.",retryFunction);

            });
        });


    }

    /**
     * Resource loaded success handler
     */
    function resourceLoadedSuccess(resourcesObject, islocal)
    {
        console.log("App > Resources loaded");

        //Save locally
        Infos.localStorage.saveResources(resourcesObject);

        //Fill Resource
        ResourceManager.getInstance().init(resourcesObject.translations);

        //Fill Faqs
        Infos.faqsJSON  = resourcesObject.faqs;

        //Fill Pages
        Infos.pagesJSON  = resourcesObject.pages;

        //hide loading
        Loading.hideLoading();

        if(islocal == true)
        {
            Debug.FORCE_NO_INTERNET = true;
            PopupManager.getInstance().openAlertPopup(ResourceManager.getString("popup_session_without_internet_title"), ResourceManager.getString("popup_session_without_internet_desc"));
        }

        //showLogin
        showLogin();
    }


    /**
     * Add menu to index.html
     */
    function showLogin()
    {
        console.log('App > showLogin');
        pageSlider.activateContainer(false);
        pageSlider.emptyPage();

        var view = new LoginView(localStorage);
        $('#menu-wrapper').html(view.el);
        view.setup();
    }

    /**
     * replace login by menu to index.html
     */
    function showHome()
    {
        console.log('App > showHome');
        var view = new HomeView(localStorage);
        var pageEl = view.el;
        $('#menu-wrapper').html(pageEl);
        view.setup();
        cachePage(view, "home", pageEl);
    }

    /**
     * Logout
     */
    function logout()
    {
        /*currentPage = "";
        cache = {};
        ServicesManager.getInstance().setToken(null);

        showLogin();*/
        killApp();
    }

    /* ---------------------------------- Routing ---------------------------------- */
    function route()
    {
        var hash = window.location.hash;
        if (!hash) return;

        var splittedHash = hash.split("#")[1].split("/");
        var pageName = splittedHash[0];
        var page;
        var pageEl;

        //Create page or get it from cache
        if(currentPage != pageName)
        {
            console.log("App > Route page : "+pageName);

            //first check the cache
            if(cache[pageName] != undefined)
            {
                page = cache[pageName].page;
                pageEl = cache[pageName].pageEl;
            }
            else
            {
                switch(pageName)
                {
                    case "homefromlogin":
                        showHome();
                        return;
                        break;

                    case "home":
                        page = null;
                        pageSlider.moveContentToRight(true, true);
                        break;

                    case "marking":
                        page = new MarkingView(localStorage);//.render().el;
                        pageEl = page.el;
                        break;

                    case "map":
                        page = new MapView("woodland");//.render().el;
                        pageEl = page.el;
                        break;

                    /*case "goto":
                        page = new MapView("goto");//.render().el;
                        pageEl = page.render().el;
                        break;*/

                    case "faq":
                        page = new FaqView(localStorage);//.render().el;
                        pageEl = page.el;
                        break;

                    case "manage":
                        page = new ManageView(localStorage);//.render().el;
                        pageEl = page.el;

                        break;

                    case "settings":
                        page = new SettingsView(localStorage);//.render().el;
                        pageEl = page.el;
                        //ServicesManager.getInstance().uploadMedia("");
                        break;

                    case "logout":
                        logout();
                        return;
                        break;
                }
            }

            //Display page
            if(pageName == "home")
                pageSlider.moveContentToRight(true, true);
            else if(pageEl)
            {
                if(!currentPage)
                pageSlider.moveContentToRight();

                pageSlider.displayPage($(pageEl),"right",false);
                pageSlider.activateContainer(true);
            }

            //Set current page ref
            if(pageName != "home")
            currentPage = pageName;

            //Show page transition then setupe page (avoid latency on transition)
            var delay= (pageName == "home")?10:600;
            setTimeout(function()
            {
                buildPage(page,pageName,pageEl)
            }, delay)
        }
        else
        {
            if(cache[pageName] != undefined)
            {
                page = cache[pageName].page;
                if(page != undefined)
                page.header.showLeftBtn("btnMenu");
            }
            pageSlider.moveContentToCenter(true);
        }

    }

    function buildPage(page, pageName, pageEl)
    {
        if(page != null)
        {
            if(page != null && cache[pageName] == undefined)
                page.setup();
            else if(page.refresh != undefined)
            {
                page.refresh();
            }


        }

        //cache the page
        cachePage(page, pageName, pageEl);
    }

    function cachePage(page, pageName, pageEl)
    {
        //cache the page
        if(page != null)
        {
            var pageToCache = {};
            pageToCache.page = page; //cache the object
            pageToCache.pageEl = pageEl; //cache the HTML
            cache[pageName] = pageToCache;
        }
    }


    /* --------------------------------- Event Registration -------------------------------- */
    $(window).on('hashchange', route);

    document.addEventListener('deviceready', function () {

        //FastClick.attach(document.body);

        if (navigator.notification) { // Override default HTML alert with native dialog
            window.alert = function (message) {
                navigator.notification.alert(
                    message,    // message
                    null,       // callback
                    "Workshop", // title
                    'OK'        // buttonName
                );
            };
        }
    }, false);

    window.onerror = function myErrorHandler(errorMsg, url, lineNumber)
    {
        if(Debug.DO_NOT_WARN_ON_ERROR === false)
        {
            var killFunction = (Debug.DO_NOT_KILL_APP_ON_ERROR === false)?killApp:null;
            PopupManager.getInstance().openAlertPopup(ResourceManager.getString("popup_global_error_title"), ResourceManager.getString("popup_global_error_desc") + "\n\n"+errorMsg+ "\n" +"url: "+url+ "\n" +"line: "+ lineNumber,killFunction);
        }

        if(Debug.DO_NOT_SEND_MAIL === false)
            ServicesManager.getInstance().mail("ERROR CONTENT:\n"+errorMsg + "\n" +"url: "+url+ "\n" +"line: "+ lineNumber+ "\n\n" + "FULL LOG:"+ "\n\n" + logger.join("\n"));

        if(Debug.OPEN_CONSOLE_ON_ERROR)
            showConsole();

        console.log("--------------------");
        console.log("--------------------");
        console.log("GLOBAL ERROR CATCH:");
        console.log("--------------------");
        console.log("--------------------");
        console.log("Error message: "+errorMsg);
        console.log("url: "+url);
        console.log("line: "+lineNumber);


    };

    function killApp()
    {
        location.reload(true);
    }

    function showConsole()
    {
        window.location.hash = "#console";
        $("#console").prepend( '<p class="closeConsole customControl">CLOSE</p>');
        $("#console")[0].innerHTML = logger.join("<br/>");
        $("#console").append( '<p class="closeConsole customControl">CLOSE</p>');
        $("#console").css('display','block');
        $(".closeConsole")[0].addEventListener('click', hideConsole);
    }

    function hideConsole()
    {
        $("#console")[0].innerHTML = "";
        $("#console").css('display','none');
    }

    /* ---------------------------------- Local Functions ---------------------------------- */

}());