/* Notes:
 * - History management is currently done using window.location.hash.  This could easily be changed to use Push State instead.
 * - jQuery dependency for now. This could also be easily removed.
 */

function PageSlider(container) {

    var container = container,
        currentPage,
        stateHistory = [];

    // Use this function if you want PageSlider to automatically determine the sliding direction based on the state history
    this.slidePage = function(page) {

        var l = stateHistory.length,
            state = window.location.hash;

        if (l === 0) {
            stateHistory.push(state);
            this.slidePageFrom(page);
            return;
        }
        if (state === stateHistory[l-2]) {
            stateHistory.pop();
            this.slidePageFrom(page, 'left');
        } else {
            stateHistory.push(state);
            this.slidePageFrom(page, 'right');
        }

    }

    /*
        Main container only
        Use this function to display page with transitions
        Switch method if container is centered or not
     */
    this.displayPage = function(page, from, noTransition, onComplete)
    {
        var containerClassStr = container.attr("class");

        //Condition: container is on the right end side
        //Change its content, and move it back to center
        if(containerClassStr.indexOf('right') != -1 || noTransition)
        {
            this.replacePage(page);
        }

        //Condition: container is centered
        //Append page and create sliding transition
        else// if(containerClassStr.indexOf('center') != -1)
        {
            this.slidePageFrom(page,from);
        }
    }

    this.replacePage = function(page)
    {
        //add page
        container.append(page);

        //make sure the container is centered and 100% width
        this.moveContentToCenter(true);

        //Make sure the page itself is centered
        page.attr("class", "page center");

        //Set current page
        currentPage = page;

        //force redraw
        container[0].offsetWidth;
    }

    //Remove all content
    this.emptyPage = function()
    {
        container.empty();
    }

    // Use this function directly if you want to control the sliding direction outside PageSlider
    this.slidePageFrom = function(page, from)
    {

        if (!currentPage || !from)
        {
            this.replacePage(page);
            return;
        }

        //append page to container
        container.append(page);
        //make sure the container is centered and 100% width
        this.moveContentToCenter(true);

        // Position the page at the starting position of the animation
        page.attr("class", "page " + from);

       // $('body').css('pointer-events','none');

        //when page transition is done, remove ols content
        currentPage.one('webkitTransitionEnd', function(e)
        {
            $('body').css('pointer-events','inherit');
            $(e.target).remove();
        });

        // Force reflow. More information here: http://www.phpied.com/rendering-repaint-reflowrelayout-restyle/
        container[0].offsetWidth;

        // Position the new page and the current page at the ending position of their animation with a transition class indicating the duration of the animation
        page.attr("class", "page transition center");
        currentPage.attr("class", "page transition " + (from === "left" ? "right" : "left"));
        currentPage = page;
    }

    /**
     * Move content wrapper to the left hand side of the screen
     * showEdge (true/false) 95% instead of 100%
     */
    this.moveContentToRight = function(showEdge, transition)
    {
        //make sure the container is visible
       // this.hideContainer(false);

        var className = (!showEdge)?'right':'right95';
        if (transition)
        {
            container.attr("class", "page transition "+className);
            container[0].offsetWidth;
        }
        else
        {
            container.attr("class", "page "+className);
            // Force reflow. More information here: http://www.phpied.com/rendering-repaint-reflowrelayout-restyle/
            container[0].offsetWidth;
        }

    }

    /**
     * Move content wrapper to the center of the sreen
     * @transition or not
     */
    this.moveContentToCenter = function(transition)
    {
        //make sure the container is visible
        //this.hideContainer(false);

        var className = 'center';
        if (transition)
            container.attr("class", "page transition "+className);
        else
            container.attr("class", "page "+className);
    }

    /**
     * hide or show container
     */
    this.activateContainer = function($flag)
    {
        if(!$flag)
        container.css('pointer-events','none');
        else
        container.css('pointer-events','inherit');
    }

}