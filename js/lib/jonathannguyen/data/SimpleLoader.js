/**
 * Created by jonathan on 18/02/14.
 */
var SimpleLoader = (function () {

    /* --------------------------------- Properties -------------------------------- */
    var instance;  // our instance holder



    /* --------------------------------- SINGLETON  -------------------------------- */
    var _static  = {

        // an emulation of static variables and methods

        /**
         * Method for getting an instance. It returns
         * a singleton instance of a singleton object
         */
        getInstance:  function()
        {
            if( instance  ===  undefined )  {
                instance = new Singleton();
            }

            return  instance;
        }

        // Static variable
        //TEMPLATE_MENU : "menu/template_MenuView",
        //TEMPLATE_MARKING : "marking/template_"
    };

    return  _static;

    function Singleton()
    {

        /* --------------------------------- functions -------------------------------- */



        /**
         * Simple XML load
         * url
         * Error handler
         */
        this.loadXML = function(url)
        {
            var xmlToreturn;
            console.log('SImpleLoader.loadXML: '+url)

             $.ajax({
                url: url,
                dataType: "xml",
                method: 'POST',
                async: false,
                success: function(data)
                {
                    console.log('SImpleLoader.loadXML > Success: '+url);
                    xmlToreturn = data;
                },
                error: function (request, status, error) {
                        console.log('SImpleLoader.loadXML > Error: '+request.responseText);
                        alert("SimpleLoader > LoadXML > Error: "+request.responseText);
                }
            });

            return xmlToreturn;
        }

        /**
         * Simple JSON load
         * url
         * Error handler
         */
        this.loadJson = function(url)
        {
            var toreturn;

            $.ajax({
                url: url,
                dataType: "json",
                method: 'POST',
                async: false,
                success: function(data)
                {
                    toreturn = data;
                },
                error: function (request, status, error) {
                    alert("SimpleLoader > LOADJSON > Error: "+request.responseText);
                }
            });

            return toreturn;
        }
    }
})();