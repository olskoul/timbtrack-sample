/**
 * Created by jonathan on 13/03/14.
 */
var UrlUtils = (function () {

    /* --------------------------------- Properties -------------------------------- */
    var instance;  // our instance holder



    /* --------------------------------- SINGLETON  -------------------------------- */
    var _static  = {

        // an emulation of static variables and methods

        /**
         * Method for getting an instance. It returns
         * a singleton instance of a singleton object
         */
        getInstance:  function()
        {
            if( instance  ===  undefined )  {
                instance = new Singleton();
            }

            return  instance;
        }

        // Static variable
        //TEMPLATE_MENU : "menu/template_MenuView",
        //TEMPLATE_MARKING : "marking/template_"
    };

    return  _static;

    function Singleton()
    {

        /* --------------------------------- functions -------------------------------- */



        /**
         * Get URL Param
         * @name : ,ame of the wanted param
         * return the value or null
         */
        this.getUrlParam = function(name)
        {
            var results = new RegExp('[\\?&]' + name + '=([^&#]*)').exec(window.location.href);
            if (results==null){
                return null;
            }
            else{
                return results[1] || 0;
            }
        }

        /**
         * Get hash value (#myhash/myindex1Hash/myindex2Hash)
         * @index : index of the wanted value
         * return the value or null
         */
        this.getHashValue = function(index)
        {
            var hash = window.location.hash;
            if (!hash) return null;

            var splittedHash = hash.split("#")[1].split("/");
            return splittedHash[index];
        }


    }
})();