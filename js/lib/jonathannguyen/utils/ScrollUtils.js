/**
 * Created by jonathan on 13/03/14.
 */
var ScrollUtils = (function () {

    /* --------------------------------- Properties -------------------------------- */
    var instance;  // our instance holder



    /* --------------------------------- SINGLETON  -------------------------------- */
    var _static  = {

        // an emulation of static variables and methods

        /**
         * Method for getting an instance. It returns
         * a singleton instance of a singleton object
         */
        getInstance:  function()
        {
            if( instance  ===  undefined )  {
                instance = new Singleton();
            }

            return  instance;
        }

        // Static variable
        //TEMPLATE_MENU : "menu/template_MenuView",
        //TEMPLATE_MARKING : "marking/template_"
    };

    return  _static;

    function Singleton()
    {

        /* --------------------------------- functions -------------------------------- */



        /**
         * Get URL Param
         * @name : ,ame of the wanted param
         * return the value or null
         */
        this.scrollToElement = function(scrollingView, element)
        {
           //$(scrollingView).scrollTop($(scrollingView).scrollTop()+$(element).position().top);

            var position = $(scrollingView).scrollTop()+$(element).position().top;

            $(scrollingView).animate({
                scrollTop: position
            }, 800, function(){
                $(scrollingView).clearQueue();
            });
        }



    }
})();