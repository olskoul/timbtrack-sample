/**
 * Created by jonathan on 13/03/14.
 */
var BtnUtils = (function () {

    /* --------------------------------- Properties -------------------------------- */
    var instance;  // our instance holder



    /* --------------------------------- SINGLETON  -------------------------------- */
    var _static  = {

        /**
         * Hide or show validate btn
         * @flag true or false
         */
        activateBtn : function (target, flag, withAlpha)
        {
            var alphaOff = (withAlpha)?.5:1;
            var visibility = (flag)? 1:alphaOff;
            var pointerEvent = (flag)? 'auto':'none';
            $(target).css('opacity',visibility);
            $(target).css('pointer-events',pointerEvent);
        }
    };

    return  _static;

    function Singleton()
    {

        /* --------------------------------- functions -------------------------------- */






    }
})();