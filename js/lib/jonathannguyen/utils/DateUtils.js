/**
 * Created by jonathan on 13/03/14.
 */
var DateUtils = (function () {

    /* --------------------------------- Properties -------------------------------- */
    var instance;  // our instance holder



    /* --------------------------------- SINGLETON  -------------------------------- */
    var _static  = {

        // an emulation of static variables and methods

        /**
         * Method for getting an instance. It returns
         * a singleton instance of a singleton object
         */
        getInstance:  function()
        {
            if( instance  ===  undefined )  {
                instance = new Singleton();
            }

            return  instance;
        },

        /**
         * Get String date ex: 19/05/2014 (21:52:16)
         * @name : ,ame of the wanted param
         * return the value or null
         */
        getStringDateFromTimeStamp : function(timeStamp)
        {
            var date = new Date(parseInt(timeStamp));
            var stringDate =  date.getDate() + "/" + parseInt(date.getMonth()+1) + "/" + date.getFullYear() +" ("+date.getHours()+":"+date.getMinutes()+":"+date.getSeconds()+")";
            return stringDate;
        }

        // Static variable
        //TEMPLATE_MENU : "menu/template_MenuView",
        //TEMPLATE_MARKING : "marking/template_"
    };

    return  _static;

    function Singleton()
    {

        /* --------------------------------- functions -------------------------------- */







    }
})();