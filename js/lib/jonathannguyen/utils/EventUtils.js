/**
 * Created by jonathan on 13/03/14.
 */
var EventUtils = (function () {

    /* --------------------------------- Properties -------------------------------- */
    var instance;  // our instance holder



    /* --------------------------------- SINGLETON  -------------------------------- */
    var _static  = {

        // an emulation of static variables and methods

        /**
         * Method for getting an instance. It returns
         * a singleton instance of a singleton object
         */
        getInstance:  function()
        {
            if( instance  ===  undefined )  {
                instance = new Singleton();
            }

            return  instance;
        },

        // Static variable
        //TEMPLATE_MENU : "menu/template_MenuView",
        //TEMPLATE_MARKING : "marking/template_"

        /**
         * Remove listener
         * @element : element <p> or anything else
         * Clone the element and replace it with the clone
         * only way to remove events form element
         */
        removeListener : function(element, nullify)
        {
            if(element == undefined || element == null)
            return null;

            var clone = element.cloneNode(true);

            if(element.parentNode != null)
            element.parentNode.replaceChild(clone, element);

            element = null;

            if(nullify != true)
            return clone;
            else
            {
                clone.remove();
                clone = null;
            }
        }
    };

    return  _static;

    function Singleton()
    {

        /* --------------------------------- functions -------------------------------- */








    }
})();