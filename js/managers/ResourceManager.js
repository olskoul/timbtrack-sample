/**
 * Created by jonathan on 18/02/14.
 */
var ResourceManager = (function () {

    /* --------------------------------- Properties -------------------------------- */
    var instance;  // our instance holder
    var rawResources; //

    /* --------------------------------- SINGLETON  -------------------------------- */
    var _static  = {

        // an emulation of static variables and methods

        /**
         * Method for getting an instance. It returns
         * a singleton instance of a singleton object
         */
        getInstance:  function()
        {
            if( instance  ===  undefined )  {
                instance = new Singleton();
            }

            return  instance;
        },

        getJson: function()
        {
            if(rawResources == undefined || !rawResources[Infos.lang])
            {
                console.warn("ResourceManager > getJson > rawResource is not defined");
                return null;
            }
            return rawResources[Infos.lang]
        },

        getString: function($key)
        {
            if(!rawResources[Infos.lang])
            {
                console.warn("ResourceManager > getJson > rawResource is not defined");
                return "? "+$key+" ?";
            }
            else if(rawResources[Infos.lang][$key] == undefined)
            return "? "+$key+" ?";
            else
            return rawResources[Infos.lang][$key];
        }


    };

    return  _static;

    function Singleton()
    {

        /* --------------------------------- functions -------------------------------- */

        /**
         * Init function
         */
        this.init = function($data)
        {
            console.log('ResourceManager > init');
           /* $data['nl-be'] = {};
            $data['en-us'] = {};*/
            rawResources = $data;
        }


        /* ---------------------------------- PRIVATE methods ---------------------------------- */


    }
})();