/**
 * Created by jonathan on 18/02/14.
 */
var TemplateManager = (function () {

    /* --------------------------------- Properties -------------------------------- */
    var TEMPLATE_PATH = 'templates';
    var instance;  // our instance holder
    var runTimeCache = {};



    /* --------------------------------- SINGLETON  -------------------------------- */
    var _static  = {

        // an emulation of static variables and methods

        /**
         * Method for getting an instance. It returns
         * a singleton instance of a singleton object
         */
        getInstance:  function()
        {
            if( instance  ===  undefined )  {
                instance = new Singleton();
            }

            return  instance;
        },

        TEMPLATE_LOGIN                              : "login/template_LoginView",
        TEMPLATE_HOME                               : "home/template_HomeView",
        TEMPLATE_MAP                                : "map/template_Map",
        TEMPLATE_GOTO                                : "map/template_Goto",
        TEMPLATE_MARKING_NEW_GROUP                  : "marking/template_Marking_NewGroup",
        TEMPLATE_MARKING                            : "marking/template_Marking",
        TEMPLATE_MARKING_NEW_GROUP                  : "marking/template_Marking_NewGroup",
        TEMPLATE_MARKING_KIND                       : "marking/template_Marking_Kind",
        TEMPLATE_MARKING_GPS                        : "marking/template_Marking_Gps",
        TEMPLATE_MARKING_DETAILS_FOOT               : "marking/template_Marking_Details_Foot",
        TEMPLATE_MARKING_DETAILS_WOOD               : "marking/template_Marking_Details_Wood",
        TEMPLATE_MARKING_DETAILS_WOOD_SUMMARY       : "marking/template_Marking_Details_Wood_Summary",
        TEMPLATE_MARKING_DETAILS_WOOD_BY_VOLUME     : "marking/template_Marking_Details_Wood_ByVolume",
        TEMPLATE_MARKING_DETAILS_GROUND             : "marking/template_Marking_Details_Ground",
        TEMPLATE_MARKING_END                        : "marking/template_Marking_End",
        TEMPLATE_NOTE                               : "notes/template_Note",
        TEMPLATE_SETTINGS                           : "settings/template_Settings",
        TEMPLATE_SETTINGS_HOME                      : "settings/template_settings_home",
        TEMPLATE_SETTINGS_LANGUAGE                  : "settings/template_settings_language",
        TEMPLATE_SETTINGS_PRIVACY                   : "settings/template_settings_privacy",
        TEMPLATE_FAQ                                : "faq/template_Faq",
        TEMPLATE_MANAGE                             : "manage/template_Manage",
        TEMPLATE_EMPTY                             : "template_empty",
        TEMPLATE_HEADER_DEFAULT                     : "header/template_Header_Default",
        TEMPLATE_POPUP_NEW_BATCH                     : "popup/template_popup_NewBatch"
    };

    return  _static;

    function Singleton()
    {

        /* --------------------------------- functions -------------------------------- */

        /**
         * Init function
         */
        this.init = function()
        {

        }

        /**
         * Render a html template with data as a string
         * Also check for cache before loading TODO: Check this functionality
         */
        this.getTemplate = function(tmpl_name, tmpl_data, doNotUseCache) {

           console.log("TemplateManager > getTemplate > "+ tmpl_name);

            if ( !runTimeCache ) {
                runTimeCache = {};
            }

            if (doNotUseCache == true || !runTimeCache[tmpl_name] )
            {

                var tmpl_url = TEMPLATE_PATH + '/' + tmpl_name + '.html';
                var tmpl_string;

                $.ajax({
                    url: tmpl_url,
                    method: 'GET',
                    async: false,
                    success: function(data) {
                        tmpl_string = data;
                    }
                });

                runTimeCache[tmpl_name] = Handlebars.compile(tmpl_string);  //_.template(tmpl_string);
            }
            //add resource object to the template data //TODO: Check if this is a good idea (perf,memory etc..)
            if(!tmpl_data)
                tmpl_data = {};

            tmpl_data['_r'] = ResourceManager.getJson();
            tmpl_data['_version'] = Infos.version;
            return runTimeCache[tmpl_name](tmpl_data);
        }
    }
})();