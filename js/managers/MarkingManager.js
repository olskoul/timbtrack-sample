/**
 * Created by jonathan on 18/02/14.
 */
var MarkingManager = (function () {

    /* --------------------------------- Properties -------------------------------- */
    var instance;  // our instance holder
    var groups = {}; // group repository
    var markings = {}; // marking repository
    var batches = {}; // batch repository
    var currentGroup; // current edited groupVo
    var currentMarking; // current edited markingVo

    /* --------------------------------- SINGLETON  -------------------------------- */
    var _static  = {

        // an emulation of static variables and methods

        /**
         * Method for getting an instance. It returns
         * a singleton instance of a singleton object
         */
        getInstance:  function()
        {
            if( instance  ===  undefined )  {
                instance = new Singleton();
            }

            return  instance;
        },

        //statics
        STEP_NEW_GROUP              : "new",
        STEP_KIND                   : "kind",
        STEP_GPS                    : "gps",
        STEP_DETAILS                : "details",
        STEP_END                    : "end",
        STEP_KIND_FOOT              : "foot",
        STEP_KIND_GROUND            : "ground",
        STEP_KIND_WOOD              : "wood",
        STEP_KIND_WOOD_SUMMARY      : "woodsummary",
        STEP_KIND_WOOD_BY_VOLUMES   : "woodbyvolume",

        BATCH_TYPE_INVENTORY        : "inventory",
        BATCH_TYPE_STOCK            : "stock"

    };

    return  _static;

    function Singleton()
    {

        /* --------------------------------- functions -------------------------------- */

        /**
         * Init function
         */
        this.init = function()
        {

        }


        /**-----------------------------------------------------------------------*/
        /**
         *  GROUPS
         */
        /**-----------------------------------------------------------------------*/

        /**
         * Creates a  new group of marking
         * @$groupName : custom Name for new group VO
         * Returns GroupVo
         */
        this.createNewGroup = function($groupName)
        {
            var id = new Date().getTime();
            console.log("MarkingManager > Create new group: > "+ id);

            var group = new GroupVo();
            group.id = id;
            group.customName = $groupName;
            //groups[id] = group;

            return group;
        }

        /**
         * Set groupVo as current edited group
         * @$groupVo : groupVo to set as currentGroup
         * Returns GroupVo
         */
        this.setCurrentGroup = function($groupVo)
        {
            if($groupVo != undefined && $groupVo != null)
            {
                currentGroup = $groupVo;
                return $groupVo;
            }
            else
            {
                alert("Group is null or undefined -> this should not happen");
                ServicesManager.getInstance().mail("WARNING > MarkingManager.setCurrentGroup > Group is null or undefined",true);
                currentGroup = null;
                return null;
            }
        }

        /**
         * Get current edited group
         * Returns currentGroup (GroupVo)
         */
        this.getCurrentGroup = function()
        {
            return currentGroup;
        }

        /**
         * Clear current edited group
         * Returns currentGroup (null)
         */
        this.clearCurrentGroup = function()
        {
            currentGroup = null;
            return currentGroup;
        }

        /**
         * Insert groupVo into Groups repository
         */
        this.registerGroup = function($groupVo)
        {
            console.log("Registering group: "+$groupVo.id +"("+$groupVo.customName+")");
            groups[$groupVo.id] = $groupVo;
        }

        /**
         * Get groups by id
         */
        this.getGroupById = function($groupId)
        {
            return groups[$groupId];
        }

        /**
         * Delete group
         */
        this.deleteGroup = function($groupId)
        {
            delete groups[$groupId];
        }

        /**-----------------------------------------------------------------------*/
        /**
         *  MARKINGS
         */
        /**-----------------------------------------------------------------------*/

        /**
         * Creates a new marking VO for a given groupName
         * return new marking
         */
        this.createNewMarking = function($kind)
        {
            console.log("MarkingManager > Create new Marking with kind > "+ $kind);

            var marking = new MarkingVo();
            marking.kind =  $kind;

            return marking;
        }

        /**
         * Set MarkingVo as current edited marking
         * @$markingVo : Markingvo to set as currentMarking
         * Returns markingVo
         */
        this.setCurrentMarking = function($markingVo)
        {
            if($markingVo != undefined && $markingVo != null)
            {
                currentMarking = $markingVo;
                return $markingVo;
            }
            else
            {
                alert("Marking is null or undefined -> this should not happen");
                ServicesManager.getInstance().mail("WARNING > MarkingManager.setCurrentMarking > Marking is null or undefined", true);
                currentMarking = null;
                return null;
            }
        }

        /**
         * Get current edited marking
         * Returns currentMarking (MarkingVo)
         */
        this.getCurrentMarking = function()
        {
            return currentMarking;
        }

        /**
         * Clear current edited marking
         * Returns currentMarking (null)
         */
        this.clearCurrentMarking = function()
        {
            currentMarking = null;
            return currentMarking;
        }

        /**
         * Insert groupVo into Groups repository
         */
        this.registerMarking = function($markingVo, $groupVo)
        {
            console.log("Registering Marking: "+$markingVo.id);
            //Add it to the markings repository
            markings[$markingVo.id] = $markingVo;
            //Add it to the given group
            var index = this.addMarkingToGroup($markingVo, $groupVo);
            //Add it to the default batch
            this.addMarkingToBatch($markingVo,WoodlandManager.getInstance().getUserWoodland().batch)
            return index;
        }

        /**
         * Deletes a marking VO for a given groupID
         */
        this.deleteMarking = function($markingVo)
        {
            console.log("MarkingManager > delete marking:"+$markingVo.id);

            this.clearMarkingFromAnyOtherGroup($markingVo);
            this.clearMarkingFromAnyOtherBatch($markingVo);

            if(markings[$markingVo.id] != undefined && markings[$markingVo.id] != null)
            delete markings[$markingVo.id];

        }

        /**
         * Insert MarkingVo id into GroupVo
         */
        this.addMarkingToGroup = function($markingVo, $groupVo)
        {
            console.log("addMarkingToGroup -> marking: "+$markingVo.id+" into group:"+ $groupVo.id);
            for(var i=0 ; i<$groupVo.markings.length ; i++)
            {
                var markingId = $groupVo.markings[i];
                if(markingId == $markingVo.id)
                {
                    return i+1;
                }
            }
            $groupVo.markings.push($markingVo.id);
            return $groupVo.markings.length;
        }

        /**
         * Creates a new marking VO for a given groupName
         */
        /*this.getMarking = function($groupId, $index)
        {
           if(groups[$groupId] != undefined)
           {
               return groups[$groupId].markings[$index];
           }
           else
            return null;
        }*/

        /**
         * Creates a new marking VO for a given groupName
         */
        this.getMarkingById = function($Id)
        {
            if(markings[$Id] != undefined)
            {
                return markings[$Id];
            }
            else
                return null;
        }

        /**
         * Get marking index in group
         */
        this.getMarkingIndex = function($markingId, $groupId)
        {
            if(groups[$groupId] == undefined)
            return null;

            var groupVo = groups[$groupId];
            for(var i=0; i<groupVo.markings.length; i++)
            {
                if($markingId == groupVo.markings[i])
                return i+1;
            }
        }



        /**
         * Delete Marking
         */
        /*this.deleteMarking = function($MarkingId)
        {
            delete markings[$MarkingId];
        }*/





        /**
         * Create batch
         */
        this.createBatch = function($customName, $isInventory, $isDefault)
        {
            console.log('create new batch');

            var batchId = new Date().getTime();
            var batchVo = new BatchVo(batchId);
            batchVo.name = $customName;
            batchVo.isDefault = false;//$isDefault;
            batchVo.type = ($isInventory)?MarkingManager.BATCH_TYPE_INVENTORY:MarkingManager.BATCH_TYPE_STOCK;
            batches[batchId] = batchVo;

            console.log('New batch id is: '+batchId);

            return batchId;
        }

        /**
         * get batch by id
         */
        this.getBatchById = function($batchId)
        {
            return batches[$batchId];
        }

        /**
         * delete batch
         */
        this.deleteBatch = function($batchId)
        {
            console.log('Deleting batch '+$batchId);

            if(batches[$batchId] != undefined)
            {
                //delete batches[$batchId];
                batches[$batchId].sync = SyncManager.SYNC_DELETE;
                console.log('Batch '+$batchId+' set to sync deleted');
            }
        }

        /**
         * get batch by woodland
         */
        this.getDefaultBatchByWoodland = function($woodlandVo)
        {
            return batches[$woodlandVo.batch];
        }

        /**
         * get current user's woodland default batch
         */
        this.getUserDefaultBatch = function()
        {
            return batches[WoodlandManager.getInstance().getUserWoodland().batch];
        }



        /**
         * change batch
         */
        /*this.changeBatch = function($markingId, $newBatchId)
        {
            var markingVo = markings[$markingId];
            if(markingVo != null && markingVo!= undefined)
            {
                this.removeMarkingFromBatch(markingVo.id,markingVo.batch);
                this.addMarkingToBatch(markingVo,$newBatchId);

            }else
            {
                console.log("MarkingManager > changeBatch > marking does not exist");
            }
        }*/

        /**
         * add Marking from batch
         */
        this.addMarkingToBatch = function($markingVo, $batchId)
        {
            console.log("add Marking To Batch: marking id "+$markingVo.id +" to batch id "+$batchId)
            this.clearMarkingFromAnyOtherBatch($markingVo);

            var batchVo = batches[$batchId];
            if(batchVo != null && batchVo!= undefined)
            {
                //Check if already added to batch
                for(var i=0; i<batchVo.markings.length; i++)
                {
                    if(batchVo.markings[i] == $markingVo.id)
                    {
                        console.log("MarkingManager > addMarkingToBatch > marking already exist in this batch");
                        return;
                    }
                }

                //add marking to batch
                batchVo.markings.push($markingVo.id);
                if(batchVo.isDefault != true)
                    batchVo.sync = (batchVo.sync == SyncManager.SYNC_CREATE)?SyncManager.SYNC_CREATE:SyncManager.SYNC_UPDATE;
                else
                    batchVo.sync = SyncManager.SYNC_READ;

                $markingVo.batch = $batchId;
                $markingVo.sync = ($markingVo.sync == SyncManager.SYNC_CREATE)?SyncManager.SYNC_CREATE:SyncManager.SYNC_UPDATE;
            }
            else
            {
                console.log("MarkingManager > addMarkingToBatch > Batch does not exist");
            }
        }

        /**
         * Clear marking from all batch (security before adding it else where)
         */
        this.clearMarkingFromAnyOtherBatch = function($markingVo)
        {
            $.each(batches, function(i,batch)
            {
                MarkingManager.getInstance().removeMarkingFromBatch($markingVo, batch);
            });

            $markingVo.batch = null;

        }

        /**
         * remove Marking from batch
         */
        this.removeMarkingFromBatch = function($markingVo, $batchVo)
        {
            if($batchVo != null && $batchVo != undefined)
            {
                for(var i=0; i < $batchVo.markings.length; i++)
                {
                    if($batchVo.markings[i] == $markingVo.id)
                    {
                        $batchVo.markings.splice(i,1);
                        return;
                    }
                }
            }
        }

        /**
         * Clear marking from all group (security before adding it else where)
         */
        this.clearMarkingFromAnyOtherGroup = function($markingId)
        {
            $.each(groups, function(i,goup)
            {
                MarkingManager.getInstance().removeMarkingFromGroup($markingId, groups[i].id);
            });

            if(markings[$markingId] != undefined)
                markings[$markingId].group = null;

        }

        /**
         * remove Marking from group
         */
        this.removeMarkingFromGroup = function($markingId, $groupId)
        {
            var groupVo = groups[$groupId];
            if(groupVo != null && groupVo!= undefined)
            {
                for(var i=0; i<groupVo.markings.length; i++)
                {
                    if(groupVo.markings[i] == $markingId)
                    {
                        groupVo.markings.splice(i,1);
                        return;
                    }
                }
            }
        }

        /**
         * Create select options for batches
         * return the select element updated
         */
        this.getBatchSelect = function(select)
        {
            //Empty the select just in case
            select.options.length = 0;

            var option;
            var typeOption

            //create a disable option tag that will serve as first child in the selection
            typeOption = document.createElement('option');
            typeOption.selected = "selected";
            typeOption.value = "";
            typeOption.text = ResourceManager.getString("manage_choose_batch");
            typeOption.disabled = 'disabled';
            select.appendChild(typeOption);

            //create child for new batch
            typeOption = document.createElement('option');
            //typeOption.selected = "selected";
            typeOption.value = "new";
            typeOption.text = ResourceManager.getString("manage_newBatch");
            //typeOption.disabled = 'disabled';
            select.appendChild(typeOption);



            for (var key in batches)
            {
                var batchVo = batches[key];
                option = document.createElement('option');
                option.value = batchVo.id;
                option.text = batchVo.name;//ResourceManager.getJson()['common_batch'] + "" + batchVo.id;
                select.appendChild(option);
            }

        }

        /**
         * Get batches object
         */
        this.getbatches = function()
        {
            return batches;
        }

        /**
         * Get markings object
         */
        this.getMarkings = function()
        {
            return markings;
        }

        /**
         * Get groups object
         */
        this.getGroups = function()
        {
            return groups;
        }

        /**
         * update Repository
         * Construct/update Vo's and Repositories
         */

        this.updateRepository = function($data)
        {
            console.log("Marking Manager > updateRepository");

            //fill markings
            markings = {}; // empty repo
            if($data.markings != undefined && $data.markings.length > 0)
            {
                console.log("Total marking amount: "+$data.markings.length);
                for(var i = 0; i<$data.markings.length ; i++ )
                {
                    var obj = $data.markings[i];
                    var markingVo = new MarkingVo();
                    markingVo.fillData(obj);
                    markings[markingVo.id] = markingVo; //add it to repo
                }
            }


            //fill groups
            groups = {};
            if($data.groups != undefined && $data.groups.length > 0)
            {
                console.log("Total groups amount: "+$data.groups.length);
                for(var i = 0; i<$data.groups.length ; i++ )
                {
                    var obj = $data.groups[i];
                    var groupVo = new GroupVo();
                    groupVo.fillData(obj);
                    groups[groupVo.id] = groupVo; //add it to repo
                }
            }

            //fill batches
            batches = {};
            if($data.batches != undefined && $data.batches.length > 0)
            {
                console.log("Total batches amount: "+$data.batches.length);
                for(var i = 0; i<$data.batches.length ; i++ )
                {
                    var obj = $data.batches[i];
                    var batchVo = new BatchVo();
                    batchVo.fillData(obj);
                    batches[batchVo.id] = batchVo; //add it to repo
                }
            }

           //Create Default Batch if doesn't exist
            if($data.batches.length < 1)
            {
                var obj = {
                    "id":0,
                    "customName":"default_batch",//ResourceManager.getString("default_batch"),
                    "markings":[

                    ],
                    "type":"inventory",
                    isDefault:true
                };
                var batchVo = new BatchVo();
                batchVo.fillData(obj);
                batches[batchVo.id] = batchVo;
            }


        }

        /**
         * Generate data for 'manage' html template
         */

        this.generateTemplateData = function()
        {
            var data = {};

            //Groups
            data.groups = [];
            for (var key in groups)
            {
                var groupVo = groups[key];
                if(groupVo.sync != SyncManager.SYNC_DELETE)
                data.groups.push(groupVo);
            }

            //Batches
            data.batches = {};
            data.batches.stock = [];
            data.batches.inventory = [];
            for (var key in batches)
            {
                var batchVo = batches[key];
                if(batchVo.sync != SyncManager.SYNC_DELETE)
                {
                    if(batchVo.type == MarkingManager.BATCH_TYPE_STOCK)
                    {
                        batchVo.priceForTemplate = batchVo.getPrice();
                        data.batches.stock.push(batchVo);
                    }
                    else
                    {
                        data.batches.inventory.push(batchVo);
                    }
                }
            }

            return data;
        }
    }
})();