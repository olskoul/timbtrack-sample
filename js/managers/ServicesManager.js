/**
 * Created by jonathan on 18/02/14.
 */
var ServicesManager = (function () {

    /* --------------------------------- Properties -------------------------------- */
    //var devURL = "assets/jsons/dev/";
    var serverURL = "http://api.testing.timbtrack.com/";
    var devURL = "http://smileasyoudoit.com/customers/timbtrack/handlers/";
    var instance;  // our instance holder
    var token = null;
    var mailSentTotal = 0;

    /* --------------------------------- SINGLETON  -------------------------------- */
    var _static  = {

        // an emulation of static variables and methods

        /**
         * Method for getting an instance. It returns
         * a singleton instance of a singleton object
         */
        getInstance:  function()
        {
            if( instance  ===  undefined )  {
                instance = new Singleton();
            }
            return  instance;
        },

        isSuccess:  function($data)
        {
            if($data.result == undefined)
            return false;

            if($data == undefined)
            return false;

          if($data.result.status == 'success')
          return true;
          else
          return false;
        },

        getToken:function()
        {
            return token;
        }



        // Static variable

        //TEMPLATE_MARKING : "marking/template_"
    };

    return  _static;

    function Singleton()
    {

        /* --------------------------------- functions -------------------------------- */

        /**
         * Init function
         */
        this.init = function($dataAdapter)
        {
        }

        /**
         * Set Token
         */
        this.setToken = function($token)
        {
            token = $token;
        }

        /**
         * Get Token
         */
        this.getToken = function()
        {
            return token;
        }

        /**
         * Get Server URL (Dev or not)
         */
        this.getUrl = function($serviceName)
        {
            if(Debug.USE_LOCAL_SERVER_URL)
            return devURL + $serviceName + ".json";
            else
            return serverURL + $serviceName ;
        }

        this.getServerUrl = function()
        {
            return serverURL;
        }

        /**
         * Resources
         */
        this.getResources = function($onSuccess, $onError)
        {
            console.log("ServiceManager > getResources");

            if(Infos.internet())
            {
                var serviceUrl = this.getUrl("ressources");
                var callObj = {
                    url : serviceUrl,
                    data : null,
                    dataType : 'json',
                    contentType: 'application/json',
                    method : 'GET',
                    sendToken:false
                };

                this.call(callObj, $onSuccess, $onError);
            }
            else
            {
                var resources = this.loadResourcesOffline();
                if( resources != null)
                {
                    console.log("ServiceManager > load resources Offline > Success");
                    $onSuccess(resources);
                }
                else
                {
                    console.log("ServiceManager > load resources Offline > Null");
                    $onError("Local Resources are null.");
                }

            }

        }

        /**
         * Resources local
         */
        this.getResourceLocally = function( $onSuccess, $onError)
        {
            var resources = this.loadResourcesOffline();
            if( resources != null)
            {
                console.log("ServiceManager > load resources Offline > Success");
                $onSuccess(resources);
            }
            else
            {
                console.log("ServiceManager > load resources Offline > Null");
                $onError("Local Resources are null.");
            }
        }

        /**
         * oauth
         */
        this.login = function($data, $onSuccess, $onError)
        {
            Loading.showLoading("loading_login");
            if(Infos.internet())
            {
                var serviceUrl = this.getUrl("oauth");

                var data = $data;
                var callObj = {
                    url: serviceUrl,
                    data:JSON.stringify(data),
                    dataType: 'json',
                    contentType: 'application/json',
                    method: 'POST',
                    sendToken:false
                };

                this.call(callObj, $onSuccess, $onError);
            }
            else
            {
                this.loginOffline($data, $onSuccess, $onError);
                //$onError();
            }
        }

        /**
         * login offline
         */
        this.loginOffline = function($data, $onSuccess, $onError)
        {
            console.log("ServiceManager > Login Offline");
            var flag = Infos.localStorage.checkUser($data.username, $data.password);
            if(flag == true)
            {
                console.log("ServiceManager > Login Offline > Success");
                $onSuccess({result:{status:"success"},access_token:"no_token_its_local_login"});
            }
            else
            {
                console.log("ServiceManager > Login Offline > failed");
                var request = {};
                request.status = "Error local login";
                request.statusText = "Wrong username/password, please check the values and try again.";

                $onError(request);
            }
        }

        /**
         * SYNC GET
         */
        this.syncGET = function($onSuccess, $onError)
        {
            console.log("ServiceManager > syncGET");

            if(Infos.internet())
            {
                var serviceUrl = this.getUrl("sync");
                var callObj = {
                    url : serviceUrl,
                    data : null,
                    dataType : 'json',
                    contentType: 'application/json',
                    method : 'GET',
                    sendToken:true
                };

                this.call(callObj, $onSuccess, $onError);
            }
            else
            {
                //this.loadUserDataOffline($data, $onSuccess, $onError);
                alert("Not connected to internet, should be prompt to use local data");
            }

        }

        /**
         * Sync POST
         */
        this.syncPOST = function($data, $onSuccess, $onError)
        {
            console.log("ServiceManager > syncPOST");
            if(Infos.internet())
            {
                var serviceUrl = this.getUrl("sync");

                var data = $data;
                var callObj = {
                    url: serviceUrl,
                    data:JSON.stringify(data),
                    dataType: 'json',
                    contentType: 'application/json',
                    method: 'POST',
                    sendToken:true
                };

                this.call(callObj, $onSuccess, $onError);
            }
            else
            {
                alert("Not connected to internet, should be prompt to use local login");
                $onError();
            }
        }

        /**
         * upload media
         */
        this.uploadMedia = function($mediaVo, $onSuccess, $onError)
        {
            console.log("Uploading media: "+$mediaVo.id);

            var imageURI = $mediaVo.hash;
            var headers={'Authorization':'Bearer '+ServicesManager.getToken()};
            var params = {};
            params.noteid = $mediaVo.noteId;

            console.log("Uploading media: with params: "+JSON.stringify(params));

            var options = new FileUploadOptions();
            options.fileKey="file";
            options.fileName=imageURI.substr(imageURI.lastIndexOf('/')+1);
            options.mimeType="image/jpeg";
            options.chunkedMode = false;
            options.headers = headers;
            options.params = params;

            var serviceUrl = encodeURI(this.getUrl("media"));

            var ft = new FileTransfer();
            ft.upload(imageURI, serviceUrl, $onSuccess, $onError, options, true);

        }

        /**
         * EMAIL
         */
        this.mail = function($msg, isSilent)
        {
            if(Infos.internet())
            {
                if(mailSentTotal <= 2)
                {
                    var serviceUrl = "http://www.smileasyoudoit.com/customers/timbtrack/mail.php";

                    //version
                    $msg = "Version: "+Infos.version+"\n\n"+$msg;
                    //date and hour
                    var errorDate= new Date();
                    var month = parseInt(errorDate.getMonth())+1;
                    var errorDateStr = " "+errorDate.getDate()+ "/" + month + "/" +errorDate.getFullYear() +" ["+errorDate.getHours()+":"+errorDate.getMinutes()+":"+errorDate.getSeconds()+"] ";
                    $msg = "Time: "+errorDateStr+"\n\n"+$msg;

                    var data = {};
                    data.msg = $msg;
                    data.email = Infos.user.email;
                    data.ticket = consoleTicket;
                    data.silent = isSilent;


                    var callObj = {
                        url: serviceUrl,
                        data:data,//JSON.stringify(data),
                        dataType: 'xml',
                        contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
                        method: 'POST',
                        sendToken:false
                    };

                    this.call(callObj, null, null);
                    if(isSilent != true)
                    mailSentTotal ++;
                }

            }
            else
            {
                //todo
                alert("Not connected to internet, should save the error locally and send it later");

            }
        }



        /**
         * Load userData
         */
        this.loadUserDataOffline = function()
        {
            console.log("ServiceManager > load User Data Offline with id: " + Infos.user.id);
            var userData = Infos.localStorage.syncGET(Infos.user.id);
            if( userData != null)
            {
                console.log("ServiceManager > load User Data Offline > Success");
                return (userData);
            }
            else
            {
                console.log("ServiceManager > load User Data Offline > Null");
                return (null);
            }
        }

        /**
         * Load resource offline
         */
        this.loadResourcesOffline = function()
        {
            console.log("ServiceManager > load resources Offline");
            var resources = Infos.localStorage.loadResources();
            return resources;

        }

        /**
         * Call function
         * $callObj
         * -> url
         * -> data (params)
         * -> dataType (json, xml)
         * -> method (POST, GET..)
         */
        this.call = function($callObj, $onSuccess, $onError)
        {
            console.log("Service Manager calls : "+$callObj.url+" with params: "+$callObj.data);
            if(Debug.FORCE_SERVER_CALL_TO_FAIL)
            {
                $callObj.url =  $callObj.url+"fail"
            }
            $.ajax({
                url: $callObj.url,
                contentType: $callObj.contentType,
                dataType: $callObj.dataType,
                method: $callObj.method,
                async: true,
                data:$callObj.data,
                crossDomain: true,
                timeout: 10000,

                beforeSend: function(xhr)
                {
                    if($callObj.sendToken == true)
                    {
                        xhr.setRequestHeader('Authorization','Bearer ' + token);
                    }
                },

                success: function(result)
                {
                    console.log("Service Manager called : "+$callObj.url + " with success!");

                    if(Debug.LOG_SERVICES_RESULTS)
                    {
                        try
                        {
                            console.log("result: "+JSON.stringify(result));
                        }
                        catch(err)
                        {
                            console.log("result: "+result);
                        }
                    }

                    if($onSuccess) {
                        $onSuccess(result);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    Loading.hideLoading();

                    console.log("Service Manager failed to call : "+$callObj.url);
                    console.log("Error: "+jqXHR.status +" "+textStatus);
                    console.log("errorThrown: "+errorThrown);
                    var stringError = JSON.stringify(jqXHR);
                    console.log("stringError: "+ stringError)

                    if($onError)
                    {
                        var message = (jqXHR.responseText != undefined)?jqXHR.responseText:stringError;
                        $onError(message,jqXHR);
                    }
                    else
                    {

                       if(ResourceManager.getJson() != null)
                       {
                           PopupManager.getInstance().openAlertPopup(ResourceManager.getString("Service Error"), ResourceManager.getString("popup_error_"+jqXHR.status));
                       }
                        else
                       {
                           PopupManager.getInstance().openAlertPopup("Service Error", textStatus);
                       }
                    }
                }
            });
        }
    }
})();