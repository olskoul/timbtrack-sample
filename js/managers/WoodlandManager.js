/**
 * Created by jonathan on 18/02/14.
 */
var WoodlandManager = (function () {

    /* --------------------------------- Properties -------------------------------- */
    var instance;  // our instance holder
    var woodlands = {}; //repository
    var sharedWoodlandsIds = []; // ids of shared woodlands
    var woodland; // user's woodland

    /* --------------------------------- SINGLETON  -------------------------------- */
    var _static  = {

        // an emulation of static variables and methods

        /**
         * Method for getting an instance. It returns
         * a singleton instance of a singleton object
         */
        getInstance:  function()
        {
            if( instance  ===  undefined )  {
                instance = new Singleton();
            }

            return  instance;
        },

        THIS_IS_A_STATIC : 0
    };

    return  _static;

    function Singleton()
    {

        /* --------------------------------- functions -------------------------------- */

        /**
         * Init function
         */
        this.init = function()
        {
        }

        /**
         * Get user wooldand
         * return woodlandVo
         */
        this.getUserWoodland = function($id)
        {
            return woodland;
        }

        /**
         * Get wooldand by id
         * @$id
         * return woodlandVo
         */
        this.getWoodlandById = function($id)
        {
            return woodlands[$id];
        }

        /**
         * Get shared wooldands
         * return array
         */
        this.getshareWoodlands = function()
        {
            return sharedWoodlandsIds;
        }

        /**
         * update Repository
         * Construct/update Vo's and Repositories
         */
        this.updateRepository = function($data)
        {
            woodlands = {};
            var woodlandVo = new WoodlandVo();
            woodlandVo.fillData($data.woodland);
            woodlands[$data.woodland.id] = woodlandVo;
            woodland = woodlandVo;

            sharedWoodlandsIds = [];
            for(var i=0; i<$data.sharedWoodlands.length; i++)
            {
                sharedWoodlandsIds.push($data.sharedWoodlands[i]);
            }
        }
    }
})();