/**
 * Created by jonathan on 18/02/14.
 */
var Loading = (function () {

    /* --------------------------------- Properties -------------------------------- */
    var loading;
    var isOpen;
    /* --------------------------------- SINGLETON  -------------------------------- */
    var _static  = {

        // an emulation of static variables and methods

        /**
         * Method to show loading with message
         */
        showLoading:  function($key, notAkey)
        {
            var $msg = (notAkey == true)?$key:ResourceManager.getString($key);
            if(isOpen != true)
            {
                loading = new PopupLoading(new Date().getTime());
                loading.setup($msg);
                loading.open();
                isOpen = true;
            }
            else
            {
                loading.update($msg);
            }

        },

        hideLoading:  function()
        {
            loading.close();
            isOpen = false;
        }



        // Static variable

        //TEMPLATE_MARKING : "marking/template_"
    };

    return  _static;

    function Singleton()
    {


    }
})();