/**
 * Created by jonathan on 18/02/14.
 */
var SyncManager = (function () {

    /* --------------------------------- Properties -------------------------------- */
    var instance;  // our instance holder
    var syncSuccessCallBack; // function to call when sync process is successful
    var syncFailedCallBack; // function to call when sync process has failed
    var currentUserRawData; //Raw JSON

    /* --------------------------------- SINGLETON  -------------------------------- */
    var _static  = {

        // an emulation of static variables and methods

        /**
         * Method for getting an instance. It returns
         * a singleton instance of a singleton object
         */
        getInstance:  function()
        {
            if( instance  ===  undefined )  {
                instance = new Singleton();
            }

            return  instance;
        },

        SYNC_CREATE: "create",
        SYNC_UPDATE: "update",
        SYNC_DELETE: "delete",
        SYNC_READ: "read",

        SYNC_TYPE_NOTES : "note",
        SYNC_TYPE_GROUP : "group",
        SYNC_TYPE_MARKING : "marking",
        SYNC_TYPE_MEDIA : "marking",
        SYNC_TYPE_BATCH : "batch"
    };

    return  _static;

    function Singleton()
    {

        /* --------------------------------- functions -------------------------------- */

        /**
         * Init function
         */
        this.init = function()
        {

        }

        /**
         * Start sync process
         *
         * -> Load local Data
         * -> if null -> LoadUserData
         *
         * -> if local data "last local update" is not 0
         * -> upload changes to server
         * -> Get the result
         * -> Save fresh synch data
         */
        this.startSync = function($onSyncSuccess, $onSyncFailed)
        {
            console.log("SyncManager > Sync");

            //Ref callbacks
            syncSuccessCallBack = $onSyncSuccess;
            syncFailedCallBack = $onSyncFailed;

            //Load Local Data
            var localData = Infos.localStorage.syncGET(Infos.user.email);

            if(localData == null)
            {
                if(Infos.internet() == true)
                {
                    console.log("SyncManager > Sync > No local data, we load the fresh ones");
                    this.loadUserDataForTheFirstTime();
                }
                else
                {
                    console.log("SyncManager > Sync > No local data, no internet, we're fucked");
                    PopupManager.getInstance().openAlertPopup(ResourceManager.getString('ERROR_TITLE_NO_INTERNET'), ResourceManager.getString('ERROR_NO_INTERNET'));
                }

            }
            else
            {
                //Parse it and init Managers
                this.parseUserData(localData);

                //Clean and Verify
                this.cleanAndVerify();

                //check if changes have been made since the last login
                //var timeStamp = localData[Infos.localStorage.LAST_LOCAL_UPDATE_KEY];
                if(this.doWeNeedToSync())
                {
                    console.log("SyncManager > Sync > Changes have been found on local data, let's ask user if he wants to sync them sync them with the server");
                    PopupManager.getInstance().openConfirmPopup(ResourceManager.getString('popup_sync_needed_title'), ResourceManager.getString('popup_sync_needed_desc'), this.yesSyncIt.bind(this), this.noDoNotSyncIt.bind(this));
                    //ServicesManager.getInstance().syncPOST(localData, this.onUserDataLoaded.bind(this),this.fallBackToLocalData.bind(this));
                }
                else if(!Debug.DISABLE_SYNC)
                {
                    //if internet is available, refresh data from server
                    if(Infos.internet())
                    {
                        console.log("SyncManager > Sync > no changes have been found, but we have internet, so let's get fresh ones just in case");
                        this.loadFreshUserData();
                    }
                    else //use local data
                    {
                        console.log("SyncManager > Sync > no changes have been found, no internet, let's use the local data");
                        this.fallBackToLocalData();
                    }
                }
                else
                {
                    console.log("SyncManager > Sync > DISABLE SYNC is set to "+Debug.DISABLE_SYNC);
                    this.fallBackToLocalData();
                }
            }

        }

        /**
         * Do we need to sync?
         */
        this.doWeNeedToSync = function() {

            console.log("SyncManager > doWeNeedToSync");

            var localData = Infos.localStorage.syncGET(Infos.user.email);
            if(localData != null && Infos.internet() == true)
            {
                var timeStamp = localData[Infos.localStorage.LAST_LOCAL_UPDATE_KEY];
                if (!Debug.DISABLE_SYNC && timeStamp != 0 && timeStamp != undefined || Debug.FORCE_SYNC)
                    return true
                else
                    return false
            }
            return false
        }


        /**
         * User say yes to sync its data
         */
        this.yesSyncIt = function() {
            console.log("SyncManager > yesSyncIt");
            var dataToPost = this.generateDataToSync();
            ServicesManager.getInstance().syncPOST(dataToPost, this.onUserDataLoaded.bind(this),this.syncPostError.bind(this));
        }

        /**
         * User say no to sync its data
         */
        this.noDoNotSyncIt = function() {
            console.log("SyncManager > noDoNotSyncIt");

            this.fallBackToLocalData();
        }

        /**
         *SYNC POST ERROR
         */
        this.syncPostError = function(errorString, request)
        {
            console.log("SyncManager > syncPostError > errorString = "+errorString);
            PopupManager.getInstance().openConfirmPopup(ResourceManager.getString('popup_sync_needed_title'), ResourceManager.getString('popup_sync_needed_desc'), this.yesSyncIt.bind(this), this.noDoNotSyncIt.bind(this));
            //continue using local data
            this.fallBackToLocalData();

        }

        /**
         * Call service to get user data
         */
        this.fallBackToLocalData = function()
        {
            console.log("SyncManager > FallBackToLocalData");
            PopupManager.getInstance().openAlertPopup(ResourceManager.getString("popup_sync_needed_title"), ResourceManager.getString("popup_sync_use_local_data_desc"));
            //SyncCompleted!
            this.syncComplete(true);

        }

        /**
         * Call service to get user data for the first time
         * Error is blocking
         */
        this.loadUserDataForTheFirstTime = function()
        {
            console.log("SyncManager > loadUserDataForTheFirstTime");

            var callBackSuccess = this.onUserDataLoaded.bind(this);
            var callBackError = this.errorSyncHandler.bind(this);

            console.log("SyncManager > Before calling loadUserData");
            ServicesManager.getInstance().syncGET(callBackSuccess,callBackError)
        }

        /**
         * Call service to get latest user data
         * Error is not blocking, use local instead
         */
        this.loadFreshUserData = function()
        {
            console.log("SyncManager > loadFreshUserData");

            var callBackSuccess = this.onUserDataLoaded.bind(this);
            var callBackError = this.fallBackToLocalData.bind(this);

            console.log("SyncManager > Before calling loadUserData");
            ServicesManager.getInstance().syncGET(callBackSuccess,callBackError)
        }

        /**
         * Get user data result function
         */
        this.onUserDataLoaded = function($data, isLocalData)
        {
            console.log("SyncManager > onUserDataLoaded");
            console.log("******: "+JSON.stringify($data));


            if(ServicesManager.isSuccess($data) || isLocalData)
            {
                console.log("SyncManager > onUserDataResult > success");

                //If its local data (coming from fallbackOnLocalData) do not save it as fresh data
                if(isLocalData != true)
                {
                    console.log(JSON.stringify($data))
                    Infos.saveUserFreshData(Infos.user.email, $data);
                } //save data locally

                //Parse data
                this.parseUserData($data); //parse data

                //store reference
                currentUserRawData = $data;

                //now upload media
                if(navigator.device != undefined)
                {
                    if(NotesManager.getInstance().getMediaToUpload().length > 0)
                        this.uploadMedias();
                    else
                        this.syncComplete(isLocalData);
                }
                else
                {
                    //SyncCompleted!
                    this.syncComplete(isLocalData);
                }

            }
            else
            {
                console.log("SyncManager > onUserDataLoaded service is not success");
                this.errorSyncHandler(JSON.stringify($data));
            }
        }

        /**
         * Upload media
         */
        this.uploadMedias = function()
        {
            //loading
            Loading.showLoading("loading_upload_media");

            console.log("SyncManager > uploadMedias");
            var mediaVos = NotesManager.getInstance().getMediaToUpload();
            var mediaUploaded = 0;
            var scopedUploadSuccessFunction =  this.loadFreshUserData.bind(this);
            var scopedUploadErrorFunction =  this.errorSyncHandler.bind(this);
            $.each(mediaVos, function(i,mediaVo)
            {
                var relatedNoteId = mediaVo.noteId;
                ServicesManager.getInstance().uploadMedia(mediaVo,
                    function(response)
                    {

                        Loading.hideLoading();

                        console.log("SyncManager > upload success");
                        console.log("**************************************");
                        console.log("**************************************");
                        console.log("**************************************");
                        console.log(JSON.stringify(response));

                        console.log("MediaVo Before override");
                        console.log(JSON.stringify(mediaVo));

                        //override mediaVo data
                       // mediaVo.dbid = mediaVo.id;
                       // mediaVo.hash = mediaVo.url;

                        var noteVo = NotesManager.getInstance().getNoteById(relatedNoteId);
                        noteVo.medias[0] = mediaVo;

                        console.log(mediaVo.hash);

                        mediaUploaded++;
                        if(mediaUploaded == mediaVos.length)
                        {
                            //reset array of media to upload
                            NotesManager.getInstance().resetMediaToUpload();
                            //SyncCompleted let's sync get!
                            scopedUploadSuccessFunction();
                        }
                    },
                    function(error)
                    {
                        //loading
                        Loading.hideLoading();
                        console.log("SyncManager > upload failed");
                        console.log("upload error source " + error.source);
                        console.log("upload error target " + error.target);
                        mediaUploaded++;
                        if(mediaUploaded == mediaVos.length)
                        {
                            //SyncCompleted!
                            scopedUploadErrorFunction("upload failed");
                        }
                    })

            });


        }

        /**
         * Parse user Data
         * Initiate Managers
         */
        this.parseUserData = function($data)
        {
            console.log('SyncManager > parseUserData');
            console.log(JSON.stringify($data));

            //landOwner
            Infos.user.updateData($data);

            //Fill Marking data
            MarkingManager.getInstance().updateRepository($data);

            //Fill Notes
            NotesManager.getInstance().updateRepository($data.notes);

            //Fill Woodland / Areas / shared woodland
            WoodlandManager.getInstance().updateRepository($data);

            //Fill Species
            SpeciesManager.getInstance().updateRepository($data.species,$data.speciesTags);

        }

        /**
         *  Clean and verify after Sync
         */
        this.cleanAndVerify = function()
        {
            console.log("CLEAN AND VERIFY");
            var flagModificationMade = false;

            //Default batch for woodland
            console.log("CLEAN AND VERIFY: Check default batch");
            var defaultBatch = MarkingManager.getInstance().getUserDefaultBatch();
            if(defaultBatch == undefined)
            {
                console.log("CLEAN AND VERIFY: Default batch for user woodland does NOT EXIST");
                throw new Error("Default batch for user woodland does NOT EXIST")
            }
            else
            {
                //Make sure the flag is set to true
                defaultBatch.isDefault = true;
                //Translate its name
                defaultBatch.name = ResourceManager.getString("default_batch");
            }

            //Groups
            console.log("CLEAN AND VERIFY: Check Groups and each marking in it");
            $.each(MarkingManager.getInstance().getGroups(), function(i,group)
            {
                //make sure all markings of the group have the group Id set
                $.each(group.markings, function(i,markingId)
                {
                    var markingVo = MarkingManager.getInstance().getMarkingById(markingId);
                    if(markingVo.group != group.id)
                    {
                        console.log("CLEAN AND VERIFY: marking.group was different, solved. Marking:"+markingVo.id+" group:"+group.id);
                        markingVo.group = group.id;
                        markingVo.sync = (markingVo.sync == SyncManager.SYNC_CREATE)?SyncManager.SYNC_CREATE:SyncManager.SYNC_UPDATE;
                        flagModificationMade = true;
                    }
                });
            });

            //BATCH
            console.log("CLEAN AND VERIFY: Check Batches");
            $.each(MarkingManager.getInstance().getbatches(), function(i,batch)
            {
                //make sure sync status is correct
                if(batch.dbid == null)
                {
                    console.log("CLEAN AND VERIFY: Batch DBID is null, SYNC is set to CREATE");
                    batch.sync = SyncManager.SYNC_CREATE;
                    flagModificationMade = true;
                }

            });

            //Marking
            console.log("CLEAN AND VERIFY: Check Markings");
            $.each(MarkingManager.getInstance().getMarkings(), function(i,marking)
            {
                //make sure tonnage is set
                if(marking.dbid == null && marking.sync != SyncManager.SYNC_CREATE)
                {
                    console.log("CLEAN AND VERIFY: marking.DBID is null, SYNC must be CREATE. Marking:"+marking.id);
                    marking.sync = SyncManager.SYNC_CREATE;
                    flagModificationMade = true;
                }

                //make sure tonnage is set
               if(marking.tonnage == null)
               {
                   console.log("CLEAN AND VERIFY: Tonnage was not set. Marking:"+marking.id);

                   marking.tonnage = marking.getTonnage(true);
                   marking.sync = (marking.sync == SyncManager.SYNC_CREATE)?SyncManager.SYNC_CREATE:SyncManager.SYNC_UPDATE;
                   flagModificationMade = true;
               }

                //Verify marking batch is not null
               if(marking.batch == null)
               {
                   console.log("CLEAN AND VERIFY: marking "+marking.id+" has batch set to "+marking.batch);
                   marking.batch = defaultBatch.id;
                   MarkingManager.getInstance().addMarkingToBatch(marking,defaultBatch.id);
                   marking.sync = (marking.sync == SyncManager.SYNC_CREATE)?SyncManager.SYNC_CREATE:SyncManager.SYNC_UPDATE;
                   flagModificationMade = true;
               }

                //verify batches existence
                var batchVo = MarkingManager.getInstance().getBatchById(marking.batch);
                if(batchVo == null || batchVo == undefined)
                {
                    console.log("CLEAN AND VERIFY: marking "+marking.id+" has batch set to "+marking.batch+" -> But this bth does not exist");
                    marking.batch = defaultBatch.id;
                    MarkingManager.getInstance().addMarkingToBatch(marking,defaultBatch.id);
                    marking.sync = (marking.sync == SyncManager.SYNC_CREATE)?SyncManager.SYNC_CREATE:SyncManager.SYNC_UPDATE;
                    flagModificationMade = true;
                }
            });

            /*if(flagModificationMade == true)
            Infos.saveUserData();*/

        }

        /**
         * Generate Data to sync
         */
        this.generateDataToSync = function()
        {
            var obj = {};
            obj.partial = true;

            //markings
            obj.markings = this.convertObjToArray(MarkingManager.getInstance().getMarkings(), true, true);

            //batches
            obj.batches = this.convertObjToArray(MarkingManager.getInstance().getbatches(), true);

            //groups
            obj.groups = this.convertObjToArray(MarkingManager.getInstance().getGroups(), true);

            //Notes
            obj.notes = this.convertObjToArray(NotesManager.getInstance().getNotes(), true);

            return obj;

        }

        /**
         * Add Batches related to marking to sync data
         * verify the batch has not been added already
         */
        this.addRelatedBatches = function(obj)
        {
            var batchesIdsToAdd = [];
            for(var i=0; i<obj.markings.length ; i++)
            {
                var markingVo = obj.markings[i];
                if(markingVo.batch != null)
                batchesIdsToAdd.push(markingVo.batch);
            }

            for(var i=0; i<batchesIdsToAdd.length ; i++)
            {
                var id = batchesIdsToAdd[i];
                var addIt = true;
                for(var y=0; y<obj.batches.length ; y++)
                {
                    var alreadyAddedId = obj.batches[y];
                    if(alreadyAddedId == id)
                    {
                        addIt = false;
                        continue;
                    }
                }
                if(addIt)
                {
                    var batchVo = MarkingManager.getInstance().getBatchById(id);
                    obj.batches.push(batchVo);
                }
            }
            return obj;
        }

        /**
         * Generate JSON to save
         */
        this.generateJSonToSave = function()
        {
            var obj = {};

            obj.lastUpdate = new Date().getTime();
            //obj.lastSync = Infos.user.lastUpdate;

            //land owner
            obj.landowner = {};
            obj.landowner.firstName = Infos.user.firstname;
            obj.landowner.lastName = Infos.user.lastname;
            obj.landowner.email = Infos.user.email;

            //Woodland
            var woodlandVo = WoodlandManager.getInstance().getUserWoodland();
            obj.woodland = woodlandVo;
            obj.sharedWoodlands = WoodlandManager.getInstance().getshareWoodlands();

            //Areas
            obj.areas = {}; //todo handle areas when receive infos from Martin

            //batches
            obj.batches = this.convertObjToArray(MarkingManager.getInstance().getbatches());

            //groups
            obj.groups = this.convertObjToArray(MarkingManager.getInstance().getGroups());

            //Notes
            obj.notes = this.convertObjToArray(NotesManager.getInstance().getNotes());

            //Species
            obj.species = this.convertObjToArray(SpeciesManager.getInstance().getSpecies());
            obj.speciesTags = this.convertObjToArray(SpeciesManager.getInstance().getSpeciesTags());

            //markings
            obj.markings = this.convertObjToArray(MarkingManager.getInstance().getMarkings());

            //Resources
            //obj.resources = ResourceManager.getJson();


            return obj;

        }

        /**
         * Convert to Array
         */
        this.convertObjToArray = function($obj, toSyncOnly, isMarking)
        {
            var array  =[];
            for (var key in $obj)
            {
                var prop = $obj[key];
                if(toSyncOnly)
                {

                    if(prop.sync != SyncManager.SYNC_READ)
                    {
                        array.push(prop);
                    }
                }
                else
                    array.push(prop);
            }
            return array;
        }

        /**
         * Sync complete handler
         */
        this.syncComplete = function(isLocalData)
        {
            console.log('SyncManager > SyncComplete');

            //loading
            Loading.hideLoading();

            if(isLocalData != true)
            {
                PopupManager.getInstance().openAlertPopup(ResourceManager.getString("popup_sync_needed_title"), ResourceManager.getString("popup_sync_success_desc"));
            }

            if(syncSuccessCallBack != null)
            syncSuccessCallBack();
        }


        /**
         * Error handler
         * While synchronizing data
         */
        this.errorSyncHandler = function(stringError, request)
        {
            Loading.hideLoading();
            syncFailedCallBack(stringError);

        }
    }
})();