/**
 * Created by jonathan on 18/02/14.
 */
var GeolocationManager = (function () {

    /* --------------------------------- Properties -------------------------------- */
    var instance;  // our instance holder
    var geolocationObj;
    var changeListener;
    var errorListener;
    var successCall;
    var errorCall;
    var pointGPS;
    var vo;
    var attempt = 0;
    var maxAttempt = 3;
    var timeout;
    var timeoutValue = 20000;
    var timeStamp;
    var lastMailSentID;


    /* --------------------------------- SINGLETON  -------------------------------- */
    var _static = {

        // an emulation of static variables and methods

        /**
         * Method for getting an instance. It returns
         * a singleton instance of a singleton object
         */
        getInstance: function () {
            if (instance === undefined) {
                instance = new Singleton();
            }

            return  instance;
        },

        ERROR_ACCURACY: "gps_error_accuracy",
        ERROR_NULL_OR_EQUAL_0: "gps_error_null_or_0",

    };

    return  _static;

    function Singleton() {

        /* --------------------------------- functions -------------------------------- */

        /**
         * Init function
         */
        this.init = function ()
        {
            //create a new one
            var options = {
                enableHighAccuracy: true//,
                //timeout: 0.1//,
                //maximumAge: 0
            };

            geolocationObj = new ol.Geolocation({
                projection: 'EPSG:3857',//view.getProjection(),
                tracking: false,
                options: options
            });

            changeListener = geolocationObj.on('change:position', this.success.bind(this));
            errorListener = geolocationObj.on('error', this.error.bind(this));
        }

        /**
         * If geolocation is available
         * Get current position
         * @vo: object scope
         * @pointGPSToFill: pointGPS to fill
         * @onSuccess: success callback
         * @onSuccess: success callback
         * @onError: error callback
         *
         * return a POINTGPS
         * latitude
         * longitude
         * altitude
         * accuracy
         * altitudeAccuracy
         * heading
         * speed
         */
        this.getCurrentPosition = function ($vo, pointGPSToFill, onSuccess, onError, isRetry)
        {
            Loading.showLoading("loading_geolocating");

            //timeout
            clearTimeout(timeout);
            var timeoutErrorFunction = this.error.bind(this);
            timeout = setTimeout(function() {
                timeoutErrorFunction();
            }, timeoutValue);

            if(isRetry != true)
            {
                attempt = 0;
                timeStamp = new Date().getTime();
            }

            successCall = onSuccess;
            errorCall = onError;
            pointGPS = pointGPSToFill;
            vo = $vo;

            geolocationObj.setTracking(true);

        };

        this.success = function ()
        {
            clearTimeout(timeout);

            var coords = geolocationObj.getPosition();

            console.log("GeolocationManager position changed, coords = " + coords);

            //Check for coordinates equal 0 or null
            if(coords == undefined || coords == null || coords[0] == 0 || coords[1] == 0)
            {
                console.log("GeolocationManager success BUT coords is null or equal to 0");
                this.error("Coords is null or equal to 0");
                return;
            }

            //Increase attempts
            attempt ++;

            //Check accuracy
            var accuracy = geolocationObj.getAccuracy();
            if(accuracy != undefined && accuracy >= 100)
            {
                console.log("GeolocationManager success BUT accuracy is higher than 100");
                this.error("Accuracy is higher than 100m", true);
                return;
            }

            //Fill Point GPS
            pointGPS.fillFromPosition(coords,geolocationObj);

            setTimeout(function()
            {
                Loading.hideLoading();
                successCall(vo,pointGPS);
            }, 500);

            geolocationObj.setTracking(false);
        }


        this.error = function (msg, isAccuracyError)
        {
            clearTimeout(timeout);
            console.log("GeolocationManager error");
            geolocationObj.setTracking(false);

            //Increase attempts
            attempt ++;

            if(attempt < maxAttempt)
                this.retry();
            else
            {
                if(timeStamp != lastMailSentID && isAccuracyError != true)
                {
                    lastMailSentID = timeStamp;
                    ServicesManager.getInstance().mail("WARNING GPS ERROR.\n\n"+ "timestamp: "+timeStamp + "\n\nMESSAGE:"+ "\n\n" + msg+ "\n\nFULL LOG:\n\n"+ logger.join("\n"),true);
                }

                setTimeout(function()
                {
                    Loading.hideLoading();
                    errorCall(vo,pointGPS, msg);
                }, 500);
            }



        }

        this.retry = function ()
        {
            console.log("GeolocationManager: retry, attempt "+attempt);
            this.getCurrentPosition(vo,pointGPS,successCall,errorCall, true);
        }

    }

/*
*
*
* */


})();