/**
 * Created by jonathan on 18/02/14.
 */
var GeolocationManager2 = (function () {

    /* --------------------------------- Properties -------------------------------- */
    var instance;  // our instance holder
    var geolocationObj;

    /* --------------------------------- SINGLETON  -------------------------------- */
    var _static  = {

        // an emulation of static variables and methods

        /**
         * Method for getting an instance. It returns
         * a singleton instance of a singleton object
         */
        getInstance:  function()
        {
            if( instance  ===  undefined )  {
                instance = new Singleton();
            }

            return  instance;
        },

        EXAMPLE_STATIC_VAR       : "this is an example"

    };

    return  _static;

    function Singleton()
    {

        /* --------------------------------- functions -------------------------------- */

        /**
         * Init function
         */
        this.init = function()
        {
            if(geolocationObj == undefined)
            {
                var options = {
                    enableHighAccuracy: true,
                    timeout: 10000//,
                    //maximumAge: 0
                };
                geolocationObj = new ol.Geolocation({
                    projection: 'EPSG:3857',//view.getProjection(),
                    tracking: true//,
                    //options:options
                });
                var changeListener = geolocationObj.on('change:position', function() {
                    if(geolocationObj != undefined)
                    {
                        console.log("GeolocationManager: " + geolocationObj.getPosition());
                    }
                    //geolocation = undefined;
                });

                var errorListener = geolocationObj.on('error', function() {
                    alert("geolocation error");
                    geolocationObj = undefined;
                    changeListener.remove();
                    errorListener.remove();
                });
            }
        }

        /**
         * If geolocation is available
         * Get current position
         * @onSuccess: success callback
         * @onError: error callback
         *
         * return a object position:
         * latitude
         * longitude
         * altitude
         * accuracy
         * altitudeAccuracy
         * heading
         * speed
         */
        this.getCurrentPosition = function()
        {
            if(geolocationObj == undefined)
            this.init();

            if(geolocationObj != undefined)
                return geolocationObj;
            else
                return null;
        }


    }
})();