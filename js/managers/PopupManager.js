/**
 * Created by jonathan on 18/02/14.
 */
var PopupManager = (function () {

    /* --------------------------------- Properties -------------------------------- */
    var instance;  // our instance holder
    /* --------------------------------- SINGLETON  -------------------------------- */
    var _static  = {

        // an emulation of static variables and methods

        /**
         * Method for getting an instance. It returns
         * a singleton instance of a singleton object
         */
        getInstance:  function()
        {
            if( instance  ===  undefined )  {
                instance = new Singleton();
            }

            return  instance;
        }



        // Static variable

        //TEMPLATE_MARKING : "marking/template_"
    };

    return  _static;

    function Singleton()
    {

        /* --------------------------------- functions -------------------------------- */

        /**
         * Init function
         */
        this.init = function()
        {

        }

        /**
         * Alert popup
         */
        this.openAlertPopup = function($title, $desc, $okCallBack)
        {
            var popup = new PopupAlert(new Date().getTime());
            popup.setup($title,$desc,$okCallBack);
            popup.open();
        }

        /**
         * Confirm popup
         */
        this.openConfirmPopup = function($title, $desc, $yesCallBack, $noCallBack)
        {
            var popup = new PopupConfirm(new Date().getTime());
            popup.setup($title,$desc,$yesCallBack,$noCallBack);
            popup.open();
        }

        /**
         * New batch popup
         */
        this.openNewBatchPopup = function($validationCallback)
        {
            var popup = new PopupNewBatch(new Date().getTime());
            popup.setup($validationCallback);
            popup.open();
        }

        /**
         * Infos marking popup
         */
        this.openInfoMarkingPopup = function($markingVo, showSeeBtn)
        {
            var popup = new PopupMarkingInfo(new Date().getTime());
            popup.setup($markingVo, showSeeBtn);
            popup.open();
        }

        /**
         * Infos input popup
         */
        this.openInfoInputPopup = function($id)
        {
            var popup = new PopupInputInfo(new Date().getTime());
            popup.setup($id);
            popup.open();
        }

    }
})();