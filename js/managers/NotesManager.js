/**
 * Created by jonathan on 18/02/14.
 */
var NotesManager = (function () {


    /* --------------------------------- Properties -------------------------------- */
    var instance;  // our instance holder
    var currentNoteVo;
    var bodyRef =  $('#body-note-wrapper');
    var wrapper =  document.createElement("div"); // element that will wrap all popups
    var title;
    var textarea;
    var photoWrap;
    var photoContainer;
    var photoFilename;
    var deletePhotoBtn;
    var photoInfos;
    var header;
    var typeSelector;
    var okBtn;
    var cancelBtn;
    var photoBtn;
    var closeBtn;
    var buttonWrap = document.createElement("ul");
    var hasPhoto = false;
    var notes = {};
    var callback;
    var currentID = -1;
    var mediaToUpload = [];
    var fullscreenContainer;
    var fullscreenImg;
    var fullscreenClose;

    /* --------------------------------- SINGLETON  -------------------------------- */
    var _static  = {

        // an emulation of static variables and methods
        CATEGORY_0 : "note_cat_0",
        CATEGORY_1 : "note_cat_1",
        CATEGORY_2 : "note_cat_2",
        CATEGORY_3 : "note_cat_3",

        /**
         * Method for getting an instance. It returns
         * a singleton instance of a singleton object
         */
        getInstance:  function()
        {
            if( instance  ===  undefined )  {
                instance = new Singleton();
            }

            return  instance;
        }



        // Static variable

        //TEMPLATE_MARKING : "marking/template_"
    };

    return  _static;

    function Singleton()
    {


        /* --------------------------------- functions -------------------------------- */

        /**
         * init view
         */
        this.init = function()
        {
            console.log('NotesManager > init');

            wrapper = $(wrapper);
            wrapper.html(TemplateManager.getInstance().getTemplate(TemplateManager.TEMPLATE_NOTE));

            buttonWrap = wrapper.find('#note_btns');
            header = wrapper.find('#note_header');
            typeSelector = wrapper.find('#note_type .note-select');
            textarea = wrapper.find('#note_textArea');
            title = wrapper.find('#note_title');
            photoWrap = wrapper.find('#note_photo_placeholder');
            photoContainer = photoWrap.find('#noteImg');
            photoFilename = photoWrap.find('.photo_filename');
            photoInfos = photoWrap.find('.photo_infos');
            closeBtn = wrapper.find('#note_close_btn');
            deletePhotoBtn = wrapper.find('.note_photo_delete');
            fullscreenContainer = wrapper.find('#noteImgFullscreen');
            fullscreenContainer.css('display','none');
            fullscreenImg = fullscreenContainer.children('img');
            fullscreenClose = fullscreenContainer.children('#fullscreen_close_btn');


            deletePhotoBtn[0].addEventListener('click', this.deletePhotoHandler.bind(this));
            closeBtn[0].addEventListener('click', this.cancelHandler.bind(this));
            photoWrap[0].addEventListener('click', this.showFullscreenImage.bind(this));
            fullscreenClose[0].addEventListener('click', this.hideFullscreenImage.bind(this));

            if(cancelBtn == undefined)
            {
                cancelBtn = $(this.createBtn('cancel'));
                cancelBtn[0].addEventListener('click', this.cancelHandler.bind(this));

            }
            if(photoBtn == undefined)
            {
                photoBtn = $(this.createBtn('photo'));
                photoBtn[0].addEventListener('click', this.photoHandler.bind(this));
                //buttonWrap[0].appendChild(photoBtn);
            }
            if(okBtn == undefined)
            {
                okBtn = $(this.createBtn('ok'));
                okBtn[0].addEventListener('click', this.okHandler.bind(this));
                //buttonWrap[0].appendChild(okBtn);
            }
        }

        /**
         * Open note view
         */
        this.openNote = function($id, $callback)
        {
            console.log("NoteManager > openNote");
            console.log("ID: "+$id);

            //Construct view if needed
            if(wrapper == undefined)
                this.init();

            //if id, it means we open to edit an existing note
            if($id != undefined && notes[$id]!= undefined)
            {
                console.log("NoteManager > noteVo found -> let's fill");
                currentID = $id;
                currentNoteVo = notes[$id];

            }
            else
            {
                console.log("NoteManager > no id > create new noteVO");

                if(Debug.USE_FAKE_NOTE != true)
                {
                    currentID = -1;
                    currentNoteVo = new NoteVo();
                }
                else
                {
                    currentID = 10000;
                    currentNoteVo = new NoteVo();
                    currentNoteVo.type = 3;
                    currentNoteVo.content = "this is a fake note with an image!";

                    var media = new MediaVo();
                        media.hash = "assets/images/temp/note.jpg";
                    currentNoteVo.medias.push(media);
                }

            }

            //Setup view (read mode if existing noteVo else write mode)
            this.setup();

            //ref callback
            callback = $callback;

            //Append note to body
            bodyRef.append(wrapper);
            //prepare animation
            $(wrapper).attr("class", "note noteClose");
            //display body wrapper
            bodyRef.css("display", "block");
            //set overflow fo body tag to hidden
            $('body').css('overflow', 'hidden');

            this.positionMe();
            this.open();

        }

        /**
         * Setup view READ  or WRITE mode
         */
        this.setup = function()
        {
            typeSelector.prop('disabled', (currentID == -1)?false:"disabled");
            textarea.prop('disabled', (currentID == -1)?false:"disabled");
            deletePhotoBtn.css('display', (currentID == -1)?'list-item':'none');
            cancelBtn.css('display', (currentID == -1)?'table-cell':'none');
            photoBtn.css('display', (currentID == -1)?'table-cell':'none');

            if(currentID != -1)
                this.fill();
        }

        /**
         * Fill note with noteVo
         */
        this.fill = function()
        {
            console.log("currentNoteVo: "+JSON.stringify(currentNoteVo));
            textarea.val(currentNoteVo.content);
            hasPhoto = (currentNoteVo.medias.length > 0);
            console.log("currentNoteVo has media: "+hasPhoto);

            for(var i= 0; i < typeSelector.length; i++)
            {
                if(typeSelector[0][i].value == currentNoteVo.type)
                    typeSelector[0].selectedIndex = i;
            }

            if(hasPhoto)
            this.showPhotoPlaceHolder();
        }

        /**
         * Apply correct position to elements
         */
        this.positionMe = function()
        {
            if(!hasPhoto)
                this.hidePhotoPlaceHolder();

            var texteAreaBottom  = (hasPhoto)?Math.round(buttonWrap.outerHeight() + photoWrap.outerHeight()):Math.round(buttonWrap.outerHeight());

            textarea.css('top', Math.round(header.outerHeight())+ typeSelector.outerHeight() + 10);
            textarea.css('bottom', texteAreaBottom);

            var fullwidth = Math.round($(window).width());
            var selectorwidth = typeSelector.outerWidth();
            var typeSelectorLeft = Math.round( (fullwidth - selectorwidth)/2 );
            typeSelector.parent().css('top', Math.round(header.outerHeight()) + 5);
            typeSelector.css('left', typeSelectorLeft);

        }

        /**
         * Open fullscreen
         */
        this.showFullscreenImage = function()
        {
            fullscreenContainer.css('display','block');
            var url = (currentNoteVo.dbid == null)?currentNoteVo.medias[0].hash:ServicesManager.getInstance().getServerUrl()+currentNoteVo.medias[0].hash;
            fullscreenImg.attr('src',url);

            var imgWidth = fullscreenImg.outerWidth();
            var imgheight = fullscreenImg.outerHeight();
            var fullwidth = Math.round($(window).width());
            var fullheight = Math.round($(window).height());
            var ratio;
            var scaledEdge;

            if(imgWidth > imgheight)
            {
                ratio = fullwidth/imgWidth;
                scaledEdge = imgheight*ratio;
                fullscreenImg.attr('width', fullwidth );
                fullscreenImg.css('top', Math.round((fullheight - scaledEdge)*0.5));
            }
            else
            {
                ratio = fullheight/imgheight;
                scaledEdge = imgWidth*ratio;
                if(scaledEdge > fullwidth)
                {
                    ratio = fullwidth/scaledEdge;
                    scaledEdge = scaledEdge*ratio;
                }
                fullscreenImg.attr('width', scaledEdge );
                fullscreenImg.css('left', Math.round((fullwidth - fullscreenImg.outerWidth())*0.5));
                fullscreenImg.css('top', Math.round((fullheight - fullscreenImg.outerHeight())*0.5));
            }
        }

        /**
         * close fullscreen
         */
        this.hideFullscreenImage = function()
        {
            fullscreenContainer.css('display','none');
            fullscreenImg.attr('src','');
        }

        /**
         * Open popup
         */
        this.open = function()
        {
            $(wrapper).attr("class", "note noteTransition noteOpen");
        }

        /**
         * Delete photo handler
         */
        this.deletePhotoHandler = function()
        {
            console.log("NoteManage > deletePhotoHandler")
            hasPhoto = false;
            currentNoteVo.medias[0] = null;
            currentNoteVo.medias = [];

            this.hidePhotoPlaceHolder();
        }

        /**
         * OK btn handler
         */
        this.okHandler = function()
        {
            console.log("NoteManage > okHandler")
            this.saveNote();
        }

        /**
         * OK btn handler
         */
        this.saveNote = function()
        {
            console.log("NoteManage > saveNote");

            if(Debug.USE_FAKE_NOTE != true) {
                currentNoteVo.content = textarea.val();
                currentNoteVo.type = typeSelector[0][typeSelector[0].selectedIndex].value;
                notes[currentNoteVo.id] = currentNoteVo;
            }

            Debug.USE_FAKE_NOTE = false;
            this.close();

            if(callback && currentID == -1)
            callback(currentNoteVo);
        }

        /**
         * CANCEL btn handler
         */
        this.cancelHandler = function()
        {
            console.log("NoteManage > CancelHandler")
            this.close();
        }

        /**
         * PHOTO btn handler
         */
        this.photoHandler = function()
        {
            console.log("NoteManage > PhotoHandler");
            console.log("navigator.device: "+navigator.device);
            if(navigator.device != undefined)
            {
                //navigator.device.capture.captureImage(captureSuccess.bind(this), captureError, {limit:1});
                var options = {};
                options.quality = 70;
                options.destinationType = Camera.DestinationType.FILE_URI;
                //options.correctOrientation = true;
                options.allowEdit = true,
                options.encodingType = Camera.EncodingType.JPEG;
                options.targetHeight = 600;
                options.targetWidth = 600;

                navigator.camera.getPicture(captureSuccess.bind(this),captureError,options);
            }
            else
            {
                PopupManager.getInstance().openAlertPopup(ResourceManager.getString("capture_image_not_available_title"), ResourceManager.getString("capture_image_not_available_desc"));
            }

            /*function captureSuccess(mediaFiles)
            {
                console.log("NoteManage > capture success");
                currentNoteVo.medias[0] = new MediaVo();
                currentNoteVo.medias[0].hash = mediaFiles[0].fullPath;
                currentNoteVo.medias[0].name = mediaFiles[0].name;
                currentNoteVo.medias[0].size = mediaFiles[0].size;
                currentNoteVo.medias[0].createdAt = mediaFiles[0].date;
                currentNoteVo.medias[0].noteId = currentNoteVo.id;
                this.addMediaToUpload(currentNoteVo.medias[0]);
                this.showPhotoPlaceHolder();
            }*/

            function captureSuccess(imageURI)
            {
                console.log("NoteManage > capture success");
                currentNoteVo.medias[0] = new MediaVo();
                currentNoteVo.medias[0].hash = imageURI;
                /*currentNoteVo.medias[0].name = mediaFiles[0].name;
                currentNoteVo.medias[0].size = mediaFiles[0].size;
                currentNoteVo.medias[0].createdAt = mediaFiles[0].date;*/
                currentNoteVo.medias[0].noteId = currentNoteVo.id;
                this.addMediaToUpload(currentNoteVo.medias[0]);
                this.showPhotoPlaceHolder();
            }

            function captureError()
            {
                console.log("NoteManage > Error");
                PopupManager.getInstance().openAlertPopup(ResourceManager.getString("capture_image_not_available_title"), ResourceManager.getString("capture_image_not_available_desc"));
            }
        }

        /**
         * Show Photo placeholder
         */
        this.showPhotoPlaceHolder = function()
        {
            var date = new Date(currentNoteVo.medias[0].createdAt);
            var dateStr = date.getDay()+"/"+date.getMonth()+"/"+date.getFullYear();
            var sizeKb = Math.round(currentNoteVo.medias[0].size * 0.000976562);
            var src = (currentNoteVo.dbid == null)?currentNoteVo.medias[0].hash:ServicesManager.getInstance().getServerUrl()+currentNoteVo.medias[0].hash;
            photoContainer.attr('src',src);
            photoFilename.text(currentNoteVo.medias[0].name);
            photoInfos.text( dateStr+ " - "+ sizeKb + " kb ");
            photoWrap.css('display', 'block');

            if(Debug.UPLOAD_PHOTO_WHEN_TAKEN)
            {

            }
        }

        /**
         * hide Photo placeholder
         */
        this.hidePhotoPlaceHolder = function()
        {
            photoContainer.attr('src',"");
            photoFilename.text("");
            photoInfos.text("");
            photoWrap.css('display', 'none');
        }



        /**
         * Close popup
         * clean content
         */
        this.close = function()
        {
            console.log("NoteManage > close");
            $('#body-note-wrapper').css('display','none');//@todo review added by Jeanch for quickfix
            $(wrapper).one('webkitTransitionEnd', function(e)
            {
                $(e.target).remove();
                bodyRef.css("display", "none");
                console.log('Note removed from DOM')
            });
            $(wrapper).attr("class", "note noteTransition noteClose");
            this.cleanContent();
            $('body').css('overflow', 'visible');
        }


        /**
         * Clean content
         */
        this.cleanContent = function()
        {
            textarea.val("");
            this.hidePhotoPlaceHolder();
        }



        /**
         * Create Btn
         */
        this.createBtn = function($type)
        {
            var btnContent;
            var btn = document.createElement('li');

            switch ($type)
            {
                case 'ok':
                    btnContent = document.createElement('i');
                    btnContent.className = "fa fa-check fa-2x";
                    btn.appendChild(btnContent);
                    break;

                case 'photo':
                    btnContent = document.createElement('i');
                    btnContent.className = "fa fa-camera fa-2x";
                    btn.appendChild(btnContent);
                    break;

                case 'cancel':

                    btn.innerHTML = ResourceManager.getString("common_cancel");
                    break;
            }

            buttonWrap[0].appendChild(btn);

            return btn;
        }

        /**
         * Get Notes object
         */
        this.getNotes = function()
        {
            return notes;
        }

        /**
         * Get Notes by id
         */
        this.getNoteById = function($id)
        {
            return notes[$id];
        }

        /**
         * Set Notes object
         */
        this.setNotes = function($notes)
        {
            notes = $notes;
        }


        /**
         * update Repository
         * Construct/update Vo's and Repositories
         */
        this.updateRepository = function($data)
        {
            notes = {}; // empty repository
            for(var i=0; i<$data.length ; i++)
            {
                var noteVo = new NoteVo();
                noteVo.fillData($data[i]);
                notes[$data[i].id] = noteVo;
            }
        }

        /**
         * Add media to upload
         */
        this.addMediaToUpload = function($mediaVo)
        {
            mediaToUpload.push($mediaVo);
        }

        /**
         * Get media to upload
         */
        this.getMediaToUpload = function()
        {
            return mediaToUpload;
        }

        /**
         * Reset media to upload
         */
        this.resetMediaToUpload = function()
        {
            mediaToUpload = [];
        }
    }
})();