/**
 * Created by jonathan on 18/02/14.
 */
var SpeciesManager = (function () {

    /* --------------------------------- Properties -------------------------------- */
    var instance;  // our instance holder
    var species = {}; //repository
    var speciesTags = {};

    /* --------------------------------- SINGLETON  -------------------------------- */
    var _static  = {

        // an emulation of static variables and methods

        /**
         * Method for getting an instance. It returns
         * a singleton instance of a singleton object
         */
        getInstance:  function()
        {
            if( instance  ===  undefined )  {
                instance = new Singleton();
            }

            return  instance;
        },

        THIS_IS_A_STATIC : 0
    };

    return  _static;

    function Singleton()
    {

        /* --------------------------------- functions -------------------------------- */

        /**
         * Init function
         */
        this.init = function()
        {
        }

        /**
         * update Repository
         * Construct/update Vo's and Repositories
         */
        this.updateRepository = function($data, $speciesTags)
        {
            if($data != undefined && $data.length > 0)
            {
                species = {};
                if($data)
                {
                    for(var i=0; i < $data.length; i++)
                    {
                        var vo = new SpecyVo();
                        vo.fillData($data[i]);
                        species[$data[i].id] = vo;
                    }
                }

                if($speciesTags != undefined)
                    speciesTags = $speciesTags;
            }
        }

        /**
         * Get price based on the specy and cicmference
         */
        this.getPrice = function($specyId, $circumference, $isWood)
        {
            var specyVo = species[$specyId];

            if($isWood == true)
            return specyVo.pricing["wood"];

            for (var key in specyVo.pricing)
            {
                var circumferenceStr = key;

                if(circumferenceStr != "wood")
                {
                    var min = circumferenceStr.split("-")[0];
                    var max = circumferenceStr.split("-")[1];
                    if($circumference > min && $circumference < max)
                    return specyVo.pricing[key];
                }

            }
        }

        /**
         * GET label
         * @param $specyId
         */
        this.getLabel = function($specyId)
        {
            var specyVo = species[$specyId];
            if(specyVo != undefined)
            return ResourceManager.getString(specyVo.translateKey);
            else
            {
                console.log('WARNING: SpecyVo  is undefined with id: '+$specyId);
                return "Species not found";
            }
        }

        /**
         * GET label
         * @param $specyId
         */
        this.getInitialsLabel = function($specyId)
        {
            var specyVo = species[$specyId];
            if(specyVo != undefined)
            {
                var speciesLabel = ResourceManager.getString(specyVo.translateKey);

                speciesLabel = speciesLabel.split(" ");
                var finalSpeciesLabel = "";
                $.each(speciesLabel, function(i,word){
                    if(word.length>2)
                    {
                        finalSpeciesLabel += word.substr(0,1).toUpperCase();
                    }
                });
                return finalSpeciesLabel;
            }
            else
            {
                console.log('WARNING: SpecyVo  is undefined with id: '+$specyId);
                return "Species not found";
            }
        }

        /**
         * Get Species object
         */
        this.getSpecies = function()
        {
            return species;
        }

        /**
         * Get SpeciesTags object
         */
        this.getSpeciesTags = function()
        {
            return speciesTags;
        }

        /**
         * Generate option tag for select tag
         * @param select
         */
        this.getSpeciesHTML = function(select)
        {
            console.log("SpeciesManager > getSpeciesHTML");
            if(select == undefined || select == null)
            {
                console.log("SpeciesManager > getSpeciesHTML: select element is null or undefined, return.");
                return;
            }
            //Empty the select just in case
            select.options.length = 0;

            var option;

            //create a disable option tag that will serve as first child in the selection
            var typeOption = document.createElement('option');
            typeOption.selected = "selected";
            typeOption.value = "";
            typeOption.text = ResourceManager.getString("marking_details_form_treeType_default");
            typeOption.disabled = 'disabled';
            select.appendChild(typeOption);


            for (var key in species)
            {
                var specyVo = species[key];

                option = document.createElement('option');
                option.value = specyVo.id;
                option.text = ResourceManager.getString(specyVo.translateKey);
                select.appendChild(option);
            }

        }
    }
})();