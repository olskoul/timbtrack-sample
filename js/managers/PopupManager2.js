/**
 * Created by jonathan on 18/02/14.
 */
var PopupManager2 = (function () {

    /* --------------------------------- Properties -------------------------------- */
    var instance;  // our instance holder
    var wrapper; // element that will wrap all popups
    var content = document.createElement('div');
    var currentContent ;
    var okBtn;
    var yesBtn;
    var noBtn;
    var cancelBtn;
    var seeBtn;
    var batchForSaleYesRadioBtn;
    var batchForSaleNoRadioBtn;
    var buttonWrap = document.createElement("ul");
    var callback;
    var callbackData;
    var batchInput;

    //type of content
    var alertContent ;
    var infosMarkingContent ;
    var newBatchContent ;

    /* --------------------------------- SINGLETON  -------------------------------- */
    var _static  = {

        // an emulation of static variables and methods

        /**
         * Method for getting an instance. It returns
         * a singleton instance of a singleton object
         */
        getInstance:  function()
        {
            if( instance  ===  undefined )  {
                instance = new Singleton();
            }

            return  instance;
        }



        // Static variable

        //TEMPLATE_MARKING : "marking/template_"
    };

    return  _static;

    function Singleton()
    {

        /* --------------------------------- functions -------------------------------- */

        /**
         * Init function
         */
        this.init = function($wrapper)
        {
            wrapper = $wrapper;

            content.className = "popupContent";
            wrapper[0].appendChild(content);
            content = wrapper.find('.popupContent');

            buttonWrap.className = "buttonWrap";
            content[0].appendChild(buttonWrap);
            buttonWrap = content.find('.buttonWrap');

            this.close();

        }

        /**
         * Alert popup
         */
        this.openAlertPopup = function($title, $desc)
        {
            if(alertContent == undefined)
                alertContent = this.createAlertContent();


            okBtn = this.createBtn('ok');
            okBtn.addEventListener('click', this.close.bind(this));


            this.addContent(alertContent);
            buttonWrap[0].appendChild(okBtn);

            $("h1",content).text($title);
            $("p",content).text($desc);

            this.open();
        }

        /**
         * Confirm popup
         */
        this.openConfirmPopup = function($title, $desc, $yesCallBack, $noCallBack)
        {
            if(alertContent == undefined)
                alertContent = this.createAlertContent();

            var closeFunction = this.close.bind(this);

            yesBtn = this.createBtn('yes');
            yesBtn.addEventListener('click', function()
            {
                closeFunction();
                $yesCallBack();

            });
            noBtn = this.createBtn('no');
            noBtn.addEventListener('click', function()
            {
                closeFunction();
                $noCallBack();

            });


            this.addContent(alertContent);
            buttonWrap[0].appendChild(yesBtn);
            buttonWrap[0].appendChild(noBtn);

            $("h1",content).text($title);
            $("p",content).text($desc);

            this.open();
        }

        /**
         * New batch popup
         */
        this.openNewBatchPopup = function($validationCallback)
        {
            this.setCallBack($validationCallback);

            if(newBatchContent == undefined)
            {
                newBatchContent = this.createNewBatchContent();
                batchInput = $(newBatchContent).find('#batchNameInput');
                batchInput[0].addEventListener('change', this.newBatchNameChanged.bind(this));


                batchForSaleNoRadioBtn = $(newBatchContent).find('#btn-no');
                batchForSaleYesRadioBtn = $(newBatchContent).find('#btn-yes');

                batchForSaleYesRadioBtn[0].addEventListener('click', this.batchForSaleYesNoClickHandler.bind(this));
                batchForSaleNoRadioBtn[0].addEventListener('click', this.batchForSaleYesNoClickHandler.bind(this));
            }

            okBtn = this.createBtn('ok');
            okBtn.addEventListener('click', this.newBatchValidate.bind(this));

            cancelBtn = this.createBtn('cancel');
            cancelBtn.addEventListener('click', this.close.bind(this));

            this.addContent(newBatchContent);
            buttonWrap[0].appendChild(cancelBtn);
            buttonWrap[0].appendChild(okBtn);

            this.selectBtn(batchForSaleNoRadioBtn,true);
            this.selectBtn(batchForSaleYesRadioBtn,false);
            this.activateBtn($(okBtn),false);
            batchInput.val("");

            this.open();
        }



        /**
         * Infos marking popup
         */
        this.openInfoMarkingPopup = function($markingVo)
        {
            if(infosMarkingContent == undefined)
                infosMarkingContent = this.createInfosMarkingContent();

            seeBtn = this.createBtn('see');
            seeBtn.addEventListener('click', this.seeMarkingOnMap.bind(this));

            okBtn = this.createBtn('ok');
            okBtn.addEventListener('click', this.close.bind(this));

            this.addContent(infosMarkingContent);
            buttonWrap[0].appendChild(seeBtn);
            buttonWrap[0].appendChild(okBtn);

            var desc = SpeciesManager.getInstance().getLabel($markingVo.species);
            desc += "<br />"+$markingVo.getTonnage() + " m3";


            $("h1",content).text(ResourceManager.getString("common_marking"));
            $("p",content).html(desc);

            this.open();
        }

        /**
         * Add content to popup
         */
        this.addContent = function($contentToAdd)
        {
            content[0].insertBefore($contentToAdd,content[0].firstChild);
            currentContent = $contentToAdd;
        }

        /**
         * Set callback
         */
        this.setCallBack = function($callback)
        {
            if($callback)
            callback = $callback;
            else
            callback = null;

            callbackData = {};
        }

        /**
         * New batch validated
         */
        this.newBatchNameChanged = function()
        {
            var batchName = batchInput.val();
            this.activateBtn($(okBtn),(batchName != ""));
        }

        /**
         * New batch validated
         */
        this.newBatchValidate = function()
        {
            var batchName = batchInput.val();
            if(batchName != "")
            {
                callbackData.name = batchName;
                this.callbackAndClose();
            }
            else
            {
                this.close();
                this.openAlertPopup("", ResourceManager.getString("popup_field_empty"));
            }

        }

        /**
         * callback and close
         */
        this.callbackAndClose = function()
        {
            if(callback != null)
            callback(callbackData);

            this.close();
        }

        /**
         * Open popup
         */
        this.open = function()
        {
           // this.cleanContent();
            wrapper.css("display", "block");
            wrapper.css("visibility", "visible");
            this.centerContent();
        }



        /**
         * See marking on map popup
         * clean content
         */
        this.seeMarkingOnMap = function()
        {
            this.close();
        }

        /**
         * Close popup
         * clean content
         */
        this.close = function()
        {
            wrapper.css("display", "none");
            wrapper.css("visibility", "hidden");
            this.cleanContent();
        }

        /**
         * Center content on screen
         */
        this.centerContent = function()
        {
            var left = Math.round($(window).width()/2 - content.outerWidth()/2);
            var top = Math.round($(window).height()/2 - content.outerHeight()/2);
            content.css("top",top);
            content.css("left",left);
        }

        /**
         * Clean content
         */
        this.cleanContent = function()
        {
            EventUtils.removeListener(okBtn,true);
            EventUtils.removeListener(cancelBtn,true);
            EventUtils.removeListener(seeBtn,true);
            EventUtils.removeListener(noBtn,true);
            EventUtils.removeListener(yesBtn,true);


            if(buttonWrap)
                buttonWrap.empty();


            if(currentContent != null)
             currentContent.remove();
             currentContent = null;
        }

        /**
         * Create alert content
         */
        this.createAlertContent = function()
        {
            var wrap = document.createElement('div');
            var title = document.createElement('h1');
            var desc = document.createElement('p');
            wrap.appendChild(title);
            wrap.appendChild(desc);
            return wrap;
        }

        /**
         * Create new batch content
         */
        this.createNewBatchContent = function()
        {
            var wrap = document.createElement('div');
            wrap.innerHTML = TemplateManager.getInstance().getTemplate(TemplateManager.TEMPLATE_POPUP_NEW_BATCH,null,false);
            return wrap;
        }

        /**
         * Create Infos marking content
         */
        this.createInfosMarkingContent = function()
        {
            var wrap = document.createElement('div');
            var title = document.createElement('h1');
            var desc = document.createElement('p');

            wrap.appendChild(title);
            wrap.appendChild(desc);
            return wrap;
        }

        /**
         * Create Btn
         */
        this.createBtn = function($type)
        {
            var btnContent;
            var btn = document.createElement('li');

            switch ($type)
            {
                case 'ok':
                    btnContent = document.createElement('i');
                    btnContent.className = "fa fa-check fa-2x";
                    btn.appendChild(btnContent);
                    break;

                case 'cancel':
                    btn.innerHTML = ResourceManager.getString("common_cancel");
                    break;

                case 'yes':
                    btn.innerHTML = ResourceManager.getString("common_yes");
                    break;

                case 'no':
                    btn.innerHTML = ResourceManager.getString("common_no");
                    break;

                case 'see':
                    btn.innerHTML = ResourceManager.getString("common_see");
                    break;
            }

            return btn;
        }

        /**
         *  New batch popup yes no click handler
         */
        this.batchForSaleYesNoClickHandler = function(e)
        {
            var target = $(e.currentTarget);


            this.selectBtn(batchForSaleNoRadioBtn,(target[0] == batchForSaleNoRadioBtn[0]));
            this.selectBtn(batchForSaleYesRadioBtn,(target[0] == batchForSaleYesRadioBtn[0]));



        }
        this.selectBtn = function(target, flag)
        {
            var bgColor = (flag)?"#95c758":"#ffffff";
            var textColor = (flag)?"#ffffff":"#2c2e33";

            target.css("background-color", bgColor);
            target.css("color", textColor);

            callbackData.isInventory = (target[0] == batchForSaleYesRadioBtn[0]);
            console.log('Is inventory: '+callbackData.isInventory);
        }
        /**
         * Hide or show validate btn
         * @flag true or false
         */
        this.activateBtn = function (target, flag)
        {
            var visibility = (flag)? 1:.2;
            var pointerEvent = (flag)? 'auto':'none';
            target.css('opacity',visibility);
            target.css('pointer-events',pointerEvent);
        }
    }
})();