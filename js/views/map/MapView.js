var MapView = function ($action) {

    this.ACTION_WOOD_LAND = 'woodland';
    this.ACTION_GO_TO = 'goto';

    var view;
    var map;
    /* ---------------------------------- Sort of constructor ---------------------------------- */
    this.initialize = function () {
        // Define a div wrapper for the view. The div wrapper is used to attach events.
        this.el = $('<div/>');
        this.content = $('<div/>');
        this.el.html(this.content);
        this.content.html(TemplateManager.getInstance().getTemplate(TemplateManager.TEMPLATE_EMPTY));
        this.showHeader();
    };

    /* ---------------------------------- Public methods ---------------------------------- */
    /*this.render = function() {

        //render wrapper template
        this.el.html(TemplateManager.getInstance().getTemplate(TemplateManager.TEMPLATE_EMPTY));

        //Show header
        this.showHeader();

        return this;

    };*/

    this.refresh = function()
    {
        if(this.lang != Infos.lang)
        {
            this.reset();
        }
        else
        {
            mapping.updateMarkings(MarkingManager.getInstance().getMarkings());
            mapping.updateNotes(NotesManager.getInstance().getNotes());

            var idTotrack = this.checkForTracking();
            if(idTotrack == false)
            {
                this.focusOnCoords();
            }
            this.header.showLeftBtn("btnMenu");
        }
    };

    this.reset = function()
    {
        //clear from memory
        this.el.empty();
        this.setup();
    };

    /**
     * Setup Map layer
     * switch action type (woodland or goto)
     */
    this.setup = function()
    {
        console.log("Mapview > setup");
        //rendered lang
        this.lang = Infos.lang;

        //render wrapper template
        this.content.html(TemplateManager.getInstance().getTemplate(TemplateManager.TEMPLATE_MAP));

        //Show header
        this.showHeader();

        if(this.initMap() == true)
        {
            this.initControls();
            //hash change for step change
            $(window).on('hashchange', this.hashChangedHanlder.bind(this));
        }
        else
        {
            console.log("Mapview > Init map failed");
            PopupManager.getInstance().openAlertPopup("Initiation of map", "Process failed to execute, this should never happen");
        }
    }


    /* ---------------------------------- Privates methods ---------------------------------- */

    /**
     * Hahs changed handler
     */
    this.hashChangedHanlder = function()
    {
        this.stopTracking();
        this.checkForTracking();
    },

    /**
     * Focus on coords
     */
    this.focusOnCoords = function()
    {
        var coordsString = UrlUtils.getInstance().getUrlParam("coordsToFit");
        if (coordsString != null && coordsString != "")
        {
            var coords = coordsString.split("___");
            mapping.fitCoords([[parseFloat(coords[0]),parseFloat(coords[1])]]);
            return true;
        }
        else return null;
    }
    /**
     * INIT MAP
     */
    this.initMap = function()
    {
        console.log("Mapview > initMap");
        if (map !== undefined) {
            return;
        }

        var woodlandGeoJSON = WoodlandManager.getInstance().getUserWoodland().geoJSON;
        var woodlandObj = JSON.parse(woodlandGeoJSON);
        var coordsToFitInto = [];

        //Add woodland to fit into
        if(woodlandObj != null && woodlandObj != undefined)
        {
            for(var i=0 ; i<woodlandObj.geometry.coordinates[0].length ; i++)
            {
                coordsToFitInto.push(woodlandObj.geometry.coordinates[0][i]);
            }

            mapping.init();
            mapping.drawVectorFromGeoJSON('olSourceWoodland',woodlandGeoJSON,false);
            mapping.updateMarkings(MarkingManager.getInstance().getMarkings());
            mapping.updateNotes(NotesManager.getInstance().getNotes());

            //tracking
            var idTotrack = this.checkForTracking();
            if(idTotrack == false)
            {
                var coordsToFit = this.focusOnCoords();
                if(coordsToFit == null)
                    mapping.fitCoords(coordsToFitInto);
            }

            return true;

        }
        else
        {
            alert("Woodland is not defined");
            return false;
        }

    }

    /**
     * CHECK FOR TRACKING
     */
    this.checkForTracking = function()
    {
        var idTotrack = UrlUtils.getInstance().getUrlParam("track");
        if(idTotrack != null && idTotrack!= null && idTotrack!= "")
        {
            this.startTracking(idTotrack);
            return true;
        }
        else
        {
            this.stopTracking();
            return false
        }

    }

    /**
     * start TRACKING
     */
    this.startTracking = function(idTotrack)
    {
        mapping.startTracking(mapping.getMarkingFeatureById(idTotrack));

        //Hide all btn except tracking btn
        var btnsOnMap = $(".customControl");
        for(var i=0; i<btnsOnMap.length; i++)
        {
            var btn = $(btnsOnMap[i]);
            if(btn.attr('id') != "mapIconTrack" && btn.attr('id') != "mapIconZoomMinus" && btn.attr('id') != "mapIconZoomPlus")
            {
                btn.css('display','none');
            }
            else
                btn.css('display','block');

        }
    },

    /**
     * stop TRACKING
     */
    this.stopTracking = function()
    {
        mapping.stopTracking();
        //Hide all btn except tracking btn
        var btnsOnMap = $(".customControl");
        for(var i=0; i<btnsOnMap.length; i++)
        {
            var btn = $(btnsOnMap[i]);
            if(btn.attr('id') == "mapIconTrack")
            {
                btn.css('display','none');
            }
            else
                btn.css('display','block');

        }
    },

    /**
     * INIT CONTROLS
     */
    this.initControls = function()
    {
        console.log("Mapview > initControls");
        /**
         *
         * INTERACTION ON MAP
         *
         */




        // NOTES and Markings
       var selectGlobal = new ol.interaction.Select({
            condition: ol.events.condition.click,
            layers: [mapping.olLayerNotes,mapping.olLayerMarkings],
            style :  mapping.styles['default']['Note_SELECTED']
        });

        var collectionGlobals = selectGlobal.getFeatures();
        collectionGlobals.on('add', function(e,i,g,h){
            var id = e.element.getId();
            var type= e.element.getProperties().type;

            if (type=='marking'){
                var markingVo = MarkingManager.getInstance().getMarkingById(id);
                if(markingVo != null && markingVo != undefined)
                    PopupManager.getInstance().openInfoMarkingPopup(markingVo);
            }else if (type=='note'){
                NotesManager.getInstance().openNote(id);
            }

        });
        collectionGlobals.on('remove', function(e){
            console.log('Unselected '+e.element.getProperties().type+' : '+e.element.getId());
        });

        mapping.addInteraction(selectGlobal);

        /**
         *
         * ON TOP OF MAP BUTTONS
         *
         */

        //Icon: mapIconCenter
        $('#mapIconCenter')[0].addEventListener('click',function()
        {
            console.log('Map View -> Center on Woodland');
            var rawGeoJSON = WoodlandManager.getInstance().getUserWoodland().geoJSON;
            var geoJSONObj = JSON.parse(rawGeoJSON);
            mapping.fitCoords(geoJSONObj.geometry.coordinates[0]);
        });

        //Icon: mapIconGeolocMe
        $('#mapIconGeolocMe')[0].addEventListener('click',this.geolocMe.bind(this));

        //Icon: create note
        $('#mapAddNote')[0].addEventListener('click', this.openNote.bind(this));

        //Icon: Zoom +
        $('#mapIconZoomPlus')[0].addEventListener('click',function()
        {
            mapping.zoomIn();
        });

        //Icon: Zoom -
        $('#mapIconZoomMinus')[0].addEventListener('click',function()
        {
            mapping.zoomOut();
        });

        //stop tracking
        var stopTrackingFunction = this.stopTracking.bind(this);
        $('#mapIconTrack')[0].addEventListener('click',function()
        {
            stopTrackingFunction();
        });

        //Goole links hack
        $('#blockGoogleLink')[0].addEventListener('click',function()
        {
            //
        });
    }

    /**
     * Add note callback
     */
    this.openNote = function()
    {
        console.log("Mapview > openNote");
        NotesManager.getInstance().openNote(-1,this.addNoteCallBack.bind(this));
    }

    /**
     * Add note callback
     */
    this.addNoteCallBack = function(noteVo)
    {
        console.log("Mapview > addnoteCallback");
        GeolocationManager.getInstance().getCurrentPosition(noteVo, noteVo.point, this.noteGeolocationSuccess.bind(this), this.noteGeolocationSuccess.bind(this));
    }

    this.noteGeolocationSuccess = function(noteVo, pointGPS)
    {
        noteVo.point = pointGPS;
        console.log("Mapview > noteGeolocationSuccess");
        Infos.saveUserData();

        if(pointGPS.longitude != false)
        {
            mapping.fitCoords([[pointGPS.longitude,pointGPS.latitude]]);
            //REFRESH MAP
            this.refresh();
        }
        else
            PopupManager.getInstance().openAlertPopup("", ResourceManager.getString("popup_no_gps_coords"));
    }

    /**
     * Show header
     */
    this.showHeader = function()
    {
        console.log("Mapview > showHeader");
        var headerTitle;
        switch($action)
        {
            case this.ACTION_WOOD_LAND:
                headerTitle = ResourceManager.getString("header_title_map");
                break;

            case this.ACTION_GO_TO:
                headerTitle = ResourceManager.getString("header_title_goto");
                break;
        }

        this.headerContainer = this.el.find('#view-header');
        this.header = new HeaderView("default",headerTitle);
        this.headerContainer.html(this.header.render().el);
        this.header.setup();
        this.header.setBtnBack('#map');
    }

    /**
     * Geoloc me
     */
    this.geolocMe = function()
    {

        console.log('Map View -> Center on me');
        GeolocationManager.getInstance().getCurrentPosition(null, new PointGPS(), this.geolocMeSuccess.bind(this), this.getPositionError.bind(this));
    }

    this.geolocMeSuccess = function(vo, pointGPS)
    {
        console.log("Mapview > geolocMe Success");
        if(pointGPS.longitude != false)
        {
            mapping.fitCoords([[pointGPS.longitude,pointGPS.latitude]]);
        }
        else
            this.getPositionError();
    }

    /**
     * Get position Error
     */
    this.getPositionError = function()
    {
        console.log("Mapview > geolocMe error");
        PopupManager.getInstance().openAlertPopup(ResourceManager.getString("popup_no_gps_title"), ResourceManager.getString("popup_no_gps_desc"));
    }


    this.initialize();

}