/**
 * Created by jonathan on 13/08/14.
 */
/**
 * OpenLayer 3 utils
 *
 */
var mapping =
{
    view: undefined,//ol.view
    map: undefined,//ol.map
    gmap: undefined,

    olSourceAreas: undefined,
    olLayerAreas: undefined,

    olSourceWoodland: undefined,
    olLayerWoodland: undefined,

    olSourceMarkings: undefined,
    olLayerMarkings: undefined,

    olSourceNotes: undefined,
    olLayerNotes: undefined,

    olLayerTextures: undefined,
    
    addingInteractionsAllowed: true,
    overlayMarkingPopin:undefined,
    selectedMarkings:[],
    selectClick : undefined,
    featuresOverlay : undefined,
    geolocation : undefined,
    trackedFeature: undefined,
    /**
     * STYLES
     */
    'styles': {
        "default": {
            'Point': [new ol.style.Style({
                image: new ol.style.Icon(/** @type {olx.style.IconOptions} */ ({
                    anchor: [0.5, 0.5],
                    anchorXUnits: 'fraction',
                    anchorYUnits: 'fraction',
                    opacity: 0.75,
                    src: 'static/img/arbre.svg'
                }))
            })],
            'Marking': [new ol.style.Style({
                image: new ol.style.Circle({
                    radius:5,
                    fill: new ol.style.Fill({
                        color: '#84ab54'
                    }),
                    stroke: new ol.style.Stroke({color: 'black', width: 1})
                })
            })],
            'Marking_SELECTED': [new ol.style.Style({
                image: new ol.style.Circle({
                    radius:5,
                    fill: new ol.style.Fill({
                        color: '#ffffff'
                    }),
                    stroke: new ol.style.Stroke({color: 'black', width: 1})
                })
            })],
            'Note': [new ol.style.Style({
                image: new ol.style.Circle({
                    radius:5,
                    fill: new ol.style.Fill({
                        color: '#50535d'
                    }),
                    stroke: new ol.style.Stroke({color: 'black', width: 1})
                })
            })],
            'Note_SELECTED': [new ol.style.Style({
                image: new ol.style.Circle({
                    radius:5,
                    fill: new ol.style.Fill({
                        color: '#ffffff'
                    }),
                    stroke: new ol.style.Stroke({color: 'black', width: 1})
                })
            })],
            'LineString': [new ol.style.Style({
                stroke: new ol.style.Stroke({
                    color: 'green',
                    width: 1
                })
            })],
            'MultiLineString': [new ol.style.Style({
                stroke: new ol.style.Stroke({
                    color: 'green',
                    width: 1
                })
            })],

            'MultiPolygon': [new ol.style.Style({
                stroke: new ol.style.Stroke({
                    color: 'yellow',
                    width: 1
                }),
                fill: new ol.style.Fill({
                    color: 'rgba(255, 255, 0, 0.1)'
                })
            })],
            'woodland': [new ol.style.Style({
                stroke: new ol.style.Stroke({
                    color: 'white',
                    width: 1,
                    lineDash: [4]
                })
            })],
            'area': [new ol.style.Style({
                stroke: new ol.style.Stroke({
                    color: '#003b0d',
                    width: 3
                }),
                fill: new ol.style.Fill({
                    color: '#017a1b'
                })
            })],
            'Water': [new ol.style.Style({
                stroke: new ol.style.Stroke({
                    color: '#003b0d',
                    width: 3
                }),
                fill: new ol.style.Fill({
                    color: '#016c7c'
                })
            })],
            'Polygon': [new ol.style.Style({
                stroke: new ol.style.Stroke({
                    color: 'blue',
                    lineDash: [4],
                    width: 3
                }),
                fill: new ol.style.Fill({
                    color: 'rgba(0, 0, 255, 0.1)'
                })
            })],
            'GeometryCollection': [new ol.style.Style({
                stroke: new ol.style.Stroke({
                    color: 'magenta',
                    width: 2
                }),
                fill: new ol.style.Fill({
                    color: 'magenta'
                }),
                image: new ol.style.Circle({
                    radius: 10,
                    fill: null,
                    stroke: new ol.style.Stroke({
                        color: 'magenta'
                    })
                })
            })],
            'Circle': [new ol.style.Style({
                stroke: new ol.style.Stroke({
                    color: 'red',
                    width: 2
                }),
                fill: new ol.style.Fill({
                    color: 'rgba(255,0,0,0.2)'
                })
            })]
        }
    },
    /**
     * Init openlayer map with gmap if online
     * @param idGmap
     * @param idOlMap
     */
    'init': function (idGmap, idOlMap) {
        var self = this;
        if (idGmap == undefined) {
            idGmap = 'gmap';
        }
        if (idOlMap == undefined) {
            idOlMap = 'olmap';
        }
        /**
         * @todo replace online var by online mobile detection?
         */
        var online = true;

        //Check if map has already been initialized
        if (this.map !== undefined) {
            return;
        }


        var interactions = [];

        var dragPan = true;

        this.view = new ol.View({
            // make sure the view doesn't go beyond the 22 zoom levels of Google Maps
            maxZoom: 18
        });

        if (online) {
            console.log(idGmap);
            this.gmap = new google.maps.Map(document.getElementById(idGmap), {
                disableDefaultUI: true,
                keyboardShortcuts: false,
                draggable: false,
                disableDoubleClickZoom: true,
                scrollwheel: false,
                streetViewControl: false,
                mapTypeId: google.maps.MapTypeId.SATELLITE,
                zoom: 8,
                center: new google.maps.LatLng(-34.397, 150.644)
            });


            this.view.on('change:center', function () {
                var center = ol.proj.transform(self.view.getCenter(), 'EPSG:3857', 'EPSG:4326');
                self.gmap.setCenter(new google.maps.LatLng(center[1], center[0]));
            });
            this.view.on('change:resolution', function () {
                self.gmap.setZoom(self.view.getZoom());
            });
        } else {
            $('#' + idGmap).css('display', 'none');
        }

        var olMapDiv = document.getElementById(idOlMap);

        this.overlayMarkingPopin = new ol.Overlay({
            element: document.getElementById('tooltip-marking'),
            positioning: 'bottom-left'
        });

        this.map = new ol.Map({
            layers: [ ],
            controls: [],
            overlays: [ this.overlayMarkingPopin ],
            interactions: ol.interaction.defaults({
                altShiftDragRotate: false,
                pinchRotate:false,
                dragPan: dragPan,
                rotate: false
            }).extend(interactions),
            target: olMapDiv,
            view: this.view
        });

        if (this.gmap !== undefined) {
            olMapDiv.parentNode.removeChild(olMapDiv);
            this.gmap.controls[google.maps.ControlPosition.TOP_LEFT].push(olMapDiv);
        }
        this._createLayers();
        this.view.setZoom(16);
    },

    'removeMarkingInteraction' : function(){
        console.log('removeMarkingInteraction');
        this.map.removeInteraction(this.selectClick);
    },
    'addMarkingInteraction': function(){
        console.log('addMarkingInteraction');
        this.addInteraction(this.selectClick)
    },

    'addInteraction': function (interaction) {
        if (this.addingInteractionsAllowed) {
            this.map.addInteraction(interaction);
        }
    },
    'removeInteraction': function (interaction) {
        this.map.removeInteraction(interaction);
    },
    'disallowAddingInteractions': function () {
        this.addingInteractionsAllowed = false;
    },
    'allowAddingInteractions': function () {
        this.addingInteractionsAllowed = true;
    },
    'createMarkingInteraction': function(){
        this.selectClick = new ol.interaction.Select({
            condition: ol.events.condition.click,
            layers: [mapping.olLayerMarkings]
        });
        var self=this;
        MarkingPoolManager.getInstance().init(this.selectClick.getFeatures());
    },
    'createTracking': function(){
        console.log('mapping > createTracking');
        this.geolocation = new ol.Geolocation({
            projection: this.view.getProjection()
        });
// update the HTML page when the position changes.
        var self=this;
        this.geolocation.on('change', function() {

            //console.log(self.geolocation.getCoordinates());
           /* console.log(geolocation.getAccuracy() + ' [m]');
            console.log(geolocation.getAltitude() + ' [m]');
            console.log(geolocation.getAltitudeAccuracy() + ' [m]');
            console.log(geolocation.getHeading() + ' [rad]');
            console.log(geolocation.getSpeed() + ' [m/s]');*/
        });

// handle geolocation error.
        this.geolocation.on('error', function(error) {

            console.log('geolocation ERROR : '+error.message);
        });

        var accuracyFeature = new ol.Feature();
        accuracyFeature.bindTo('geometry', this.geolocation, 'accuracyGeometry');

        var positionFeature = new ol.Feature();

        positionFeature.bindTo('geometry', this.geolocation, 'position')
            .transform(function() {}, function(coordinates) {
                if(coordinates != undefined)
                {
                    //console.log(self.trackedFeature);
                    //console.log(coordinates);

                        self.centerOn(coordinates[0],coordinates[1], "EPSG:3857");


                    return new ol.geom.LineString([coordinates,self.trackedFeature.getGeometry().getCoordinates()]);
                }
                else
                {
                    return null;
                }

            });

        this.featuresOverlay.addFeature(accuracyFeature);
        this.featuresOverlay.addFeature(positionFeature);
    },
    'startTracking': function(feature){

        if(feature != undefined && feature != null)
        {
            this.trackedFeature=feature;
            if (this.geolocation == undefined) this.createTracking();
            this.featuresOverlay.setMap(this.map);
            this.geolocation.setTracking(true);
        }

    },
    'stopTracking' : function(){
        if(this.geolocation != undefined && this.geolocation != null)
        {
            this.geolocation.setTracking(false);
            this.featuresOverlay.setMap(null);
        }

    },
    /**
     * Center the map so that we can see every Marking features
     * @todo should be removed and use fitCollection instead
     */
    'fitEverything': function () {
        var coordsToFitInto = [];

        //Markings
        var features=this.olSourceMarkings.getFeatures();
        $.each(features,function (i,feature){
            var geometry = feature.getGeometry();
            var extent = geometry.getExtent();
            if(extent && extent.length>0)
            {
                coordsToFitInto.push([extent[0],extent[1]]);
                coordsToFitInto.push([extent[2],extent[3]]);
            }

        });


        var extent = new ol.extent.boundingExtent(coords);//geoJSONObj.geometry.coordinates[0]
        this.view.fitExtent(extent, this.map.getSize());
    },
    /**
     * Center the map to an extent that includes all the given collection coordinates
     * @param collection
     */
    'fitCollection': function (collection) {
        var coordsToFitInto = [];
        collection.forEach(function (feature,e){
            var geometry = feature.getGeometry();
            var extent = geometry.getExtent();
            if(extent && extent.length>0)
            {
                coordsToFitInto.push([extent[0],extent[1]]);
                coordsToFitInto.push([extent[2],extent[3]]);
            }
            return true;
        });

        var extent = new ol.extent.boundingExtent(coordsToFitInto);//geoJSONObj.geometry.coordinates[0]
        this.view.fitExtent(extent, this.map.getSize());
    },
    /**
     * Center the map to an extent that includes all the given coordinates
     * @param coords
     * @param view
     * @param map
     */
    'fitCoords': function (coords) {
        if(coords.length>0)
        {
            console.log("Mapping.fitCoords: "+coords)
            var extent = new ol.extent.boundingExtent(coords);//geoJSONObj.geometry.coordinates[0]
            this.view.fitExtent(extent, this.map.getSize());
        }
        else
        {
            console.log("Mapping.fitCoords -> Failed "+coords)
        }

    },
    /**
     *
     * @param lng
     * @param lat
     * @param projection
     */
    'centerOn': function (lng, lat, projection) {
        if (projection == undefined) {
            projection = 'EPSG:4326';
        }
        this.view.setCenter(ol.proj.transform([lng, lat], projection, 'EPSG:3857'));
    },
    /**
     *
     * @param zoomValue
     */
    'zoom': function (zoomValue) {
        this.view.setZoom(zoomValue);
    },
    /**
     *
     */
    'zoomIn': function () {
        this.zoom(this.view.getZoom() + 1);
    },
    /**
     *
     */
    'zoomOut': function () {
        this.zoom(this.view.getZoom() - 1);
    },
    'drawMarking': function (longitude, latitude, markingId) {
        var markingFeature = new ol.Feature();
        markingFeature.setId(markingId);
        markingFeature.setProperties({'type':'marking'});
        markingFeature.setGeometry(new ol.geom.Point([ longitude,latitude]));

        this.olSourceMarkings.addFeature(markingFeature);
    },
    'drawNote': function (longitude, latitude, noteId) {
        var noteFeature = new ol.Feature();
        noteFeature.setId(noteId);
        noteFeature.setProperties({'type':'note'});
        noteFeature.setGeometry(new ol.geom.Point([ longitude,latitude]));

        this.olSourceNotes.addFeature(noteFeature);
    },


    /**
     *@markingCollection Markings object
     */
    'updateMarkings': function (markingCollection) {

        console.log("Mapping.updateMarkings");

        var features=this.olSourceMarkings.getFeatures();
        $.each(markingCollection,function (i,marking)
        {
            var id = marking.id;

            var isOnMap = mapping.getMarkingById(id);

            if(!isOnMap)
            {
                console.log("Feature of marking "+id+" is missing");
                mapping.drawMarking(marking.point.longitude,marking.point.latitude,id);
            }

        });


    },

    /**
     *@NotesCollection Notes object
     */
    'updateNotes': function (notesCollection) {

        console.log("Mapping.updateNotes");

        var features=this.olSourceNotes.getFeatures();
        $.each(notesCollection,function (i,note)
        {
            var id = note.id;
            console.log("Checking note: "+id);
            var isOnMap = mapping.getNoteById(id);

            if(!isOnMap)
            {
                console.log("Feature of note "+id+" is missing ,adding "+note.point.longitude+':'+note.point.latitude);
                mapping.drawNote(note.point.longitude,note.point.latitude,id);
            }

        });


    },
    'getMarkingById': function (markingId) {

        var features=this.olSourceMarkings.getFeatures();
        var isOnMap = false;
        $.each(features,function (i,feature){
            if(feature.getId() == markingId)
            {
                isOnMap = true;
            }
        });
        return isOnMap;
    },
    'getMarkingFeatureById': function (markingId) {
        var toReturn =undefined;
        var features=this.olSourceMarkings.getFeatures();

        $.each(features,function (i,feature){

            if(feature.getId() == markingId)
            {
                toReturn = feature;
            }
        });
        return toReturn;
    },
    'getNoteById': function (noteId) {

        var features=this.olSourceNotes.getFeatures();
        var isOnMap = false;
        $.each(features,function (i,feature){
            if(feature.getId() == noteId)
            {
                isOnMap = true;
            }
        });
        return isOnMap;
    },
    'transformMobileCoords': function (long, lat) {
        return ol.proj.transform([long, lat], 'EPSG:4326', 'EPSG:3857');
    },
    /**
     * Add a layer
     * @param $source (GEOJSON)
     * @param $view
     * @param $map
     */
    'drawVectorFromGeoJSON': function (layerSource,geoJson, fitAfter) {
        console.log('§§§§§§§§§§§§§§§§§§§drawVectorFromGeoJSON§§§§§§§§§§§§§§§§');
        if (geoJson == undefined){
            console.warning('Missing geoJSON : not drawing layer');
            return;
        }
        if (this[layerSource] == undefined){
            console.warning('Layer source not found : '+layerSource);
            return;
        }


        if (fitAfter == undefined) {
            fitAfter = false;
        }

        var geoJsonObj = JSON.parse(geoJson);
        var f = new ol.format.GeoJSON();
        var feature = f.readFeature(geoJson);

        if (geoJsonObj.properties){
            feature.set('properties', geoJsonObj.properties);
        }

        this[layerSource].addFeature(feature);

        var self = this;


        function showImage(imgPath, feature) {
            var myImage = new Image();
            myImage.name = imgPath;
            myImage.onload = function () {
                feature.texture = self.createTextureLayer(geoJsonObj, imgPath, myImage.width, myImage.height);
            };
            myImage.src = imgPath;
        }
        if (layerSource!=='olSourceWoodland'){
            showImage('/image.php?json=' + geoJson, feature);
        }
        if (fitAfter ) this.fitCoords(geoJsonObj.geometry.coordinates[0]);
    },
    'createTextureLayer': function (geoJsonObj, imgPath, width, height) {
        var extent = new ol.extent.boundingExtent(geoJsonObj.geometry.coordinates[0]);
        var pixelProjection = new ol.proj.Projection({
            code: 'pixel',
            units: 'pixels',
            extent: [
                extent[0],
                extent[1],
                extent[0] + width,
                extent[1] + height
            ]
        });
        var newExt = ol.proj.transformExtent(pixelProjection.getExtent(), pixelProjection, 'EPSG:3857');
        var textureLayer = new ol.layer.Image({
            opacity: 0.8,
            source: new ol.source.ImageStatic({
                attributions: [
                    new ol.Attribution({
                        html: '&copy; <a href="http://xkcd.com/license.html">xkcd</a>'
                    })
                ],
                url: imgPath,
                imageSize: [width, height],
                projection: pixelProjection,
                imageExtent: newExt
            })
        });
        this.olLayerTextures.getLayers().insertAt(0, textureLayer);
        
        return textureLayer;
    },

    ////////////////************************************PRIVATE or utils functions***********************///////////////
    '_createLayers': function () {
        this._createFeaturesOverlay()
       this._createLayerAreas();
        this._createLayerTextures();
        this._createLayerNotes();
       this._createLayerMarkings();
        this._createLayerWoodland();
    },
    '_createFeaturesOverlay': function(){
        var self=this;
        this.featuresOverlay = new ol.FeatureOverlay({
            map: self.map
        });
    },
    '_createLayerMarkings': function(){
        this.olSourceMarkings = new ol.source.GeoJSON(
            /** @type {olx.source.GeoJSONOptions} */ ({
                object: {
                    'type': 'FeatureCollection',
                    'crs': {
                        'type': 'name',
                        'properties': {
                            'name': 'EPSG:3857'
                        }
                    },
                    'features': [

                    ]
                }
            }));

        this.olSourceMarkings.on('addfeature', function(event) {
            var feature = event.feature;
            window.console.log('added feature '+feature.getId());
        });
        this.olSourceMarkings.once('change', function() {

            window.console.log('source loaded '+this.getState());
        });
        var self = this;
        this.olLayerMarkings = new ol.layer.Vector({
            source: this.olSourceMarkings,
            style: (function () {
                return function (feature, resolution) {
                    console.log('styling marking');
                    var prop = feature.get('propreties');

                    if (prop !== undefined && prop.style) {
                        return self.styles['default'][prop.style];
                    } else {
                        var geom = feature.get('geometry');

                        return self.styles['default']['Marking'];
                    }

                };
            })()
        });

        this.map.addLayer(this.olLayerMarkings);

    },
    '_createLayerNotes': function(){
        this.olSourceNotes = new ol.source.GeoJSON(
            /** @type {olx.source.GeoJSONOptions} */ ({
                object: {
                    'type': 'FeatureCollection',
                    'crs': {
                        'type': 'name',
                        'properties': {
                            'name': 'EPSG:3857'
                        }
                    },
                    'features': [

                    ]
                }
            }));
        var self = this;
        this.olLayerNotes = new ol.layer.Vector({
            source: this.olSourceNotes,
            style: (function () {
                return function (feature, resolution) {
                    console.log('styling note');
                    var prop = feature.get('propreties');

                    if (prop !== undefined && prop.style) {
                        return self.styles['default'][prop.style];
                    } else {
                        var geom = feature.get('geometry');

                        return self.styles['default']['Note'];
                    }

                };
            })()
        });
        this.map.addLayer(this.olLayerNotes);
    },
    '_createLayerWoodland': function(){
        this.olSourceWoodland = new ol.source.GeoJSON(
            /** @type {olx.source.GeoJSONOptions} */ ({
                object: {
                    'type': 'FeatureCollection',
                    'crs': {
                        'type': 'name',
                        'properties': {
                            'name': 'EPSG:3857'
                        }
                    },
                    'features': [

                    ]
                }
            }));


        var self = this;
        this.olLayerWoodland = new ol.layer.Vector({
            source: this.olSourceWoodland,
            style: (function () {
                return function (feature, resolution) {

                    var prop = feature.get('properties');
                    /*console.log(prop);
                    console.log(feature);*/
                    if (prop !== undefined && prop.style) {
                        return self.styles['default'][prop.style];
                    } else {
                        var geom = feature.get('geometry');
                        return self.styles['default'][geom.type];
                    }

                };
            })()
        });
        this.map.addLayer(this.olLayerWoodland);
    },
    '_createLayerAreas': function(){
        this.olSourceAreas = new ol.source.GeoJSON(
            /** @type {olx.source.GeoJSONOptions} */ ({
                object: {
                    'type': 'FeatureCollection',
                    'crs': {
                        'type': 'name',
                        'properties': {
                            'name': 'EPSG:3857'
                        }
                    },
                    'features': [

                    ]
                }
            }));


        var self = this;
        this.olLayerAreas = new ol.layer.Vector({
            source: this.olSourceAreas,
            style: (function () {
                return function (feature, resolution) {
                    console.log('styling area');
                    var prop = feature.get('properties');
                    /*console.log(prop);
                    console.log(feature);*/
                    if (prop !== undefined && prop.style) {
                        return self.styles['default'][prop.style];
                    } else {
                        var geom = feature.get('geometry');
                        return self.styles['default'][geom.type];
                    }

                };
            })()
        });
        this.map.addLayer(this.olLayerAreas);
    },
    '_createLayerTextures': function(){
        this.olLayerTextures = new ol.layer.Group([]);
        this.map.addLayer(this.olLayerTextures);
    },
    '_listLayers': function () {

        this.map.getLayers().forEach(function (layer, i) {
            console.log(i);
            if (layer instanceof ol.layer.Group) {
                layer.getLayers().forEach(function (sublayer, j) {
                    console.log(i);
                });
            }
        });
    },
    '_listMarkings': function () {
        var features=this.olSourceMarkings.getFeatures();
        $.each(features,function (i,feature){
            console.log(feature.getId());
            console.log(feature.getGeometry().getType());
            console.log(feature.getGeometry().getExtent());
        });
    },
    '_listNotes': function () {
        var features=this.olSourceNotes.getFeatures();
        $.each(features,function (i,feature){
            console.log(feature.getId());
            console.log(feature.getGeometry().getType());
            console.log(feature.getGeometry().getExtent());
        });
    }

}