var SettingsLanguageView = SettingsStepAbstract.extend({

    langBtns:null,

    init: function($parent)
    {
        this._super(TemplateManager.TEMPLATE_SETTINGS_LANGUAGE, $parent);
    },

    setup: function()
    {
        this.lang = Infos.lang;
        this.content.html(TemplateManager.getInstance().getTemplate(this.templateName, this.templateData));


        this.header.setBtnBack('#settings');
        this.langBtns = $('.lang-choice');
        this.setLangUi();

        this.addListeners();
    },

    reset: function()
    {
       //override this to do nothing...
    },


    addListeners: function()
    {
        for(var i=0; i<this.langBtns.length; i++)
        {
            var btn = this.langBtns[i];
            btn.addEventListener('click', this.langClickHandler.bind(this));
        }
    },

    setLangUi: function()
    {
        for(var i=0 ; i < this.langBtns.length ; i++)
        {
            var btn = this.langBtns[i];
            var id = btn.id.split('settings-language-')[1];
            var display = ( id == Infos.lang)?'inline':'none';

            $(btn).children('i').css('display',display);
        }
    },

    langClickHandler: function(e)
    {
        console.log('SETTINGS: lang click handler');
        var btn = $(e.currentTarget)[0];
        var id = btn.id.split('settings-language-')[1];
        console.log('SETTINGS: change language :'+id);
        Infos.lang = id;
        //Infos.langHasChanged = true;
        this.setLangUi();
    }

});