var SettingsView = function (adapter, step) {

    var SETTINGS_HOME = "settings-home";
    var SETTINGS_LANGUAGE = "settings-language";
    var SETTINGS_PRIVACY = "settings-privacy";
    var dataTemplatePrivacy = {};
    var currentStepName;
    var cache = {};

    /* ---------------------------------- Sort of constructor ---------------------------------- */
    this.initialize = function () {
        // Define a div wrapper for the view. The div wrapper is used to attach events.
        this.el = $('<div/>');
    };

    this.initialize();

    /* ---------------------------------- Public methods ---------------------------------- */
    this.render = function()
    {
        //render wrapper template
        this.el.html(TemplateManager.getInstance().getTemplate(TemplateManager.TEMPLATE_EMPTY));

        //Show header
        this.showHeader();

        return this;

    };

    this.refresh = function()
    {
        if(this.lang != Infos.lang)
        {
            this.reset();
        }
        else
        {
            this.updateHeader(currentStepName);
        }
    };

    this.reset = function()
    {
        //clear from memory
        this.el.empty();
        currentStepName = null;
        this.setup();
    };


    this.setup = function()
    {
        //rendered lang
        this.lang = Infos.lang;

        //generate template data privacy:
        //dataTemplatePrivacy = {data:Infos.generatePrivacyTemplateData()};

        //render wrapper template
        var template = TemplateManager.getInstance().getTemplate(TemplateManager.TEMPLATE_SETTINGS);
        this.el.html(template);

        //ref
        this.wrapper = this.el.find('#settings-wrapper');

        //Show header
        this.showHeader();

        //init pageSlider
        this.pageSlider = new PageSlider(this.wrapper);

        //show default step (home)
        this.showStep(SETTINGS_HOME);

        //hash change for step change
        $(window).on('hashchange', this.hashChangedhandler.bind(this));
    }

    /* ---------------------------------- Privates methods ---------------------------------- */


    /**
     * hash changed handler
     */
    this.hashChangedhandler = function(e)
    {
        var viewName = UrlUtils.getInstance().getHashValue(0);
        if(viewName != "settings")
        {
            console.log("Settings View > We need to kill views and listeners!!")
            return;
        }
        var subStepName = UrlUtils.getInstance().getHashValue(1);
        if(subStepName == undefined)
            subStepName = SETTINGS_HOME;

        this.showStep(subStepName);
    }



    /**
     * show step
     */
    this.showStep = function($id)
    {
        if(currentStepName == $id)
        return;

        var subStep;
        var pageEl;

        //first check the cache
        if(cache[$id] != undefined)
        {
            subStep = cache[$id].page;
            pageEl = cache[$id].pageEl;
        }
        else
        {
            switch($id)
            {
                case SETTINGS_HOME:
                    subStep = new SettingsHomeView(this);
                    break;

                case SETTINGS_LANGUAGE:
                    subStep = new SettingsLanguageView(this);
                    break;

                case SETTINGS_PRIVACY:
                    subStep = new SettingsPrivacyView(this);
                    break;
            }

            if(subStep == undefined)
            return;

            //element
            pageEl = subStep.el;
        }

        //ref
        currentStepName = $id;

        //Show step
        this.pageSlider.slidePage($(pageEl));

        //setup step
        var builPageRef = this.buildPage.bind(this);
        setTimeout(function()
        {
            builPageRef(subStep,currentStepName,pageEl);
        }, 10)



        //update header
        this.updateHeader($id);

        //cache
       // this.cachePage(subStep,currentStepName,pageEl);
    }

    this.buildPage = function(page, pageName, pageEl)
    {
        if(page != null)
        {
            if(page != null && cache[pageName] == undefined)
                page.setup();
            else if(page.refresh != undefined)
                page.refresh();
        }

        //cache the page
        this.cachePage(page, pageName, pageEl);
    }

    this.cachePage = function(page, pageName, pageEl)
    {
        //cache the page
        if(page != null)
        {
            var pageToCache = {};
            pageToCache.page = page; //cache the object
            pageToCache.pageEl = pageEl; //cache the HTML
            cache[pageName] = pageToCache;
        }
    }



    /**
     * update header
     */
    this.updateHeader = function($id)
    {
        //update btns
        if($id == SETTINGS_HOME)
            this.header.showLeftBtn('btnMenu');
        else
            this.header.showLeftBtn('btnBack');

        //update title
        switch($id)
        {
            case SETTINGS_HOME:
                this.header.changeTitle(ResourceManager.getString("settings_home"));
                break;

            case SETTINGS_LANGUAGE:
                this.header.changeTitle(ResourceManager.getString("settings_languages"));
                break;

            case SETTINGS_PRIVACY:
                this.header.changeTitle(ResourceManager.getString("settings_privacy_policy"));
                break;
        }
    }

    /**
     * Show header
     */
    this.showHeader = function()
    {
        this.headerContainer = this.el.find('#view-header');
        this.header = new HeaderView("default",ResourceManager.getString("header_title_settings"));
        this.headerContainer.html(this.header.render().el);
        this.header.setup();
        this.header.showLeftBtn('btnMenu');
        this.header.setBtnBack('#settings');
    }

    /**
     * get header instance
     */
    this.getHeader = function()
    {
        return this.header;
    }


}