var SettingsHomeView = SettingsStepAbstract.extend({

    init: function($parent)
    {
        this._super(TemplateManager.TEMPLATE_SETTINGS_HOME,$parent);
    },

    addListeners: function()
    {
        this.items = this.content.find('.settingsList ul li');
        for(var i=0; i<this.items.length; i++)
        {
            var btn = this.items[i];
            btn.addEventListener('click', this.itemClickHandler.bind(this));
        }
    },

    /**
     * item list click handler
     */
    itemClickHandler : function(e)
    {
        var target = e.currentTarget;
        var  id = target.id;

        switch(id)
        {
            case "settings-clearLocalStorage":
                PopupManager.getInstance().openConfirmPopup(ResourceManager.getString("popup_clear_localdata_title"),ResourceManager.getString("popup_clear_localdata_desc"),this.clearLocalStorageAndReload.bind(this),null)
                break;
            case "settings-version":
                //do nothing
                break;
            default:
                window.location.hash = "#settings/"+id;
        }

    },

    /**
     * Clears local storage
     * All data of all user will be deleted
     * Reload window = complete refresh
     */
    clearLocalStorageAndReload : function()
    {
        Infos.localStorage.clearLocalData();
        location.reload(true);
    }

});