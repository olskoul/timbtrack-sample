var SettingsPrivacyView = SettingsStepAbstract.extend({

    init: function($parent)
    {
        this._super(TemplateManager.TEMPLATE_SETTINGS_PRIVACY,$parent);
    },

    setup: function()
    {
        this.header.setBtnBack('#settings');

        this.lang = Infos.lang;
        this.templateData = {data:Infos.generatePrivacyTemplateData()};
        this.el.html(TemplateManager.getInstance().getTemplate(this.templateName, this.templateData));

        this.addListeners();
    },

    refresh: function()
    {
        this._super();
    },

    render: function()
    {

        return this;
    }

});