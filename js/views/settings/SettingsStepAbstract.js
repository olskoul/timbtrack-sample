var SettingsStepAbstract = Class.extend({

    init: function($templateName,$parent)
    {
        this.templateName = $templateName;
        this.parent = $parent;
        this.header = this.parent.getHeader();
        this.el = $('<div/>'); // Define a div wrapper for the view. The div wrapper is used to attach events.
        this.content = $('<div/>');  // Define a div that will be the content
        this.el.html(this.content);

    },

    render: function()
    {


    },

    refresh: function()
    {
        if(this.lang != Infos.lang)
        {
            this.lang = Infos.lang;
            this.reset();
        }
    },

    reset : function()
    {
        //clear from memory
        //var clone = this.content[0].cloneNode(true);
        this.content.empty();
        this.setup();
    },


    setup: function()
    {
        this.lang = Infos.lang;
        this.content.html(TemplateManager.getInstance().getTemplate(this.templateName, this.templateData));

        this.addListeners();
    },

    addListeners: function()
    {
    }

});




