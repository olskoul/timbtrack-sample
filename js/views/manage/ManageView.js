var ManageView = function (adapter, step) {

    var bottomBtnsWrapper;
    var changeBatchBtn;
    var deleteBatchBtn;
    var batchSelect;
    var allTabs;
    var allCheckboxes;
    var allBatchCheckboxes;
    var allMarkingCheckboxes;
    var selectedBatch = "";
    var selectedMarkings = [];

    /* ---------------------------------- Sort of constructor ---------------------------------- */
    this.initialize = function () {
        // Define a div wrapper for the view. The div wrapper is used to attach events.
        this.el = $('<div/>');
        this.content = $('<div/>');
        this.el.html(this.content);
        this.content.html(TemplateManager.getInstance().getTemplate(TemplateManager.TEMPLATE_EMPTY));
        this.showHeader();
    };



    /* ---------------------------------- Public methods ---------------------------------- */
   /* this.render = function()
    {
        //generate empty template for faster display
        var htmlTemp  = TemplateManager.getInstance().getTemplate(TemplateManager.TEMPLATE_EMPTY,null,false);
        this.el.html("");
        this.el.html(htmlTemp);


        //Show header
        this.headerContainer = this.el.find('#view-header');
        this.showHeader();
        return this;

    };*/

    this.refresh = function()
    {
       this.reset();
    };

    this.reset = function()
    {
        //clear from memory
        this.el.empty();
        this.setup();

        //reset vars
        selectedBatch = "";
        selectedMarkings = [];

        //this.initialize();
        this.setup();
    };

    this.setup = function()
    {
        //rendered lang
        this.lang = Infos.lang;

        this.wrapper = $('#manage-wrapper');

        //generate template data:
        Handlebars.registerHelper ("markingIdToLabel", function (id) {


            var markingVo = MarkingManager.getInstance().getMarkingById(id);
            var unit = (markingVo.kind == "wood")?ResourceManager.getString("common_stere_s"):"m³";
            var speciesLabel = SpeciesManager.getInstance().getInitialsLabel(markingVo.species);
            var finalLabel = "\""+speciesLabel +"\" : "+markingVo.getTonnage()+" "+unit;
            return finalLabel;
        });

        var data =  {data:MarkingManager.getInstance().generateTemplateData()};

        //render wrapper template
        var htmlTemp  = TemplateManager.getInstance().getTemplate(TemplateManager.TEMPLATE_MANAGE,data,false);
        this.el.html("");
        this.el.html(htmlTemp);

        this.headerContainer = this.el.find('#view-header');

        //Show header
        this.showHeader();

        //Refs
        allTabs = $('.tabContent');
        allBatchCheckboxes = $('.manage-batch-checkbox');
        allMarkingCheckboxes = $('.manage-marking-checkbox');
        allCheckboxes = $( ":checkbox" );
        batchSelect = $( "#batchSelect" );

        //hide batchSelect
        batchSelect.css('display','none');
        MarkingManager.getInstance().getBatchSelect(batchSelect[0]);
        batchSelect[0].addEventListener('change', this.batchSelectHandler.bind(this));

        //tabs
        var tabs = $(".tabWrap li");
        for(var i=0; i<tabs.length; i++)
        {
            var tab = tabs[i];
            tab.addEventListener('click', this.tabClickHandler.bind(this));
        }

        //Marking Accordion
        var groupMarkingItem = $('.markingGroupItem h1');
        for(var i=0; i<groupMarkingItem.length; i++)
        {
            var item = groupMarkingItem[i];
            item.addEventListener('click', this.markingGroupClickHandler.bind(this));

            var parent = $(item).parent();
            var infosBtns = parent.find('.infoMarking');

            for(var y=0; y<infosBtns.length; y++)
            {
                var infoBtn = infosBtns[y];
                infoBtn.addEventListener('click', this.infoMarkingClickHandler.bind(this));
            }
        }

        //Batches Accordion
        var batchesItem = $('.batchItem h1');
        for(var i=0; i<batchesItem.length; i++)
        {
            var item = batchesItem[i];
            item.addEventListener('click', this.batcheItemClickHandler.bind(this));

            var parent = $(item).parent();
            var infosBtns = parent.find('.infoMarking');

            for(var y=0; y<infosBtns.length; y++)
            {
                var infoBtn = infosBtns[y];
                infoBtn.addEventListener('click', this.infoMarkingClickHandler.bind(this));
            }

        }

        //inventory Accordion
        var inventoryItem = $('.inventoryItem h1');
        for(var i=0; i<inventoryItem.length; i++)
        {
            var item = inventoryItem[i];
            item.addEventListener('click', this.inventoryItemClickHandler.bind(this));

            var parent = $(item).parent();
            var infosBtns = parent.find('.infoMarking');

            for(var y=0; y<infosBtns.length; y++)
            {
                var infoBtn = infosBtns[y];
                infoBtn.addEventListener('click', this.infoMarkingClickHandler.bind(this));
            }

        }

        //Bottom btns
        bottomBtnsWrapper = $('#batchActionBtn');
        changeBatchBtn = $('#changeBatchBtn');
        deleteBatchBtn = $('#deleteBatchBtn');

        changeBatchBtn[0].addEventListener('click', this.changeBatchClickHandler.bind(this));
        deleteBatchBtn[0].addEventListener('click', this.deleteBatchClickHandler.bind(this));

        //checkboxes click markings
        for(var y=0; y<allMarkingCheckboxes.length; y++)
        {
            var checkbox = allMarkingCheckboxes[y];
            checkbox.addEventListener('click', this.markingCheckboxClickHandler.bind(this));
        }
        //checkboxes click batches
        for(var y=0; y<allBatchCheckboxes.length; y++)
        {
            var checkbox = allBatchCheckboxes[y];
            checkbox.addEventListener('click', this.batchCheckboxClickHandler.bind(this));
        }

        //Hide bottom btns by defaults
        this.showBottomBtns(false);

        //open Default tab
        this.openTabByID('tabMarking');
    }


    /* ---------------------------------- Privates methods ---------------------------------- */

    /**
     * Change batch btn click handler
     */
    this.changeBatchClickHandler = function(e)
    {
        this.showBatchSelect();
    }

    /**
     * Delete batch btn click handler
     */
    this.deleteBatchClickHandler = function(e)
    {
       if(selectedBatch != "")
       {
           var batchToDeleteVo = MarkingManager.getInstance().getBatchById(selectedBatch);

           //First move all markings into the default batch
           $.each(batchToDeleteVo.markings, function(i,marking)
           {
               var markingVo = MarkingManager.getInstance().getMarkingById(batchToDeleteVo.markings[0]);
               MarkingManager.getInstance().addMarkingToBatch(markingVo,MarkingManager.getInstance().getUserDefaultBatch().id);
           });


           //then delete the batch
           MarkingManager.getInstance().deleteBatch(selectedBatch);

           //refresh
           this.refresh();

           //Save
           Infos.saveUserData();
       }
    }

    /**
     * open batch selection
     */
    this.showBatchSelect = function ()
    {
        batchSelect[0].selectedIndex = 0;
        batchSelect.css('display','block');
        var event;
        event = document.createEvent('MouseEvents');
        event.initMouseEvent('mousedown', true, true, window);
        batchSelect[0].dispatchEvent(event);

    };

    /**
     * Batch selected handler
     */
    this.batchSelectHandler = function()
    {
        batchSelect.css('display','none');
        var newbatchID  = batchSelect[0][batchSelect[0].selectedIndex].value;

        if(newbatchID != 'new')
        {
            this.changeBatch(newbatchID);
        }
        else
        {
            PopupManager.getInstance().openNewBatchPopup(this.newBatchPopupValidatedHandler.bind(this));
        }
    }

    /**
     * New batch popup validated
     * Create the batch with the data received from the popup
     * @data.batchName
     * @data.isInventory
     */
    this.newBatchPopupValidatedHandler = function($data)
    {
        //create new batch
        var newBatchId = MarkingManager.getInstance().createBatch($data.name,$data.isInventory, false);

        //change batch for the selected markings
        this.changeBatch(newBatchId);
    }

    /**
     * Change batch btn click handler
     */
    this.changeBatch = function($batchId)
    {
        for(var y=0; y<selectedMarkings.length; y++)
        {
            var markingVo = MarkingManager.getInstance().getMarkingById(selectedMarkings[y]);
            MarkingManager.getInstance().addMarkingToBatch(markingVo,$batchId);
        }

        //refresh data
        this.refresh();

        //Save changes
        Infos.saveUserData();
    }


    /**
     * Marking checkbox click handler
     */
    this.markingCheckboxClickHandler = function(e)
    {
        //deselect all batch
        this.resetSelectedBatches();

        var target = $(e.currentTarget)[0];
        var id = target.id.split('_')[1];

        //if checked add selected marking to the list
        if(target.checked)
        {
            selectedMarkings.push(id);
            //Show action buttons
            this.showBottomBtns(true,false,true);
        }
        else // else remove marking from list and check if we still show the btns
        {
            this.removeMarkingFromSelected(id);
            if(selectedMarkings.length < 1)
                this.showBottomBtns(false);
        }


    }

    /**
     * Batch checkbox click handler
     */
    this.removeMarkingFromSelected = function(id)
    {
        for(var i=0; i<selectedMarkings.length; i++)
        {
            var markingID = selectedMarkings[i];
            if(markingID == id)
            {
                selectedMarkings.splice(i, 1);
                return;
            }
        }
    }

    /**
     * Batch checkbox click handler
     */
    this.batchCheckboxClickHandler = function(e)
    {
        //deselect all markings
        this.resetSelectedMarkings();

        var target = $(e.currentTarget)[0];
        var id = target.id.split('checkboxLot')[1];
        //alert(id);
        var batchVo = MarkingManager.getInstance().getBatchById(id);

        if(target.checked)
        {
            this.resetSelectedBatches();
            target.checked = true;
            selectedBatch = id;

            if(batchVo.isDefault == false)
            this.showBottomBtns(true, true, false);
        }
        else
        {
            selectedBatch = "";
            this.showBottomBtns(false);
        }

    }
    /**
     * Show/Hide bottom btns
     */
    this.showBottomBtns = function(flag, showDeleteBtn, showChangeBtn)
    {
        var deleteDisplay = (showDeleteBtn)?'table-cell':'none';
        deleteBatchBtn.css('display',deleteDisplay);

        var changeDisplay = (showChangeBtn)?'table-cell':'none';
        changeBatchBtn.css('display',changeDisplay);

        if(flag)
        {
            bottomBtnsWrapper.css('display','table');
            allTabs.css('bottom',bottomBtnsWrapper.height());
        }
        else
        {
            bottomBtnsWrapper.css('display','none');
            allTabs.css('bottom',0);
        }
    }

    /**
     * Marking info click Handler
     */
    this.tabClickHandler = function(e)
    {
        console.log('ManageView > tabClickHandler');

        //open tab by iD
        var target = $(e.currentTarget)[0];
        this.openTabByID(target.id);
    }

    /**
     * Open Tab by id
     */
    this.openTabByID = function($id)
    {
        //reset selected items
        this.resetSelectedMarkings();
        this.resetSelectedBatches();

        this.showBottomBtns(false);

        //hide all first
        allTabs.css('display', 'none');

        //Off color all
        $('.tabWrap li').css('background-color','#83bb3f');

        console.log("open tab: "+$id);
        switch($id)
        {
            case 'tabMarking':
                $('.markingGroupList').css('display', 'block');
                $('#tabMarking').css('background-color','#659428');
                break;

            case 'tabBatches':
                $('.batchesList').css('display', 'block');
                $('#tabBatches').css('background-color','#659428');
                break;

            case 'tabInventory':
                $('.inventoryList').css('display', 'block');
                $('#tabInventory').css('background-color','#659428');
                break;
        }
    }


    /**
     * Reset selected markings
     */
    this.resetSelectedMarkings = function(e)
    {
        selectedMarkings = [];
        allMarkingCheckboxes.prop('checked', false);
        selectedBatch = "";

    }

    /**
     * Reset selected markings
     */
    this.resetSelectedBatches = function(e)
    {
        allBatchCheckboxes.prop('checked', false);
        selectedBatch = "";
    }

    /**
     * Marking info click Handler
     */
    this.infoMarkingClickHandler = function(e)
    {
        var target = $(e.currentTarget);
        var id = target.closest("p").attr("id");
        id = id.split('marking')[1];
        PopupManager.getInstance().openInfoMarkingPopup(MarkingManager.getInstance().getMarkingById(id), true);
        console.log("ManageView > Infos marking clicked")
    }

    /**
     * Marking group item click Handler
     */
    this.markingGroupClickHandler = function(e)
    {
        console.log('ManageView > markingGroupClickHandler');
        var target = $(e.currentTarget);
        var parent = target.parent();
        var markings = parent.find('p');

        if(markings.css('display') == 'none')
        {
            markings.css('display', 'block');
        }
        else
        {
            markings.css('display', 'none');
        }
    }

    /**
     * batches item click Handler
     */
    this.batcheItemClickHandler = function(e)
    {
        console.log('ManageView > batcheItemClickHandler');
        var target = $(e.currentTarget);
        var parent = target.parent();
        var markings = parent.find('p');
        var price = parent.find('p.batch-price');

        if(markings.css('display') == 'none')
        {
            markings.css('display', 'block');
            price.css('display', 'table');

        }
        else
        {
            markings.css('display', 'none');
            price.css('display', 'none');
        }

    }

    /**
     * inventory item click Handler
     */
    this.inventoryItemClickHandler = function(e)
    {
        console.log('ManageView > inventoryItemClickHandler');
        var target = $(e.currentTarget);
        var parent = target.parent();
        var markings = parent.find('p');

        if(markings.css('display') == 'none')
        {
            markings.css('display', 'block');
        }
        else
        {
            markings.css('display', 'none');
        }



    }

    /**
     * Show header
     */
    this.showHeader = function()
    {
        this.headerContainer = this.el.find('#view-header');
        this.header = new HeaderView("default",ResourceManager.getString("header_title_manage"));
        this.headerContainer.html(this.header.render().el);
        this.header.setup();
        this.header.showLeftBtn("btnMenu");
        this.header.setBtnBack('#manage');

        /*this.header = new HeaderView("default",ResourceManager.getString("header_title_manage"));
        this.headerContainer.html(this.header.render().el);
        this.header.setup();*/
    }

    this.initialize();

}