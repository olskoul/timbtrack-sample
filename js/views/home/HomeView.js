var HomeView = function (adapter) {

/* ---------------------------------- Sort of constructor ---------------------------------- */
    this.initialize = function () {

        this.el = $('<div/>');
        this.content = $('<div/>');
        this.el.html(this.content);
        this.content.html(TemplateManager.getInstance().getTemplate(TemplateManager.TEMPLATE_EMPTY));
    };

/* ---------------------------------- Public methods ---------------------------------- */

    this.refresh = function()
    {
        if(this.lang != Infos.lang)
        {
            this.reset();
        }
        else
        {
            this.checkSync();
        }
    };

    this.reset = function()
    {
        //clear from memory
        this.el.empty();
        this.setup();
    };

    this.setup = function()
    {
        //render wrapper template
        var data =  {data:Infos.user.generateTemplateData()};
        this.el.html(TemplateManager.getInstance().getTemplate(TemplateManager.TEMPLATE_HOME, data));

        //Rendered lang
        this.lang = Infos.lang;

        //disable mouse on icon (better click zone)
        var icons = $('li .menu-icon');

        //Draw markings
        $.each(icons, function(i,icon){
           BtnUtils.activateBtn(icon,false,false);
        });


        $('.notification')[0].addEventListener('click',this.doSync.bind(this));
        this.hideSyncNeedNotification();
        this.checkSync();
    };

    this.checkSync = function()
    {
        console.log("HomeView > checkSync");
        if(SyncManager.getInstance().doWeNeedToSync())
            this.showSyncNeedNotification();
        else
            this.hideSyncNeedNotification();
    };

    this.showSyncNeedNotification = function()
    {
        $('.notification').css('display','table');
        $('.notification-content')[0].innerHTML = ResourceManager.getString('notification_sync_needed');
    };

    this.hideSyncNeedNotification = function()
    {
        $('.notification').css('display','none');
    };

    this.doSync = function()
    {
        console.log("HomeView > doSync");
        Loading.showLoading("loading_sync");
        this.hideSyncNeedNotification();
        var callBackSuccess = this.manualSyncHandlerSuccess.bind(this);
        var callBackError = this.onError.bind(this);
        SyncManager.getInstance().startSync(callBackSuccess,callBackError);
    };

    /**
     * Manual sync complete handler
     */
    this.manualSyncHandlerSuccess = function()
    {
        this.checkSync();
    }

    /**
     * Sync error function
     */
    this.onError = function(msg)
    {
        Loading.hideLoading();
        console.log("HomeView > DoSync > onError: "+msg);
        PopupManager.getInstance().openAlertPopup(ResourceManager.getString("popup_sync_needed_title"), ResourceManager.getString("popup_sync_error_fatal_desc") + "("+msg+")");
        ServicesManager.getInstance().mail("WARNING > HomeView.onError > From DoSync > message:\n"+msg, true);
    }

    this.initialize();

};