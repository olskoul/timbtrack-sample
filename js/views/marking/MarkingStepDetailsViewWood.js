var MarkingStepDetailsWood = MarkingStepAbstract.extend({

    init: function()
    {
        this._super(MarkingManager.STEP_KIND_WOOD, TemplateManager.TEMPLATE_MARKING_DETAILS_WOOD);
    },


    checkIfEditionMode : function()
    {
        return (this.markingVo != undefined
            && this.markingVo != null
            && this.markingVo.kind == 'wood'
            && (this.markingVo.calculation.woodVolumes.length > 0 || this.markingVo.calculation.quickTonnage != null)
            );
    },

    setup: function()
    {
        this.lang = Infos.lang;
        this.content.html(TemplateManager.getInstance().getTemplate(this.templateName));

        this.quickTonnageBtn = this.el.find('#quickTonnageBtn')[0];
        this.byVolumeBtn = this.el.find('#byVolumeBtn')[0];
        this.input = this.el.find('#quickEstimInput');

        this.addListeners();
        this.header.showLeftBtn("btnMenu");
    },

    reFillForm: function()
    {
        if(this.checkIfEditionMode() == true)
        {
            if(this.markingVo.quickTonnage == true)
            {
                this.input.val(this.markingVo.calculation.quickTonnage);
                BtnUtils.activateBtn( this.byVolumeBtn , false, true);
            }
            else
            {
                this.byVolumeClickHandler();
            }
        }
        else
        {
            //Customer wants to keep this value...
            //this.input.val("");
            BtnUtils.activateBtn( this.byVolumeBtn , true, true);
        }

    },

    addListeners: function()
    {
        this._super();

        //quick Estim Btn
        this.quickTonnageBtn.addEventListener('click',this.quickTonnageClickHandler.bind(this));

        //By volume Btn
        this.byVolumeBtn.addEventListener('click',this.byVolumeClickHandler.bind(this));

    },

    byVolumeClickHandler : function()
    {
        this.markingVo.quickTonnage = false;
        this.markingVo.calculation.quickTonnage = null;
        window.location.hash = "#marking/"+MarkingManager.STEP_KIND_WOOD_BY_VOLUMES+"/"; //?groupid=" + this.groupVo.id + "&markingid="+this.markingVo.id ;
    },

    quickTonnageClickHandler : function()
    {

        if(this.input.val() == "")
        {
            PopupManager.getInstance().openAlertPopup("", ResourceManager.getString("popup_field_wood_nochoice_made"));
            return false;
        }


        if(!this.checkIfEditionMode() == true)
        {
            this.markingVo.quickTonnage = true;
            this.markingVo.calculation.quickTonnage = this.input.val();
        }
        else
        {
            //volume = this.markingVo.calculation.woodVolumes[0];
        }

        //go to summary page
        window.location.hash = "#marking/"+MarkingManager.STEP_KIND_WOOD_SUMMARY+"/";
    },

    validate: function()
    {

    }
});