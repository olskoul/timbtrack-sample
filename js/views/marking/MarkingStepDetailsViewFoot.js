var MarkingStepDetailsFoot = MarkingStepAbstract.extend({

    init: function()
    {
        this._super("details/"+MarkingManager.STEP_KIND_FOOT, TemplateManager.TEMPLATE_MARKING_DETAILS_FOOT);

        this.qualityArray = ["F", "E", "D", "C", "B", "A"];
    },

    checkIfEditionMode : function()
    {
        return (this.markingVo != undefined
            && this.markingVo != null
            && this.markingVo.kind == 'foot'
            && this.markingVo.calculation.circumference != null);
    },

    setup: function()
    {
        this._super();

        //Slider outupt field quality
        this.sliderOutput = this.el.find('#quality-value');
        //Slider outupt field scrollin
        this.sliderScrollingOutput = this.el.find('#scrolling-value');

        //select no by default
        this.selectBtn(this.specialtreeNoBtn,true);

        var specialTreeP = this.el.find('.yes-no .left p');
        var parentHeight = specialTreeP.parent().height();
        var position = Math.round((parentHeight - specialTreeP.height())/2);
        specialTreeP.css("top", position);

        this.circumference = $('#circumferenceInput');
        this.height = $('#heightInput');
        this.speciesSelect = $('#speciesSelect')[0];

        SpeciesManager.getInstance().getSpeciesHTML(this.speciesSelect); //Todo fix this

    },

    reFillForm: function()
    {
        if(this.checkIfEditionMode() == true)
        {
            this.circumference.val(this.markingVo.calculation.circumference);
            this.height.val(this.markingVo.calculation.height);

            for(var i= 0; i < this.speciesSelect.length; i++)
            {
                if(this.speciesSelect[i].value == this.markingVo.species)
                    this.speciesSelect.selectedIndex = i;
            }

            this.selectBtn(this.specialtreeYesBtn,this.markingVo.specialTree);
            this.selectBtn(this.specialtreeNoBtn,!(this.markingVo.specialTree));

            this.sliderQuality[0].value = this.markingVo.quality;
            this.sliderScrolling[0].value = this.markingVo.calculation.scrolling;
        }
        else
        {
            //customer want to reset cicumference only
            this.circumference.val("");

            /*this.height.val("");

            this.speciesSelect.selectedIndex = 0;


            this.selectBtn(this.specialtreeYesBtn,false);
            this.selectBtn(this.specialtreeNoBtn,true);

            var options = this.sliderQuality( 'option' );
            this.sliderQuality[0]( 'values', [ options.min, options.max ] );

            var options =  this.sliderScrolling( 'option' );
            this.sliderScrolling[0]( 'values', [ options.min, options.max ] );*/

        }

    },

    addListeners: function()
    {
        this._super();

        //Slider quality
        this.sliderQuality = this.el.find('#quality-slider');
        this.sliderQuality[0].addEventListener('change',this.qualityChangeHandler.bind(this));

        //Slider scrolling
        this.sliderScrolling = this.el.find('#scrolling-slider');
        this.sliderScrolling[0].addEventListener('change',this.scrollingChangeHandler.bind(this));

        //yes-no
        /*this.specialtreeNoBtn = this.el.find('#specialtree-no');
        this.specialtreeYesBtn = this.el.find('#specialtree-yes');
        this.specialtreeNoBtn[0].addEventListener('click', this.yesNoClickHandler.bind(this));
        this.specialtreeYesBtn[0].addEventListener('click', this.yesNoClickHandler.bind(this));*/


    },



    scrollingChangeHandler: function(e)
    {
        var sliderValue = this.sliderScrolling[0].value;
        this.sliderScrollingOutput[0].innerHTML = sliderValue;
    },

    qualityChangeHandler: function(e)
    {
        var sliderValue = this.sliderQuality[0].value;
        this.sliderOutput[0].innerHTML = this.qualityArray[sliderValue/20];
    },

    yesNoClickHandler: function(e)
    {
        /*var target = $(e.currentTarget);

        this.selectBtn(this.specialtreeNoBtn,(target[0] == this.specialtreeNoBtn[0]));
        this.selectBtn(this.specialtreeYesBtn,(target[0] == this.specialtreeYesBtn[0]));*/
    },

    selectBtn: function(target, flag)
    {
       /* var bgColor = (flag)?"#95c758":"#ffffff";
        var textColor = (flag)?"#ffffff":"#2c2e33";

        target.css("background-color", bgColor);
        target.css("color", textColor);

        this.isSpecialTree = !(target[0] == this.specialtreeNoBtn[0]);*/
    },

    validate: function()
    {
        //reset all errors
        this.showError(this.circumference,"number",false);
        this.showError(this.height,"number",false);
        //this.showError(this.speciesSelect,"select",false);
        var hasError = false;

        //Form check
        if(this.circumference.val() == "")
        {
            this.showError(this.circumference,"number",true);
            hasError = true;
        }
        //Form check
        if(this.height.val() == "")
        {
            this.showError(this.height,"number",true);
            hasError = true;
        }
        //Form check
        if(this.speciesSelect[this.speciesSelect.selectedIndex].value == "")
        {
            this.showError(this.speciesSelect,"select",true);
            hasError = true;
        }

        if(hasError == true)
        {
            PopupManager.getInstance().openAlertPopup("", ResourceManager.getString("popup_field_empty"));
            return false;
        }

        this.markingVo.quality = this.sliderOutput[0].innerHTML;
        //this.markingVo.specialTree = this.isSpecialTree;
        this.markingVo.calculation.circumference = this.circumference.val();
        this.markingVo.calculation.height = this.height.val();
        this.markingVo.calculation.scrolling = this.sliderScrolling[0].value;
        this.markingVo.setSpecies(this.speciesSelect[this.speciesSelect.selectedIndex].value);
        this.markingVo.tonnage = this.markingVo.getTonnage(true);

        return "/"+MarkingManager.STEP_END+"/"; //?groupid=" + this.groupVo.id + "&markingid="+this.markingVo.id;
    }
});