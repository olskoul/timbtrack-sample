var MarkingStepDetailsWoodByVolume = MarkingStepAbstract.extend({

    init: function()
    {
        this._super(MarkingManager.STEP_KIND_WOOD_BY_VOLUMES, TemplateManager.TEMPLATE_MARKING_DETAILS_WOOD_BY_VOLUME);
    },

    checkIfEditionMode : function()
    {
        return (this.markingVo != undefined
            && this.markingVo != null
            && this.markingVo.kind == 'wood'
            && this.markingVo.calculation.woodVolumes.length > 0);
    },

    setup: function()
    {
        this._super();

        this.volumeList = this.el.find('#volumeList');
        this.widthInput = this.el.find('#widthInput');
        this.heightInput = this.el.find('#heightInput');
        this.depthInput = this.el.find('#depthInput');

    },

    reFillForm: function()
    {
        if(this.checkIfEditionMode())
        {
            this.renderList();
        }
        else
        {
        }

    },

    addListeners: function()
    {
        //Add volume btn
        this.addVolumeBtn = this.el.find('#addVolumeBtn');
        this.addVolumeBtn[0].addEventListener('click',this.addVolumeClickHandler.bind(this));
    },

    addVolumeClickHandler : function(e)
    {
        this.showError(this.widthInput,"number",false);
        this.showError(this.heightInput,"number",false);
        this.showError(this.depthInput,"number",false);
        var hasError = false;

        if(this.widthInput.val() == "")
        {
            this.showError(this.widthInput,"number",true);
            hasError = true;
        }
        if(this.heightInput.val() == "" )
        {
            this.showError(this.heightInput,"number",true);
            hasError = true;
        }
        if(this.depthInput.val() == "")
        {
            this.showError(this.depthInput,"number",true);
            hasError = true;
        }

        if(hasError == true)
        {
            PopupManager.getInstance().openAlertPopup("", ResourceManager.getString("popup_field_empty"));
            return false;
        }


        var volume = new WoodVolumesVo(this.widthInput.val(), this.heightInput.val(), this.depthInput.val());
        this.markingVo.calculation.woodVolumes.push(volume);

        this.renderList();
    },

    renderList: function()
    {
        var htmlStr = "";
        var d = document.createDocumentFragment();
        this.volumeList.empty();

        for(var i=0; i<this.markingVo.calculation.woodVolumes.length; i++)
        {
            var volume = this.markingVo.calculation.woodVolumes[i];

            var li = document.createElement('li');
            var span = document.createElement('span');

            var tagI = document.createElement('i');
            var iId = document.createAttribute('id');
            var iclass = document.createAttribute('class');
            iId.value = i;
            iclass.value = "fa fa-trash-o";
            tagI.setAttributeNode(iId);
            tagI.setAttributeNode(iclass);

            tagI.addEventListener('click',this.deleteVolumeclickHandler.bind(this));

            var notAccurateVolume = volume.getVolume();
            notAccurateVolume = (notAccurateVolume > 0.001)?Math.floor(volume.getVolume() * 1000) / 1000:notAccurateVolume;
            var text = ResourceManager.getString("common_volume") +" "+(i+1)+" - "+ notAccurateVolume+" "+ ResourceManager.getString("common_stere_s");

            span.innerHTML = text;
            li.appendChild(span);
            li.appendChild(tagI);
            d.appendChild(li);
            //this.volumeList[0].appendChild(li);


        }

        this.volumeList[0].appendChild(d);
    },

    deleteVolumeclickHandler : function(e)
    {
        var target = e.currentTarget;
        this.deleteVolume(target.id)
    },

    deleteVolume : function(indexToDelete)
    {
        this.markingVo.calculation.woodVolumes.splice(indexToDelete,1);
        this.renderList();
    },

    validate: function()
    {

        return "/"+MarkingManager.STEP_KIND_WOOD_SUMMARY+"/";//?groupid=" + this.groupVo.id + "&markingid="+this.markingVo.id;
    }
});