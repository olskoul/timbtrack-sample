var MarkingStepDetailsWoodSummary = MarkingStepAbstract.extend({

    lastSelectedSpecies:0,

    init: function()
    {
        this._super(MarkingManager.STEP_KIND_WOOD_SUMMARY, TemplateManager.TEMPLATE_MARKING_DETAILS_WOOD_SUMMARY);
    },

    checkIfEditionMode : function()
    {
        return (this.markingVo != undefined
            && this.markingVo != null
            && this.markingVo.kind == 'wood'
            && (this.markingVo.calculation.woodVolumes.length > 0 || this.markingVo.calculation.quickTonnage != null)
            );
    },

    setup: function()
    {
        var templateData  = {data:this.markingVo.generateWoodSummaryTemplateData()};
        this.lang = Infos.lang;
        this.content.html(TemplateManager.getInstance().getTemplate(this.templateName,templateData));


        this.speciesSelect = $('#speciesSelect')[0];
        SpeciesManager.getInstance().getSpeciesHTML(this.speciesSelect);
        this.speciesSelect.selectedIndex = this.lastSelectedSpecies;

        this.addListeners();
        this.header.showLeftBtn("btnMenu");


    },

    reFillForm: function()
    {
        if(this.checkIfEditionMode() == true)
        {
            if(this.markingVo != null && this.markingVo.kind == 'wood')
            {
                for(var i= 0; i < this.speciesSelect.length; i++)
                {
                    if(this.speciesSelect[i].value == this.markingVo.species)
                        this.speciesSelect.selectedIndex = i;
                }
            }
        }
        else
        {
            this.speciesSelect.selectedIndex = this.lastSelectedSpecies;
        }

    },

    addListeners: function()
    {

    },


    validate: function()
    {
        this.showError(this.speciesSelect,"select",false);
        var hasError = false;

        if(this.speciesSelect[this.speciesSelect.selectedIndex].value == "")
        {
            this.showError(this.speciesSelect,"select",true);
            hasError = true;
        }

        if(hasError == true)
        {
            PopupManager.getInstance().openAlertPopup("", ResourceManager.getString("popup_field_empty"));
            return false;
        }


        this.markingVo.setSpecies(this.speciesSelect[this.speciesSelect.selectedIndex].value);
        this.markingVo.tonnage = this.markingVo.getTonnage(true);

        this.lastSelectedSpecies = this.speciesSelect.selectedIndex

        return "/"+MarkingManager.STEP_END+"/";  //?groupid=" + this.groupVo.id + "&markingid="+this.markingVo.id;
    }
});