var MarkingView = function (adapter, step) {

    var currentStep = (step) ? step : MarkingManager.STEP_NEW_GROUP;
    var cache = {};
    var headerHeight = "56px";
    var contentPosY = "128px";

    /* ---------------------------------- Sort of constructor ---------------------------------- */
    this.initialize = function () {
        // Define a div wrapper for the view. The div wrapper is used to attach events.
        this.el = $('<div/>');
        this.content = $('<div/>');
        this.el.html(this.content);
        this.content.html(TemplateManager.getInstance().getTemplate(TemplateManager.TEMPLATE_EMPTY));
        this.showHeader();
    };

    /* ---------------------------------- Public methods ---------------------------------- */


    this.refresh = function()
    {
        if(this.lang != Infos.lang)
        {
            //this.yesCancel(true);
            this.reset();
        }
        else
        {

        }
    };

    this.reset = function()
    {
        currentStep = "";
        //clear from memory
        this.el.empty();
        this.setup();
    };

    this.setup = function()
    {
        //rendered lang
        this.lang = Infos.lang;

        //Compile template
        this.el.html(TemplateManager.getInstance().getTemplate(TemplateManager.TEMPLATE_MARKING));
        //Show header
        this.showHeader();

        //Setup references
        this.scrollableContent =  this.el.find('#scrollable-view');
        this.stepContainer =  this.el.find('#marking-content-wrapper');

        this.stepInfosBubble = this.el.find('#stepInfos');
        this.stepInfos = this.el.find('#marking-stepsInfos');
        this.stepBtnContainer = this.el.find('#stepBtn');
        this.endBtnContainer = this.el.find('#endBtn');

        //validate btn
        this.validateBtn = this.el.find('.marking-btn-validate');
        this.validateBtn[0].addEventListener('click', this.validateStep.bind(this));

        //cancel btn
        this.cancelBtn = this.el.find('.marking-btn-cancel');
        this.cancelBtn[0].addEventListener('click', this.cancelNewMarking.bind(this));

        //new marking btn
        this.newMarkingBtn = this.el.find('.marking-btn-new');
        this.newMarkingBtn[0].addEventListener('click', this.newMarkingClickHandler.bind(this));

        //manage btn
        this.manageBtn = this.el.find('.marking-btn-manage');
        this.manageBtn[0].addEventListener('click', this.manageClickHandler.bind(this));

        //Step buttons
        this.stepBtns = [];
        this.stepBtns.push(this.stepInfos.find('#step1'));
        this.stepBtns.push(this.stepInfos.find('#step2'));
        this.stepBtns.push(this.stepInfos.find('#step3'));
        this.stepBtns.push(this.stepInfos.find('#step4'));
        this.stepEl = this.stepInfos.find('#step1');
        this.stepInfosBubble.css('opacity',0);
        this.moveBubble();
        for(var i=0; i<this.stepBtns.length; i++)
        {
            var btn = this.stepBtns[i];
            btn[0].addEventListener('click', this.stepBtnClickHandler.bind(this));
        }

        //init pageSlider
        this.pageSlider = new PageSlider(this.stepContainer);


        //Show step
        this.showStep();

        //hash change for step change
        $(window).on('hashchange', this.showStep.bind(this));

    }

    /* ---------------------------------- Privates methods ---------------------------------- */

    /**
     * Show header
     */
    this.showHeader = function()
    {
        this.headerContainer = this.el.find('#view-header');
        this.header = new HeaderView("default",ResourceManager.getString("header_title_marking"));
        this.headerContainer.html(this.header.render().el);
        this.header.setup();
        this.header.showLeftBtn("btnMenu");
        this.header.setBtnBack('#marking');
    }

    /**
     * update header
     */
    /*this.updateHeader = function($id)
    {
        //update btns
        if($id == SETTINGS_HOME)
            this.header.showLeftBtn('btnMenu');
        else
            this.header.showLeftBtn('btnBack');

        //update title
        switch($id)
        {
            case SETTINGS_HOME:
                this.header.changeTitle(ResourceManager.getString("settings_home"));
                break;

            case SETTINGS_LANGUAGE:
                this.header.changeTitle(ResourceManager.getString("settings_languages"));
                break;

            case SETTINGS_PRIVACY:
                this.header.changeTitle(ResourceManager.getString("settings_privacy_policy"));
                break;
        }
    }*/

    /**
     * Show step in Marking view
     */
    this.showStep = function()
    {
        var hash = window.location.hash;
        if (!hash) return;
        if( !this.validateStep ) return;

        var splittedHash = hash.split("#")[1].split("/");
        var pageName = splittedHash[0];
        var stepName = splittedHash[1];
        var step;
        var stepEl;
        var clearCurrent = UrlUtils.getInstance().getUrlParam("clearcurrent");

        console.log("Marking View > showStep ->  "+ hash);
        console.log("Marking View > showStep -> clear current ="+ clearCurrent);
        if(clearCurrent === "true")
        {
            console.log("Marking View > showStep > Clear current group");
            MarkingManager.getInstance().clearCurrentGroup();
            console.log("Marking View > showStep > Clear current Marking");
            MarkingManager.getInstance().clearCurrentMarking();
        }

        //update header
        if(pageName == "marking")
        this.header.showLeftBtn("btnMenu");


        //Security not to show anything if same step
        if(currentStep && currentStep.stepName == stepName) //&& stepName != MarkingManager.STEP_DETAILS)
        {
            console.log("Marking View > showStep > step is already displayed: "+ currentStep.stepName);
            return;
        }

        //first check the cache
        var cacheName = stepName;
        if(cache[cacheName] != undefined)
        {
            step = cache[cacheName].step;
            stepEl = cache[cacheName].stepEl;
        }
        else
        {
            switch(stepName)
            {
                case MarkingManager.STEP_NEW_GROUP:
                    step = new MarkingStepNewGroup();
                    break;

                case MarkingManager.STEP_KIND:
                    step = new MarkingStepKind();
                    break;

                case MarkingManager.STEP_GPS:
                    step = new MarkingStepGPS();
                    break;

                case MarkingManager.STEP_KIND_FOOT:
                    step = new MarkingStepDetailsFoot();
                    break;

                case MarkingManager.STEP_KIND_WOOD:
                    step = new MarkingStepDetailsWood();
                    break;

                case MarkingManager.STEP_KIND_WOOD_SUMMARY:
                    step = new MarkingStepDetailsWoodSummary();
                    break;

                case MarkingManager.STEP_KIND_WOOD_BY_VOLUMES:
                    step = new MarkingStepDetailsWoodByVolume();
                    break;

                case MarkingManager.STEP_KIND_GROUND:
                    step = new MarkingStepDetailsGround();
                    break;

                case MarkingManager.STEP_END:
                    step = new MarkingStepEnd();
                    break;
            }
        }

        //show btn exceptions
        //Step WOOD does not need a validate btn
        if(stepName == MarkingManager.STEP_KIND_WOOD)
            this.activateBtn(this.validateBtn,false);
        else
            this.activateBtn(this.validateBtn,true);

        //Step new group does not need a cancel btn
        if(stepName == MarkingManager.STEP_NEW_GROUP)
            this.cancelBtn.css("display", "none");
        else
            this.cancelBtn.css("display", "table-cell");



        //Animate, render and build step
        if(step != undefined)
        {
            //ref header
            step.refHeader(this.header);

            //element
            stepEl = step.el;

            //Show page
            this.pageSlider.slidePageFrom($(stepEl),"right");

            //reset element scroll position to 0
            this.scrollableContent[0].scrollTop = 0;

            //build page after transition (timeout)
            var buildPageRef = this.buildPage.bind(this);
            setTimeout(function()
            {
                buildPageRef(step,cacheName, stepEl);
            }, 10)

            //ref the current step
            currentStep = step;

            //update step counter
            this.updateCurrentStepUi();
        }
        else
        {
            console.log("Marking View > showStep > step is undefined "+ stepName);
        }
    }


    this.buildPage = function(step, stepName, stepEl)
    {
        if(step != null)
        {
            if(step != null && cache[stepName] == undefined)
                step.setup();
            else if(step.refresh != undefined)
                step.refresh();

            //Cancelation is not possible on edition mode
            if(step.checkIfCancelable() == true)
                this.activateBtn(this.cancelBtn,true);
            else
                this.activateBtn(this.cancelBtn,false);
        }

        //cache the page
        this.cachePage(step, stepName, stepEl);
    }

    this.cachePage = function(step, stepName, stepEl)
    {
        //cache the page
        if(step != null)
        {
            var pageToCache = {};
            pageToCache.step = step; //cache the object
            pageToCache.stepEl = stepEl; //cache the HTML
            cache[stepName] = pageToCache;
        }
    }

    /**
     * Update stepInfos
     */
    this.updateCurrentStepUi = function()
    {
        var hash = window.location.hash;
        if (!hash) return;

        var splittedHash = hash.split("#")[1].split("/");
        var stepName = splittedHash[1];

        //HIDE - SHOW step InfosBubbles and bottom buttons
        if(stepName == MarkingManager.STEP_END)
        {
            //step Infos bubble
            this.stepInfos.css("display", "none");
            this.scrollableContent.css("top", headerHeight);

            //btns
            this.stepBtnContainer.css("display", "none");
            this.endBtnContainer.css("display", "table");
        }
        else
        {
            //Step Infos bubble
            this.stepInfos.css("display", "block");
            this.scrollableContent.css("top", contentPosY);

            //btns
            this.stepBtnContainer.css("display", "table");
            this.endBtnContainer.css("display", "none");
        }

        //UPDATE step infos bubble
        var stepIndex = 1;
        switch(stepName)
        {
            case MarkingManager.STEP_NEW_GROUP:
                stepIndex = 1;
                break;

            case MarkingManager.STEP_KIND:
                stepIndex = 2;
                break;

            case MarkingManager.STEP_GPS:
                stepIndex = 3;
                break;

            case MarkingManager.STEP_KIND_FOOT:
            case MarkingManager.STEP_KIND_WOOD:
            case MarkingManager.STEP_KIND_WOOD_SUMMARY:
            case MarkingManager.STEP_KIND_WOOD_BY_VOLUMES:
            case MarkingManager.STEP_KIND_GROUND:
                stepIndex = 4;
                break;
        }

        //always clear it just in case
        clearInterval(this.interval);

        if(stepIndex != 0)
        {
            this.stepInfosBubble.html(stepIndex + "<span>/4</span>");
            this.stepEl = this.stepInfos.find('#step'+stepIndex);
            this.interval = setInterval(this.moveBubble.bind(this),100);

            //update clickability of the stepBtn infos
            for(var i=0; i<this.stepBtns.length; i++)
            {
                var btn = this.stepBtns[i];
                if(i <= stepIndex-1)
                    this.activateBtn(btn,true);
                else
                    this.activateBtn(btn,false);

            }
        }


    }

    /**
     * Move step infos bubble to the corresponding steps
     */
    this.moveBubble = function ()
    {
        var x = Math.round(this.stepEl.position().left + this.stepEl.width()/2 - (this.stepInfosBubble.width()/2) +3 );
        var y = Math.round(this.stepEl.position().top + (this.stepEl.height() - this.stepInfosBubble.height())/2);

        //check if the x return a correct value (if not, continue interval until its completely rendered
        if(x != 0 && x != null && x != undefined && x != "" )
        {
            clearInterval(this.interval);
            this.stepInfosBubble.css('left',x);
            this.stepInfosBubble.css('top',y);
            this.stepInfosBubble.css('opacity',1);
        }

    },


    this.stepBtnClickHandler = function (e)
    {
        var target = e.currentTarget;
        switch(target.id)
        {
            case 'step1':
                window.location.hash = "#marking/"+MarkingManager.STEP_NEW_GROUP;
                break;

            case 'step2':
                window.location.hash = "#marking/"+MarkingManager.STEP_KIND;
                break;

            case 'step3':
                window.location.hash = "#marking/"+MarkingManager.STEP_GPS;
                break;

            /*case 'step4':
                window.location.hash = "#marking/"+MarkingManager.STEP_DETAILS;//+"/"+markingVo.kind +"/?groupid=" + groupId + "&markingid="+markingId;
                break;*/
        }

    },

    /**
     * Validate current step before routing to next one
     * returns true or false
     */
    this.validateStep = function()
    {
        if(!currentStep)
        {
            this.showStep();
            return;
        }

        var result = currentStep.validate();
        if(result != false)
        {
            window.location.hash = "#marking"+result;
        }

        return result;
    }

    /**
     * Cancel process confirmation
     */
    this.cancelNewMarking = function()
    {
        if(currentStep != undefined)
        {
            currentStep.cancel();
        }
    }

    /**
     * Start new marking process
     * group is the same
     * clear current marking
     */
    this.newMarkingClickHandler = function()
    {
        var currentMarking = MarkingManager.getInstance().clearCurrentMarking();
        if(currentMarking == null)
        {
            window.location.hash = "#marking/"+MarkingManager.STEP_KIND;
        }
    }

    /**
     * Go to gestion
     */
    this.manageClickHandler = function()
    {
        MarkingManager.getInstance().clearCurrentMarking();
        MarkingManager.getInstance().clearCurrentGroup();
        window.location.hash = "#manage";
    }

    /**
     * Hide or show validate btn
     * @flag true or false
     */
    this.activateBtn = function (target, flag)
    {
        var visibility = (flag)? 1:.5;
        var pointerEvent = (flag)? 'auto':'none';
        target.css('opacity',visibility);
        target.css('pointer-events',pointerEvent);
    }


    this.initialize();

}