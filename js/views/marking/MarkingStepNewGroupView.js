var MarkingStepNewGroup = MarkingStepAbstract.extend({

    input:null,

    init: function()
    {
        this._super(MarkingManager.STEP_NEW_GROUP, TemplateManager.TEMPLATE_MARKING_NEW_GROUP);
    },

    checkIfEditionMode : function()
    {
        return (this.groupVo != undefined && this.groupVo != null);
    },

    setup: function()
    {
        this._super();
        this.input = $('#groupeNameInput');

        //This view also act as a reset for any current marking or group that was not completed
        this.clearCurrentGroupAndMarking();
    },

    reFillForm: function()
    {


        this.input = $('#groupeNameInput');
        if(this.checkIfEditionMode() == true)
        {
            this.input.val(this.groupVo.customName);
        }
        else
        {
            //This view also act as a reset for any current marking or group that was not completed
            this.clearCurrentGroupAndMarking();
            this.input.val("");
        }
    },

    clearCurrentGroupAndMarking: function()
    {
        MarkingManager.getInstance().clearCurrentGroup();
        MarkingManager.getInstance().clearCurrentMarking();
    },

    validate: function()
    {
        var groupName = this.input.val();

        //Form check
        if(groupName == "")
        {
            PopupManager.getInstance().openAlertPopup("", ResourceManager.getString("popup_choose_a_group_name"));
            return false;
        }

        // if no edition mode, let's create a new groupVo
        if(this.checkIfEditionMode() == false)
        {
            var newGroup = MarkingManager.getInstance().createNewGroup(groupName);
            MarkingManager.getInstance().setCurrentGroup(newGroup);
        }

        //Go to next step (Kind)
        return "/"+MarkingManager.STEP_KIND+"/";
    }

});