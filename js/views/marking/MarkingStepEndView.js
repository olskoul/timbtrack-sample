var MarkingStepEnd = MarkingStepAbstract.extend({

    init: function()
    {
        this._super(MarkingManager.STEP_END,TemplateManager.TEMPLATE_MARKING_END);
    },

    setup: function()
    {
        //register current group and current
        // -> Set group in which the marking is
        this.markingVo.group = this.groupVo.id;
        // -> Set sequence (species) if null, otherwise, keep same value
        if(this.markingVo.seq == null)
        this.markingVo.seq = this.groupVo.getNextSeq(this.markingVo);
        // -> Register Group
        MarkingManager.getInstance().registerGroup(this.groupVo);
        // -> Register Marking
        var markingIndex = MarkingManager.getInstance().registerMarking(this.markingVo,this.groupVo);
        // -> Save data Locally
        Infos.saveUserData();

        var markingLabel = this.markingVo.seq + " \"" + SpeciesManager.getInstance().getInitialsLabel(this.markingVo.species)+"\"";
        var templateData = {data:this.groupVo.generateTemplateData(),currentMarkingSeq:markingLabel};
        this.lang = Infos.lang;
        this.content.html(TemplateManager.getInstance().getTemplate(this.templateName, templateData));
        this.addListeners();
        this.header.showLeftBtn("btnMenu");


    },

    addListeners: function()
    {
        //hash change for step change
        this.addNoteBtn = this.el.find('.add-note');
        this.addNoteBtn[0].addEventListener('click', this.addNoteHandler.bind(this));

        //btns
        var markingListItem= $(".markingItem");
        for(var i=0; i<markingListItem.length; i++)
        {
            var item = markingListItem[i];
            var id = $(item).attr('id').split('marking')[1];
            var btnCheck = $(item).find('.checkedMarking');
            var btnEdit = $(item).find('.editMarking');
            var btnDelete = $(item).find('.deleteMarking');

            if(id == this.markingVo.id)
            {
                //Hide check btn
                $(btnCheck).css('display','none');

                //listeners
                btnEdit[0].addEventListener('click', this.editCurrentMarking.bind(this));
            }
            else
            {
                //hide edit btn
                $(btnEdit).css('display','none');
            }

            //delete
            btnDelete[0].addEventListener('click', this.deleteMarking.bind(this,id));

        }
    },

    editCurrentMarking:function()
    {
        if(this.markingVo != null && this.markingVo != undefined)
        {
            window.location.hash = "#marking/"+this.markingVo.kind;
        }
    },

    deleteMarking:function(markingId)
    {
        var markingVo = MarkingManager.getInstance().getMarkingById(markingId);

        if(markingVo != undefined && markingVo != null)
        {
            //delete from repository
            MarkingManager.getInstance().deleteMarking(markingId);

            var test = this.groupVo.markings;
            //save data
            //Infos.saveUserData();
            //reset
            this.reset();
        }
        //remove html element
        //var item = this.el.find("#marking"+markingId);
        //item.remove();

    },

    addNoteHandler:function()
    {
        NotesManager.getInstance().openNote(this.markingVo.notes[0],this.addNoteCallBack.bind(this));
    },

    addNoteCallBack:function(noteVo)
    {
        this.markingVo.notes.push(noteVo.id);
        noteVo.point = this.markingVo.point;
        var test = this.addNoteBtn.find("span");
        test.text(ResourceManager.getString("marking_edit_a_note"));

        console.log("MarkingStepEnd > Note Added > Save user data");
        Infos.saveUserData();
    }

});