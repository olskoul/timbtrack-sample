var MarkingStepAbstract = Class.extend({

    init: function($stepName,$templateName)
    {
        //Template
        this.stepName = $stepName;
        this.templateName = $templateName;
        this.el = $('<div/>'); // Define a div wrapper for the view. The div wrapper is used to attach events.
        this.content = $('<div/>');  // Define a div that will be the content
        this.el.html(this.content);

        //Current groupVo and markingVo
        this.updateCurrentGroupAndMarking();

        //edition mode
       // this.isEditionMode = this.checkIfEditionMode();

        //set lang rendered
        this.lang = Infos.lang;
    },

    updateCurrentGroupAndMarking: function()
    {
        //var groupId = UrlUtils.getInstance().getUrlParam("groupid");
        //var markingId = UrlUtils.getInstance().getUrlParam("markingid");

        this.groupVo = MarkingManager.getInstance().getCurrentGroup();
        this.markingVo = MarkingManager.getInstance().getCurrentMarking();
    },

    refresh: function()
    {
        this.updateCurrentGroupAndMarking();

        if(this.lang != Infos.lang)
        {
            this.lang = Infos.lang;
            this.reset();
        }

        //if end step or wood summary, reset view (re-generate template data)
        if( this.stepName == "end"
            || this.stepName == MarkingManager.STEP_KIND_WOOD_SUMMARY
            || this.stepName == MarkingManager.STEP_KIND_WOOD_BY_VOLUMES
        )
        {
            this.reset();
        }

        this.header.showLeftBtn("btnMenu");

        this.reFillForm();
    },

    reset : function()
    {
        //clear from memory
        this.content.empty();
        this.setup();
    },

    reFillForm: function(flag)
    {

    },


    checkIfEditionMode: function()
    {
        return false;
    },

    checkIfCancelable: function()
    {
        if(this.markingVo != undefined && this.markingVo != null)
        {
            if(MarkingManager.getInstance().getMarkingById(this.markingVo.id) == null)
            return true;
            else
            return false;
        }
        else
        return true;
    },

    setup: function()
    {
        this.lang = Infos.lang;
        this.content.html(TemplateManager.getInstance().getTemplate(this.templateName));
        this.addListeners();
        this.header.showLeftBtn("btnMenu");
    },

    addListeners: function()
    {
        //infos
        var infosBtns = $(".infoInput");
        for(var i=0; i<infosBtns.length; i++)
        {
            var infosBtn = infosBtns[i];
            infosBtn.addEventListener('click', this.infosBtnClickHandler.bind(this));
        }
    },

    validate: function()
    {
    },

    cancel: function()
    {
        PopupManager.getInstance().openConfirmPopup(ResourceManager.getString('popup_cancel_marking_title'), ResourceManager.getString('popup_cancel_marking_desc'), this.doCancel.bind(this), null);
    },

    doCancel: function()
    {
        if(this.groupVo != undefined && this.groupVo != null)
        {
            //Anyway we clear currentMarking
            MarkingManager.getInstance().clearCurrentMarking();

            //GroupVo is already saved because has at least 1 saved Marking in it.
            if(this.groupVo.markings.length > 0)
            {
                //go to end step kind to start a new one
                window.location.hash = "#marking/"+MarkingManager.STEP_KIND;
                return;
            }
            else //GroupVo is empty, marking is not finished and user wants to cancel
            {
                //MarkingManager.getInstance().clearCurrentGroup();
                window.location.hash = "#marking/"+MarkingManager.STEP_NEW_GROUP;
                return;
            }
        }
    },


    infosBtnClickHandler:function (e)
    {
        console.log('Marking infos input ClickHandler');

        //open tab by iD
        var target = $(e.currentTarget)[0];
        PopupManager.getInstance().openInfoInputPopup(target.id);
    },

    refHeader : function ($header)
    {
        this.header = $header;
    },

    showError : function ($element, $type, $flag)
    {
        if($element != undefined && $element != null)
        {
            switch($type)
            {
                case "number":
                        if($flag == false)
                            $element.css('background-color', 'rgba(255, 255, 255, 1)');
                        else
                            $element.css('background-color', 'rgba(255, 0, 0, .2)');
                    break;

                case "select":
                        if($flag == false)
                            $($element).css('background-color', 'rgba(255, 255, 255, 1)');
                        else
                            $($element).css('background-color', 'rgba(255, 0, 0, .2)');
                    break;
            }
        }
    }


});




