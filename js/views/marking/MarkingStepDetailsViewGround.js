var MarkingStepDetailsGround = MarkingStepAbstract.extend({

    init: function()
    {
        this._super("details/"+MarkingManager.STEP_KIND_GROUND, TemplateManager.TEMPLATE_MARKING_DETAILS_GROUND);
        this.qualityArray = ["F", "E", "D", "C", "B", "A"];
    },

    checkIfEditionMode : function()
    {
        return (this.markingVo != undefined
            && this.markingVo != null
            && this.markingVo.kind == 'ground'
            && this.markingVo.calculation.diameter != null);
    },

    setup: function()
    {
        this._super();

        //Slider outupt field
        this.sliderOutput = this.el.find('#quality-value');

        this.diameter = $('#diameterInput');
        this.length = $('#lengthInput');
        this.speciesSelect = $('#speciesSelect')[0];
        SpeciesManager.getInstance().getSpeciesHTML(this.speciesSelect);

    },

    reFillForm: function()
    {
        if(this.checkIfEditionMode())
        {
            this.diameter.val(this.markingVo.calculation.diameter);
            this.length.val(this.markingVo.calculation.length);

            for(var i= 0; i < this.speciesSelect.length; i++)
            {
                if(this.speciesSelect[i].value == this.markingVo.species)
                    this.speciesSelect.selectedIndex = i;
            }

            this.slider[0].value = this.markingVo.quality;
        }
        else
        {
            /*this.diameter.val("");
            this.length.val("");

            this.speciesSelect.selectedIndex = 0;

           var options = this.slider[0]( 'option' );
            this.slider[0]( 'values', [ options.min, options.max ] );*/
        }

    },

    addListeners: function()
    {
        this._super();
        //Slider
        this.slider = this.el.find('#quality-slider');
        this.slider[0].addEventListener('change',this.qualityChangeHandler.bind(this));

    },

    qualityChangeHandler: function(e)
    {
        var sliderValue = this.slider[0].value;
        this.sliderOutput[0].innerHTML = this.qualityArray[sliderValue/20];
    },


    validate: function()
    {
        //reset all errors
        this.showError(this.diameter,"number",false);
        this.showError(this.length,"number",false);
        this.showError(this.speciesSelect,"select",false);
        var hasError = false;

        //form check
        if(this.diameter.val() == "")
        {
            this.showError(this.diameter,"number",true);
            hasError = true;
        }
        //form check
        if(this.length.val() == "")
        {
            this.showError(this.length,"number",true);
            hasError = true;
        }
        //form check
        if(this.speciesSelect[this.speciesSelect.selectedIndex].value == "")
        {
            this.showError(this.speciesSelect,"select",true);
            hasError = true;
        }

        if(hasError == true)
        {
            PopupManager.getInstance().openAlertPopup("", ResourceManager.getString("popup_field_empty"));
            return false;
        }


        this.markingVo.quality = this.sliderOutput[0].innerHTML;
        this.markingVo.calculation.diameter = this.diameter.val();
        this.markingVo.calculation.length = this.length.val();
        this.markingVo.setSpecies(this.speciesSelect[this.speciesSelect.selectedIndex].value);
        this.markingVo.tonnage = this.markingVo.getTonnage(true);

        return "/"+MarkingManager.STEP_END+"/";
    }
});