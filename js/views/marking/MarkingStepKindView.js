var MarkingStepKind = MarkingStepAbstract.extend({

    lastGroupID:"",

    init: function()
    {
        this._super(MarkingManager.STEP_KIND,TemplateManager.TEMPLATE_MARKING_KIND);
    },

    checkIfEditionMode : function()
    {
        return (this.markingVo != undefined && this.markingVo != null);
    },

    setup: function()
    {
        this.lang = Infos.lang;
        this.content.html(TemplateManager.getInstance().getTemplate(this.templateName));

        this.selectedKind = "";
        this.btnArray = [];

        this.btnArray.push(this.el.find('.wood'));
        this.btnArray.push(this.el.find('.foot'));
        this.btnArray.push(this.el.find('.ground'));

        //deselect all btn by default
        this.updateBtns(null);


        this.addListeners();
        this.header.showLeftBtn("btnMenu");

    },

    reFillForm: function()
    {
        if(this.checkIfEditionMode() == true)
        {
            var kind = this.markingVo.kind;
            this.updateBtns(this.el.find('.'+kind));
        }
        else
        {
            if(this.lastGroupID != this.groupVo.id)
            this.updateBtns(null);
        }

    },

    addListeners: function()
    {
        for(var i=0; i<this.btnArray.length; i++)
        {
            this.btnArray[i][0].addEventListener('click', this.btnClickHandler.bind(this));
        }
    },


    btnClickHandler : function (e)
    {
        var target = (e != null)?$(e.currentTarget):null;
        this.updateBtns(target);
        this.validate(true);
    },

    updateBtns : function($btn)
    {
        this.selectedKind = "";

        for(var i=0; i<this.btnArray.length; i++)
        {
            if($btn !=null && this.btnArray[i][0].className == $btn[0].className)
            {
                this.btnArray[i].css("color", "#ffffff");
                this.btnArray[i].css("background-color", "#62b5c7");
                this.selectedKind = this.btnArray[i][0].className;
            }
            else
            {
                this.btnArray[i].css("color", "#2c2e33");
                this.btnArray[i].css("background-color", "#ffffff");
            }
        }
    },

    validate: function(fromKindBtn)
    {
        //Check global
        if(this.groupVo == undefined || this.groupVo == null)
        {
            PopupManager.getInstance().openAlertPopup(ResourceManager.getString("popup_no_group_id_title"), ResourceManager.getString("popup_no_group_id_desc"));
            return "/"+MarkingManager.STEP_NEW_GROUP;
        }

        //form Check
        if(this.selectedKind == "")
        {
            PopupManager.getInstance().openAlertPopup("", ResourceManager.getString("popup_choose_method"));
            return false;
        }

        //Edition mode or not
        if(this.checkIfEditionMode() == false)
        {
            // Create a new marking to be used
            var markingVo = MarkingManager.getInstance().createNewMarking(this.selectedKind);
            //Set current edited markingVo
            var result = MarkingManager.getInstance().setCurrentMarking(markingVo);
            if(result == null)
            return false;
        }
        else if(this.markingVo.kind != this.selectedKind)
        {
            //User has changed kind, create new marking
            // Create a new marking to be used
            var markingVo = MarkingManager.getInstance().createNewMarking(this.selectedKind);
            //Set current edited markingVo
            var result = MarkingManager.getInstance().setCurrentMarking(markingVo);
            if(result == null)
                return false;
        }
        else
        {
            //Assign selected marking to currently edited markingVo
            this.markingVo.kind = this.selectedKind;
        }

        //ref last group used
        this.lastGroupID = this.groupVo.id;

        if(fromKindBtn == true)
            window.location.hash = "#marking" + "/"+MarkingManager.STEP_GPS+"/";
        else
        return "/"+MarkingManager.STEP_GPS+"/";
    }
});