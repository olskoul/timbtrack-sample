var MarkingStepGPS = MarkingStepAbstract.extend({

    lastGroupId : "",
    MAX_ATTEMPT : 4,
    popupIsOpen: false,

    init: function()
    {
        this._super(MarkingManager.STEP_GPS,TemplateManager.TEMPLATE_MARKING_GPS);
    },


    checkIfEditionMode : function()
    {
        return (this.markingVo != undefined && this.markingVo != null && this.markingVo.point.longitude != false);
    },


    setup: function()
    {
        this._super();

        this.attempt = 0;

        this.mapIcon = this.el.find('#marking-gps-icon');
        this.mapIcon.css('color','#2c2e33');
    },

    reFillForm: function()
    {
        this.attempt = 0;

        if(this.checkIfEditionMode() == true)
        {
            this.displayCoords();
        }
        else
        {
            //Check current group is the same as last group
            if(this.lastGroupId == this.groupVo.id)
            {
                this.mapIcon.css('color','#2c2e33');
                //Go to next Page
                var refbtnClickHandler = this.btnClickHandler.bind(this);
                setTimeout(function()
                {
                    refbtnClickHandler(null);
                }, 500)
            }
        }

    },

    addListeners: function()
    {
        var getPositionBtn = this.el.find('#getPositionBtn')[0];
        getPositionBtn.addEventListener('click', this.btnClickHandler.bind(this));
    },

    btnClickHandler : function (e)
    {
        console.log("MarkingGPSview > btnClickHandler");
        if(this.attempt < this.MAX_ATTEMPT)
        {
            console.log("MarkingGPSview > btnClickHandler > retry");
            //get current position
            GeolocationManager.getInstance().getCurrentPosition(this.markingVo, this.markingVo.point, this.getPositionHandler.bind(this), this.getPositionError.bind(this));
        }
        else
        this.validate(true);

    },



    getPositionHandler : function (vo, pointGPS)
    {
        console.log("Marking step GPS. getPosition success");

        this.attempt++;

        this.markingVo.point = pointGPS;
        this.displayCoords();

        //vibration
        if(navigator.notification)
        {
            navigator.notification.vibrate(1000);
        }

        //Go to next Page
        var refValidate = this.validate.bind(this);
        setTimeout(function()
        {
            refValidate(true);
        }, 500);


    },

    displayCoords : function (atStart)
    {
        var element = this.el.find('#geolocationResult')[0];
        element.innerHTML = this.markingVo.point.latitude +" / "+this.markingVo.point.longitude;
        this.mapIcon.css('color','green');

        //ref last group id
        this.lastGroupId = this.groupVo.id;

        /*if(atStart != true)
        this.validate(true);*/
    },

    getPositionError : function (vo, pointGPS, errorMsg)
    {
        console.log("Marking step GPS. getPositionError");

        this.attempt++;

        this.markingVo.point = pointGPS;
        this.mapIcon.css('color','red');

        if(this.popupIsOpen == false)
        {
            this.popupIsOpen = true;
            PopupManager.getInstance().openAlertPopup(ResourceManager.getString("popup_no_gps_title"), ResourceManager.getString("popup_no_gps_desc")+"\n ("+errorMsg+")" +"\n("+this.attempt+"/"+this.MAX_ATTEMPT+")",this.errorPopupClose.bind(this));
        }
    },

    errorPopupClose : function ()
    {
        this.popupIsOpen = false;
    },

    validate: function(fromGpsHandler)
    {
        console.log("MarkingGPSview > validate");

        //Check global
        if(this.groupVo == undefined || this.groupVo == null )
        {
            PopupManager.getInstance().openAlertPopup(ResourceManager.getString("popup_no_group_id_title"), ResourceManager.getString("popup_no_group_id_desc"));
            return "/new";
        }

        if(this.markingVo == undefined || this.markingVo == null )
        {
            PopupManager.getInstance().openAlertPopup(ResourceManager.getString("popup_no_marking_title"), ResourceManager.getString("popup_no_marking_desc"));
            return "/new";
        }

        if(this.markingVo.point.longitude == false)
        {
            if(this.attempt < this.MAX_ATTEMPT)
            {
                //try again
                console.log("MarkingGPSview > validate > retry");
                this.btnClickHandler(null);
                return false;
            }
            else
            {
                //Inform user that no gps found, got to fix it online
                PopupManager.getInstance().openAlertPopup(ResourceManager.getString("popup_no_gps_title"), ResourceManager.getString("popup_no_gps_found_skip_label"));
            }

        }

        //Quick way to next page
        if(fromGpsHandler == true)
            window.location.hash = "#marking/"+this.markingVo.kind;
        else
        return "/"+this.markingVo.kind;
    }
});