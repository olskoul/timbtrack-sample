var FaqView = function (adapter, step) {



    /* ---------------------------------- Sort of constructor ---------------------------------- */
    this.initialize = function () {
        // Define a div wrapper for the view. The div wrapper is used to attach events.
        this.el = $('<div/>');
        this.content = $('<div/>');
        this.el.html(this.content);
        this.content.html(TemplateManager.getInstance().getTemplate(TemplateManager.TEMPLATE_EMPTY));
        this.showHeader();
    };


    /* ---------------------------------- Public methods ---------------------------------- */
    /*this.render = function()
    {
        //render empty template
        this.el.html(TemplateManager.getInstance().getTemplate(TemplateManager.TEMPLATE_EMPTY));

        //Show header
        this.showHeader();

        return this;

    };*/

    this.refresh = function()
    {
        if(this.lang != Infos.lang)
        {
            this.reset();
        }
        else
        {
            this.header.showLeftBtn("btnMenu");
        }

    };

    this.reset = function()
    {
        //clear from memory
        this.el.empty();
        this.setup();
    };

    this.setup = function()
    {
        //rendered lang
        this.lang = Infos.lang;

        //generate template data:
        var data = {data:Infos.generateFaqTemplateData()};
        //render wrapper template
        this.content.html(TemplateManager.getInstance().getTemplate(TemplateManager.TEMPLATE_FAQ,data));
        //refs
        this.faqWrapper = this.el.find('#faq-wrapper');
        //Show header
        this.showHeader();

        var questionItems = $('.faqClickItem');
        for(var i=0; i<questionItems.length; i++)
        {
            var item = questionItems[i];
            item.addEventListener('click', this.markingGroupClickHandler.bind(this));
        }
    }

    /* ---------------------------------- Privates methods ---------------------------------- */
    /**
     * Item click Handler
     */
    this.markingGroupClickHandler = function(e)
    {
        var target = $(e.currentTarget);
        var answer = target.parent().find('p');
        if(answer.css('display') == 'none')
        {
            target.css('background-color','#c8d0d1');
            answer.css('background-color','#c8d0d1');
            answer.css('display', 'block');
            ScrollUtils.getInstance().scrollToElement(this.faqWrapper,target);
        }
        else
        {
            target.css('background-color','#e2e8e9');
            answer.css('display', 'none');
        }
    }

    /**
     * Show header
     */
    this.showHeader = function()
    {
        this.headerContainer = this.el.find('#view-header');
        this.header = new HeaderView("default",ResourceManager.getString("header_title_faq"));
        this.headerContainer.html(this.header.render().el);
        this.header.setup();
        this.header.showLeftBtn("btnMenu");
        this.header.setBtnBack('#faq');
    }

    this.initialize();

}