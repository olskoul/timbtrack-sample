var LoginView = function (adapter) {

    this.initialize = function ()
    {
        // Define a div wrapper for the view. The div wrapper is used to attach events.
        this.el = $('<div/>');
        this.content = $('<div/>');
        this.el.html(this.content);
        //this.content.html(TemplateManager.getInstance().getTemplate(TemplateManager.TEMPLATE_EMPTY));
    };

    this.render = function() {



        return this;
    };

    this.setup = function()
    {
        this.content.html(TemplateManager.getInstance().getTemplate(TemplateManager.TEMPLATE_LOGIN));

        this.loginForm = $('#homeloginForm');
        this.userInfo = $('#homeUserInfos');
        this.menu = $('.menu-list-wrapper');
        this.header = $('.menu-header');
        this.help = $('.home-help');
        this.loginInput = $('#loginInput');
        this.passInput = $('#passeInput');
        this.submitBtn = $('.home-submit');
        this.submitBtn[0].addEventListener('click', this.submitClickHandler.bind(this));

        var posX = Math.round(($(window).width() - this.loginForm.width())/2);
        var posY = 75;//Math.round(($(window).height() - this.loginForm.height())/4);
        this.loginForm.css('left',posX);
        this.loginForm.css('top',posY);

        this.help.css('display','block');
        this.help.css('bottom','20px');
        this.help.css('left',Math.round(($(window).width() - this.help.width())/2));

        var lastUser = Infos.localStorage.getLastUser();
        this.loginInput.val(lastUser);

        if(Debug.LOGIN_PRE_FILL == true)
        {
            this.loginInput.val("sdgqdgf");
            this.passInput.val("tttttt");
        }


    };

    /**
     * Submit login form
     */
    this.submitClickHandler = function()
    {
        //loading
       // Loading.showLoading("loading_login");

         //Disable btn
        this.activateBtn(this.submitBtn,false);

        if(this.formIsValid())
        {
            var data =
            {
                username : this.loginInput.val(),
                password : this.passInput.val(),
                grant_type: "password",
                client_id: "timbtrack"
            }
            //Call login
            ServicesManager.getInstance().login(data, this.onLoginResultSuccess.bind(this), this.onLoginResultError.bind(this))
        }
        else
        {
            this.activateBtn(this.submitBtn,true);
        }

    }



    /**
     * Login success function
     */
    this.onLoginResultSuccess = function($data)
    {
        Loading.hideLoading();

        console.log("Login View > onLoginResult > success");
        console.log(JSON.stringify($data));

        //save user locally
        Infos.localStorage.saveUser(this.loginInput.val(),this.passInput.val());

        //save last user
        Infos.localStorage.saveLastUser(this.loginInput.val());

        //save token
        ServicesManager.getInstance().setToken($data.access_token);

        //save user email (uniq id for local loads)
        Infos.user.email = this.loginInput.val();

        //Sync data
        this.syncUserData();

    }

    /**
     * Login Error function
     */
    this.onLoginResultError = function(stringError, request)
    {
        //loading
        Loading.hideLoading();
        console.log("Login View > onError > stringError = "+stringError);
        this.activateBtn(this.submitBtn,true);
        PopupManager.getInstance().openAlertPopup(ResourceManager.getString("home_login_failed_title"), ResourceManager.getString("popup_error_login_"+request.status));

    }

    /**
     * Sync user data
     * start sync process
     */
    this.syncUserData = function()
    {
        //loading
        Loading.showLoading("loading_sync");

        console.log("Login View > syncUserData");

        var callBackSuccess = this.syncCompleteHandler.bind(this);
        var callBackError = this.syncErrorHandler.bind(this);

        SyncManager.getInstance().startSync(callBackSuccess,callBackError);
    }


    /**
     * Sync Complete
     * Init notes
     */
    this.syncCompleteHandler = function()
    {
        //loading
        Loading.hideLoading();

        console.log('Login > syncCompleteHandler');

        //init Notes
        NotesManager.getInstance().init();

        //Set user ad logged in
        Infos.user.isLoggedIn = true;

        //show home
        window.location.hash = "#homefromlogin";
    }

    /**
     * Sync Error function
     */
    this.syncErrorHandler = function(msg)
    {
        //loading
        Loading.hideLoading();
        console.log("LoginView > syncUserData > onError: "+msg);
        PopupManager.getInstance().openAlertPopup(ResourceManager.getString("popup_sync_needed_title"), ResourceManager.getString("popup_sync_error_fatal_desc") + "\n\n("+msg+")");
        ServicesManager.getInstance().mail("Error > LoginView > syncUserData > error > message:\n"+msg);
        this.activateBtn(this.submitBtn,true);
    }







    /**
     * Hide or show validate btn
     * @flag true or false
     */
    this.activateBtn = function (target, flag)
    {
        var visibility = (flag)? 1:.5;
        var pointerEvent = (flag)? 'auto':'none';
        target.css('opacity',visibility);
        target.css('pointer-events',pointerEvent);
    }

    /**
     * Submit login form
     */
    this.formIsValid = function()
    {
        if(this.loginInput.val() == "")
        {
            PopupManager.getInstance().openAlertPopup("Login", "Your username is empty.");
            return false;
        }

        if(this.passInput.val() == "")
        {
            PopupManager.getInstance().openAlertPopup("Login", "Your password is empty.");
            return false;
        }
        return true;
    }

    this.initialize();

}