var PopupAbstract = Class.extend({

    init: function($uid)
    {
        console.log($uid);

        this.wrapper = document.createElement('div');
        this.content = document.createElement('div');
        this.buttonWrap = document.createElement("ul");

        this.wrapper.className = "popup-wrapper";
        this.wrapper.id = $uid;
        $('body').append(this.wrapper);
        this.wrapper = $('#'+$uid);

        this.content.className = "popupContent";
        this.wrapper[0].appendChild(this.content);
        this.content = this.wrapper.find('.popupContent');

        this.buttonWrap.className = "buttonWrap";
        this.content[0].appendChild(this.buttonWrap);
        this.buttonWrap = this.content.find('.buttonWrap');
    },

    setup: function()
    {
        this.addListeners();
    },

    addListeners: function()
    {
    },

    /**
     * Open popup
     */
    open : function()
    {
        // this.cleanContent();
        this.wrapper.css("display", "block");
        this.wrapper.css("visibility", "visible");
        this.centerContent();
    },

    close: function()
    {
        this.wrapper.css("display", "none");
        this.wrapper.css("visibility", "hidden");
        this.cleanContent();
    },

    /**
     * Clean content
     */
    cleanContent : function()
    {
        //this.removeListener();

        if(this.buttonWrap)
            this.buttonWrap.empty();


        if(this.content != null)
            this.content.remove();
        this.content = null;
    },

    /**
     * Remove listeners
     */
    removeListener : function()
    {
        /*EventUtils.removeListener(okBtn,true);
        EventUtils.removeListener(cancelBtn,true);
        EventUtils.removeListener(seeBtn,true);
        EventUtils.removeListener(noBtn,true);
        EventUtils.removeListener(yesBtn,true);*/
    },

    /**
     * Center content on screen
     */
    centerContent :  function()
    {
        var left = Math.round($(window).width()/2 - this.content.outerWidth()/2);
        var top = Math.round($(window).height()/2 - this.content.outerHeight()/2);
        this.content.css("top",top);
        this.content.css("left",left);
    },

    /**
     * Create Btn
     */
    createBtn : function($type)
    {
        var btnContent;
        var btn = document.createElement('li');

        switch ($type)
        {
            case 'ok':
                btnContent = document.createElement('i');
                btnContent.className = "fa fa-check fa-2x";
                btn.appendChild(btnContent);
                break;

            case 'cancel':
                btn.innerHTML = ResourceManager.getString("common_cancel");
                break;

            case 'yes':
                btn.innerHTML = ResourceManager.getString("common_yes");
                break;

            case 'no':
                btn.innerHTML = ResourceManager.getString("common_no");
                break;

            case 'see':
                btn.innerHTML = ResourceManager.getString("common_see");
                break;

            case 'track':
                btn.innerHTML = ResourceManager.getString("common_track");
                break;
        }

        return btn;
    },

    /**
     * Add content to popup
     */
    addContent : function($contentToAdd)
    {
        this.content[0].insertBefore($contentToAdd,this.content[0].firstChild);
        //currentContent = $contentToAdd;
    }

});




