var PopupAlert = PopupAbstract.extend({


    init: function($uid)
    {
        this._super($uid);

    },

    setup: function($title, $desc, $okCallBack)
    {
        var wrap = document.createElement('div');
        var title = document.createElement('h1');
        var desc = document.createElement('p');
        wrap.appendChild(title);
        wrap.appendChild(desc);
        this.addContent(wrap);

        this.okBtn = this.createBtn('ok');
        this.buttonWrap[0].appendChild(this.okBtn);

        var closeFunction =  this.close.bind(this);
        this.okBtn.addEventListener('click', function()
        {
            closeFunction();
            if($okCallBack != undefined &&  $okCallBack != null)
                $okCallBack();

        });

        $("h1",this.content).text($title);
        $("p",this.content).text($desc);

        this._super();
    },

    addListeners: function()
    {
       // this.okBtn.addEventListener('click', this.close.bind(this));
    },

    close: function()
    {
        this._super();
    },

    /**
     * Remove listeners
     */
    removeListener : function()
    {
        //if(this.okBtn != undefined)
        //EventUtils.removeListener(this.okBtn,true);
    }


});