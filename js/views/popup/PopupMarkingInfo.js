var PopupMarkingInfo = PopupAbstract.extend({


    markingVo:null,

    init: function($uid)
    {
        this._super($uid);

    },

    setup: function($markingVo, showSeeBtn)
    {
        markingVo = $markingVo;
        var wrap = document.createElement('div');
        var title = document.createElement('h1');
        var desc = document.createElement('p');
        wrap.appendChild(title);
        wrap.appendChild(desc);
        this.addContent(wrap);


        this.seeBtn = this.createBtn('see');
        this.trackBtn = this.createBtn('track');
        this.okBtn = this.createBtn('ok');
        if(showSeeBtn == true)
        {
            this.seeBtn = this.createBtn('see');
            this.buttonWrap[0].appendChild(this.seeBtn);
        }

        this.buttonWrap[0].appendChild(this.trackBtn);
        this.buttonWrap[0].appendChild(this.okBtn);

        var desc = SpeciesManager.getInstance().getLabel($markingVo.species);
        var unit = (markingVo.kind == "wood")?ResourceManager.getString("common_stere_s"):"m³";
        desc += "<br />"+$markingVo.getTonnage() + " " +unit;
        desc += "<br />"+DateUtils.getStringDateFromTimeStamp($markingVo.id);
        desc += "<br />"+ResourceManager.getString("common_batch")+": "+MarkingManager.getInstance().getBatchById($markingVo.batch).name ;
        desc += "<br />"+$markingVo.id ;

        $("h1",this.content).text(ResourceManager.getString("common_marking"));
        $("p",this.content).html(desc);

        this._super();
    },

    addListeners: function()
    {
        this.seeBtn.addEventListener('click', this.seeMarkingOnMap.bind(this));
        this.okBtn.addEventListener('click', this.close.bind(this));
        this.trackBtn.addEventListener('click', this.trackMarkingOnMap.bind(this));
    },

    close: function()
    {
        this._super();
    },

    /**
     * Remove listeners
     */
    removeListener : function()
    {
        //EventUtils.removeListener(this.okBtn,true);
    },

    /**
     * See marking on map popup
     * See marking on pam
     */
    seeMarkingOnMap : function()
    {
        window.location.hash = "#map/?coordsToFit="+markingVo.point.longitude+"___"+markingVo.point.latitude;
        this.close();
    },

    /**
     * Track marking on map
     * track marking on map
     */
    trackMarkingOnMap : function()
    {
        window.location.hash = "#map/?track="+markingVo.id;
        this.close();
    }


});