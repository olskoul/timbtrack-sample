var PopupLoading = PopupAbstract.extend({


    init: function($uid)
    {
        this._super($uid);
        this.buttonWrap.css('display','none');
        this.content.css('padding-bottom','20px');
    },

    setup: function($msg)
    {
        var wrap = document.createElement('div');
        var title = document.createElement('h1');
        var icon = document.createElement('i');
        var desc = document.createElement('p');
        title.appendChild(icon);
        wrap.appendChild(icon);
        wrap.appendChild(desc);
        this.addContent(wrap);
        $("i",this.content).attr('class','fa fa-clock-o fa-2x');
        $("p",this.content).text(" "+$msg);

        this._super();
    },

    addListeners: function()
    {

    },

    update: function($msg)
    {
       // $("i",this.content).text(" "+$msg);
        $("p",this.content).text(" "+$msg);
    },

    close: function()
    {
        this._super();
    },

    /**
     * Remove listeners
     */
    removeListener : function()
    {
    }


});