var PopupConfirm = PopupAbstract.extend({


    init: function($uid)
    {
        this._super($uid);

    },

    setup: function($title, $desc, $yesCallBack, $noCallBack)
    {
        var wrap = document.createElement('div');
        var title = document.createElement('h1');
        var desc = document.createElement('p');
        wrap.appendChild(title);
        wrap.appendChild(desc);
        this.addContent(wrap);

        this.yesBtn = this.createBtn('yes');
        this.noBtn = this.createBtn('no');
        this.buttonWrap[0].appendChild(this.yesBtn);
        this.buttonWrap[0].appendChild(this.noBtn);

        var closeFunction =  this.close.bind(this);
        this.yesBtn.addEventListener('click', function()
        {
            closeFunction();
            if($yesCallBack != undefined)
            $yesCallBack();

        });

        this.noBtn.addEventListener('click', function()
        {
            closeFunction();
            if($noCallBack != undefined)
            $noCallBack();

        });


        $("h1",this.content).text($title);
        $("p",this.content).text($desc);

        this._super();
    },

    addListeners: function()
    {

    },

    close: function()
    {
        this._super();
    },

    /**
     * Remove listeners
     */
    removeListener : function()
    {
        //EventUtils.removeListener(this.yesBtn,true);
        //EventUtils.removeListener(this.noBtn,true);
    }


});