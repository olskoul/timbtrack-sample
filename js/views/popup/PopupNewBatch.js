var PopupNewBatch = PopupAbstract.extend({

    callbackData : {},
    callback : null,

    init: function($uid)
    {
        this._super($uid);

    },

    setup: function($validationCallback)
    {
        this.callback = $validationCallback;

        var wrap = document.createElement('div');
        wrap.innerHTML = TemplateManager.getInstance().getTemplate(TemplateManager.TEMPLATE_POPUP_NEW_BATCH,null,false);
        this.addContent(wrap);

        //Ref input
        this.batchInput = $(wrap).find('#batchNameInput');
        this.batchInput[0].addEventListener('change', this.newBatchNameChanged.bind(this));
        //Ref Radio btn
        this.batchForSaleNoRadioBtn = $(wrap).find('#btn-no');
        this.batchForSaleYesRadioBtn = $(wrap).find('#btn-yes');
        //ok btn
        this.okBtn = this.createBtn('ok');
        this.cancelBtn = this.createBtn('cancel');
        this.buttonWrap[0].appendChild(this.cancelBtn);
        this.buttonWrap[0].appendChild(this.okBtn);

        //Reset views
        this.selectBtn(this.batchForSaleNoRadioBtn,true);
        this.selectBtn(this.batchForSaleYesRadioBtn,false);
        this.activateBtn($(this.okBtn),false);
        this.batchInput.val("");

        this._super();
    },

    addListeners: function()
    {
        this.okBtn.addEventListener('click', this.newBatchValidate.bind(this));
        this.cancelBtn.addEventListener('click', this.close.bind(this));
        this.batchForSaleYesRadioBtn[0].addEventListener('click', this.batchForSaleYesNoClickHandler.bind(this));
        this.batchForSaleNoRadioBtn[0].addEventListener('click', this.batchForSaleYesNoClickHandler.bind(this));
    },

    close: function()
    {
        this._super();
    },

    /**
     * Remove listeners
     */
    removeListener : function()
    {
        /*EventUtils.removeListener(this.okBtn,true);
        EventUtils.removeListener(this.cancelBtn,true);
        EventUtils.removeListener(this.batchForSaleYesRadioBtn[0],true);
        EventUtils.removeListener(this.batchForSaleNoRadioBtn[0],true);*/
    },

    /**
     *  New batch popup yes no click handler
     */
    batchForSaleYesNoClickHandler : function(e)
    {
        var target = $(e.currentTarget);
        this.selectBtn(this.batchForSaleNoRadioBtn,(target[0] == this.batchForSaleNoRadioBtn[0]));
        this.selectBtn(this.batchForSaleYesRadioBtn,(target[0] == this.batchForSaleYesRadioBtn[0]));

        this.callbackData.isInventory = !(target[0] == this.batchForSaleYesRadioBtn[0]);
        console.log('Is inventory: '+this.callbackData.isInventory);
    },

    /**
     * select button
     * @flag true or false
     */
    selectBtn : function(target, flag)
    {
        var bgColor = (flag)?"#95c758":"#ffffff";
        var textColor = (flag)?"#ffffff":"#2c2e33";

        target.css("background-color", bgColor);
        target.css("color", textColor);


    },

    /**
     * Hide or show validate btn
     * @flag true or false
     */
    activateBtn : function (target, flag)
    {
        var visibility = (flag)? 1:.2;
        var pointerEvent = (flag)? 'auto':'none';
        target.css('opacity',visibility);
        target.css('pointer-events',pointerEvent);
    },

    /**
     * New batch validated
     */
    newBatchValidate : function()
    {
        var batchName = this.batchInput.val();
        if(batchName != "")
        {
            this.callbackData.name = batchName;
            this.callbackAndClose();
        }
        else
        {
            this.close();
            this.openAlertPopup("", ResourceManager.getString("popup_field_empty"));
        }

    },

    /**
     * callback and close
     */
    callbackAndClose : function()
    {
        if(this.callback != null)
            this.callback(this.callbackData);

        this.close();
    },

    /**
     * New batch validated
     */
    newBatchNameChanged : function()
    {
        var batchName = this.batchInput.val();
        this.activateBtn($(this.okBtn),(batchName != ""));
    }


});