var PopupInputInfo = PopupAbstract.extend({


    init: function($uid)
    {
        this._super($uid);

    },

    setup: function($id)
    {
        var wrap = document.createElement('div');
            var title = document.createElement('h1');
        var desc = document.createElement('p');
        wrap.appendChild(title);
        wrap.appendChild(desc);
        this.addContent(wrap);

        this.okBtn = this.createBtn('ok');
        this.buttonWrap[0].appendChild(this.okBtn);

        var desc = ResourceManager.getString("popup_"+$id);



        $("h1",this.content).text(ResourceManager.getString("popup_infosInput_title"));
        $("p",this.content).html(desc);

        this._super();
    },

    addListeners: function()
    {
        this.okBtn.addEventListener('click', this.close.bind(this));
    },

    close: function()
    {
        this._super();
    },

    /**
     * Remove listeners
     */
    removeListener : function()
    {
        //EventUtils.removeListener(this.okBtn,true);
    },

    /**
     * See marking on map popup
     * clean content
     */
    seeMarkingOnMap : function()
    {
        //TODO: Should be opening mapView with the markingID to locate
        this.close();
    }


});