var HeaderView = function ($type, $title)
{
    var LEFT_BTN_MENU = "btnMenu";
    var LEFT_BTN_BACK = "btnBack";
    var backClickCallback = null;
    var customBackHash = "";

    /* ---------------------------------- Sort of constructor ---------------------------------- */
    this.initialize = function () {
        // Define a div wrapper for the view. The div wrapper is used to attach events.
        this.el = $('<div/>');
    };

    this.initialize();

    /* ---------------------------------- Public methods ---------------------------------- */
    this.render = function()
    {
        var type;
        switch ($type)
        {
            default:
                type = TemplateManager.TEMPLATE_HEADER_DEFAULT;
                break;
        }
        //generate template data:
        var data = {data:{title:$title}};

        //render wrapper template
        this.el.html(TemplateManager.getInstance().getTemplate(type,data));


        return this;
    };

    this.setup = function()
    {
        this.btnBack = this.el.find('#btnBack');
        this.btnMenu = this.el.find('#btnMenu');
        this.leftBtns = [];
        this.leftBtns.push(this.btnMenu);
        this.leftBtns.push(this.btnBack);

        //setup default behavior
        this.btnBack[0].addEventListener("click",this.backClickHandler.bind(this));
        this.btnMenu[0].addEventListener("click",this.menuClickHandler.bind(this));

        //backClickCallback default value
        backClickCallback = null;

        //listen to hash change
        //$(window).on('hashchange', this.updateBtns.bind(this));

        //default hide all except btn menu
        this.showLeftBtn(LEFT_BTN_MENU);
    }

    /*this.updateBtns = function()
    {
        var pageName = UrlUtils.getInstance().getHashValue(0);
        var substep = UrlUtils.getInstance().getHashValue(1);
        if(pageName == "home")
            this.showLeftBtn(LEFT_BTN_BACK);
        else if(pageName != "marking" && substep == null)
        this.showLeftBtn(LEFT_BTN_MENU);
    }*/

    this.showLeftBtn = function($btnId)
    {
        var wantedId = '#'+$btnId;

        for(var i=0; i<this.leftBtns.length; i++)
        {
            var btn = this.leftBtns[i];
            if(btn.selector != wantedId)
                btn.css('display','none');
            else
                btn.css('display','block');
        }

    }

    this.changeTitle = function($title)
    {
        var title = $('#headerTitle');
        title.text($title);
    }

    this.setBtnBack = function($hash)
    {
        customBackHash = $hash;
        backClickCallback = this.customBackCallback.bind(this);
    }

    this.backClickHandler = function()
    {
        if(backClickCallback != null)
        backClickCallback();
    }

    this.menuClickHandler = function()
    {
        window.location.hash = "#home";
        this.showLeftBtn(LEFT_BTN_BACK);
    }

    /*this.defaultBackCallback = function()
    {
        history.back();
    }*/

    this.customBackCallback = function()
    {
        window.location.hash = customBackHash;
    }
}